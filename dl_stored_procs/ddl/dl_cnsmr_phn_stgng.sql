USE [crs5_oltp]
GO

/****** Object:  Table [dbo].[dl_cnsmr_phn_stgng]    Script Date: 9/8/2021 12:49:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if object_id('dbo.dl_cnsmr_phn_stgng', 'U') is not null
    drop table dbo.dl_cnsmr_phn_stgng;

CREATE TABLE [dbo].[dl_cnsmr_phn_stgng](
	[dl_cnsmr_phn_id] [bigint] IDENTITY(1,1) NOT NULL,
	[cnsmr_idntfr_lgcy_txt] [varchar](40) NULL,
	[agncy_entty_crrltn_id] [varchar](30) NULL,
	[cnsmr_idntfr_drvr_lcns_txt] [varchar](20) NULL,
	[cnsmr_phn_connct_attmpt_cnt] [smallint] NULL,
	[cnsmr_phn_connct_attmpt_lst_dt] [datetime] NULL,
	[c_p_c_s_lst_dt] [datetime] NULL,
	[cnsmr_phn_crt_dttm] [datetime] NULL,
	[cnsmr_phn_nmbr_txt] [varchar](20) NULL,
	[cnsmr_phn_src_val_txt] [varchar](32) NULL,
	[cnsmr_phn_stts_val_txt] [varchar](32) NULL,
	[cnsmr_phn_typ_val_txt] [varchar](32) NULL,
	[cnsmr_phn_xtnsn_txt] [varchar](10) NULL,
	[cnsmr_phn_cntry_val_txt] [varchar](32) NULL,
	[cnsmr_phn_cntry_dial_cd_txt] [varchar](10) NULL,
	[cnsmr_phn_area_dial_cd_txt] [varchar](10) NULL,
	[cnsmr_phn_sft_dlt_flg] [char](1) NULL,
	[cnsmr_phn_qlty_score_nmbr] [bigint] NOT NULL,
	[cnsmr_phn_cnsnt_flg] [char](1) NULL,
	[cnsmr_phn_cnsnt_dt] [datetime] NULL,
	[cnsmr_phn_tchnlgy_typ_val_txt] [varchar](32) NULL,
	[cnsmr_phn_cnfrmtn_dt] [datetime] NULL,
	[cnsmr_phn_sms_cnsnt_flg] [char](1) NULL,
	[cnsmr_phn_sms_cnsnt_dt] [datetime] NULL,
	[grp_nmbr] [bigint] NOT NULL,
	[upsrt_dttm] [datetime] NOT NULL,
	[upsrt_soft_comp_id] [int] NOT NULL,
	[upsrt_trnsctn_nmbr] [int] NOT NULL,
	[upsrt_usr_id] [bigint] NOT NULL,
	[crtd_usr_id] [bigint] NOT NULL,
	[crtd_dttm] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[dl_cnsmr_phn_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[dl_cnsmr_phn_stgng] ADD  DEFAULT ('N') FOR [cnsmr_phn_sft_dlt_flg]
GO

ALTER TABLE [dbo].[dl_cnsmr_phn_stgng] ADD  DEFAULT ((1)) FOR [cnsmr_phn_qlty_score_nmbr]
GO

ALTER TABLE [dbo].[dl_cnsmr_phn_stgng] ADD  DEFAULT ('N') FOR [cnsmr_phn_sms_cnsnt_flg]
GO


