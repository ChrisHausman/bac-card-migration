USE [crs5_oltp]
GO

/****** Object:  Table [dbo].[dl_cnsmr_addrss_stgng]    Script Date: 9/8/2021 12:50:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if object_id('dbo.dl_cnsmr_addrss_stgng', 'U') is not null
    drop table dbo.dl_cnsmr_addrss_stgng;

CREATE TABLE [dbo].[dl_cnsmr_addrss_stgng](
	[dl_cnsmr_addrss_id] [bigint] IDENTITY(1,1) NOT NULL,
	[cnsmr_idntfr_lgcy_txt] [varchar](40) NULL,
	[agncy_entty_crrltn_id] [varchar](30) NULL,
	[cnsmr_idntfr_drvr_lcns_txt] [varchar](20) NULL,
	[cnsmr_addrss_city_txt] [varchar](64) NULL,
	[cnsmr_addrss_cnty_txt] [varchar](128) NULL,
	[cnsmr_addrss_cntry_val_txt] [varchar](128) NULL,
	[cnsmr_addrss_crt_dttm] [datetime] NULL,
	[cnsmr_addrss_dlvrblty_txt] [varchar](32) NULL,
	[cnsmr_addrss_full_txt] [varchar](256) NULL,
	[cnsmr_addrss_ln_1_txt] [varchar](128) NULL,
	[cnsmr_addrss_ln_2_txt] [varchar](128) NULL,
	[cnsmr_addrss_ln_3_txt] [varchar](128) NULL,
	[cnsmr_addrss_mail_rtrn_val_txt] [varchar](128) NULL,
	[cnsmr_addrss_mail_rtrn_dt] [datetime] NULL,
	[cnsmr_addrss_obslt_dt] [datetime] NULL,
	[cnsmr_addrss_obslt_rsn_txt] [varchar](128) NULL,
	[cnsmr_addrss_src_val_txt] [varchar](128) NULL,
	[cnsmr_addrss_st_txt] [varchar](20) NULL,
	[cnsmr_addrss_stts_val_txt] [varchar](128) NULL,
	[cnsmr_addrss_pstl_cd_txt] [varchar](32) NULL,
	[c_a_m_r_cmmnt_txt] [varchar](1024) NULL,
	[cnsmr_addrss_cnfrmtn_dt] [datetime] NULL,
	[grp_nmbr] [bigint] NOT NULL,
	[upsrt_dttm] [datetime] NOT NULL,
	[upsrt_soft_comp_id] [int] NOT NULL,
	[upsrt_trnsctn_nmbr] [int] NOT NULL,
	[upsrt_usr_id] [bigint] NOT NULL,
	[crtd_usr_id] [bigint] NOT NULL,
	[crtd_dttm] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[dl_cnsmr_addrss_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


