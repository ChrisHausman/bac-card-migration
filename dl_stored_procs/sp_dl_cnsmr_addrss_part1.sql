/****** Object: Procedure [dbo].[Sp_dl_cnsmr_addrss_part1]   Script Date: 4/28/2023 1:34:45 PM ******/
USE [crs5_oltp];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
CREATE PROCEDURE [dbo].[Sp_dl_cnsmr_addrss_part1]
AS
BEGIN
	DECLARE @loop_count AS BIGINT
	DECLARE @total_count AS INTEGER
	DECLARE @total_loops AS INTEGER
	DECLARE @errNum INT
	DECLARE @errMsg VARCHAR(1024)
  DECLARE @StatusMsg as varchar(2500)
  
	SET @loop_count = 1;

	SELECT @total_count = Count(*)
	FROM dbo.DL_CNSMR_ADDRSS_STGNG;

	SELECT @total_loops = Max(grp_nmbr)
	FROM dbo.DL_CNSMR_ADDRSS_STGNG;

	SET @StatusMsg =  N'Number of passes (cnsmr_addrss) : ' + convert(varchar,@total_loops)
  PRINT @StatusMsg
  EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Status', 'DL Consumer Address Part 1', @StatusMsg
  
  WHILE @loop_count > 0
	BEGIN
		BEGIN TRY
			BEGIN TRAN

			SET @StatusMsg = N'Pass ' + convert(varchar,@loop_count) + ' of ' + convert(varchar,@total_loops) + ' at ' + convert(varchar, getdate(), 113)
			PRINT @StatusMsg
			EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Status', 'DL Consumer Address Part 1', @StatusMsg
      
      INSERT INTO cnsmr_addrss (
				cnsmr_id
				,cnsmr_addrss_city_txt
				,cnsmr_addrss_cnty_txt
				,cnsmr_addrss_cntry_cd
				,cnsmr_addrss_crt_dttm
				,cnsmr_addrss_dlvrblty_txt
				,cnsmr_addrss_full_txt
				,cnsmr_addrss_ln_1_txt
				,cnsmr_addrss_ln_2_txt
				,cnsmr_addrss_ln_3_txt
				,cnsmr_addrss_mail_rtrn_cd
				,cnsmr_addrss_mail_rtrn_dt
				,cnsmr_addrss_obslt_dt
				,cnsmr_addrss_obslt_rsn_txt
				,cnsmr_addrss_src_cd
				,cnsmr_addrss_st_txt
				,cnsmr_addrss_stts_cd
				,cnsmr_addrss_pstl_cd_txt
				,cnsmr_addrss_mail_rtrn_cmmnt_txt
				,cnsmr_addrss_cnfrmtn_dt
				,upsrt_dttm
				,upsrt_soft_comp_id
				,upsrt_trnsctn_nmbr
				,upsrt_usr_id
				) (
				SELECT cn.cnsmr_id
				,dl.cnsmr_addrss_city_txt
				,dl.cnsmr_addrss_cnty_txt
				,rcc.cntry_cd
				,dl.cnsmr_addrss_crt_dttm
				,dl.cnsmr_addrss_dlvrblty_txt
				,dl.cnsmr_addrss_full_txt
				,dl.cnsmr_addrss_ln_1_txt
				,dl.cnsmr_addrss_ln_2_txt
				,dl.cnsmr_addrss_ln_3_txt
				,ramrc.addrss_mail_rtrn_cd
				,dl.CNSMR_ADDRSS_MAIL_RTRN_DT
				,dl.CNSMR_ADDRSS_OBSLT_DT
				,dl.CNSMR_ADDRSS_OBSLT_RSN_TXT
				,RASC.ADDRSS_SRC_CD
				,dl.cnsmr_addrss_st_txt
				,1 /* 2/22/22 - Hardcode to VALID as per BAC request. */ --RATC.ADDRSS_STTS_CD
				,dl.cnsmr_addrss_pstl_cd_txt
				,dl.C_A_M_R_CMMNT_TXT
				,dl.CNSMR_ADDRSS_CNFRMTN_DT
				,dl.upsrt_dttm
				,dl.upsrt_soft_comp_id
				,dl.upsrt_trnsctn_nmbr
				,dl.upsrt_usr_id
				FROM DL_CNSMR_ADDRSS_STGNG dl
				     INNER JOIN cnsmr cn
					   ON cn.agncy_entty_crrltn_id = dl.agncy_entty_crrltn_id
					      AND cn.cnsmr_idntfr_drvr_lcns_txt = dl.cnsmr_idntfr_drvr_lcns_txt
					 LEFT OUTER JOIN REF_CNTRY_CD rcc
					   ON rcc.cntry_val_txt = dl.cnsmr_addrss_cntry_val_txt
					 LEFT OUTER JOIN Ref_addrss_mail_rtrn_cd ramrc
					   ON ramrc.addrss_mail_rtrn_val_txt = dl.cnsmr_addrss_mail_rtrn_val_txt
					 LEFT OUTER JOIN REF_ADDRSS_SRC_CD RASC
					   ON RASC.ADDRSS_SRC_VAL_TXT = dl.cnsmr_addrss_src_val_txt
					 LEFT OUTER JOIN REF_ADDRSS_STTS_CD RATC
					   ON RATC.ADDRSS_STTS_VAL_TXT = dl.cnsmr_addrss_stts_val_txt
				 WHERE grp_nmbr = @loop_count
				)

			IF @loop_count < @total_loops
			BEGIN
				SET @loop_count = @loop_count + 2
			END
			ELSE
			BEGIN
				SET @loop_count = 0
			END

			COMMIT TRAN
		END TRY

		BEGIN CATCH
			SELECT @errNum = Error_number()

			SELECT @errMsg = CONVERT(VARCHAR, @errNum) + '-' + Error_message()

			SELECT Error_number() ErrorNBR
				,Error_severity() Severity
				,Error_line() ErrorLine
				,Error_message() Msg

			PRINT 'Error !!! - ' + @errMsg

			ROLLBACK TRAN

			SET ROWCOUNT 0

			RAISERROR (
					@errMsg
					,@errNum
					,1
					,'Insert cnsmr address in Chunks'
					)
			EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Error', 'DL Consumer Address Part 1', @errMsg;
		END CATCH
	END
END
------------------------------------------------------------------------------------------------------------------------
Print 'Finished Script: C:\Users\zktbnwi\Downloads\DB-scripts\DBAug2022\Batch3\034\09_sp_dl_cnsmr_addrss_part1.sql'

GO

