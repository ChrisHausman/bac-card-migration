/****** Object: Procedure [dbo].[Sp_dl_cnsmr_phn_part1]   Script Date: 4/28/2023 1:34:45 PM ******/
USE [crs5_oltp];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
CREATE PROCEDURE [dbo].[Sp_dl_cnsmr_phn_part1]
AS
BEGIN
	DECLARE @loop_count AS BIGINT
	DECLARE @total_count AS INTEGER
	DECLARE @total_loops AS INTEGER
	DECLARE @errNum INT
	DECLARE @errMsg VARCHAR(1024)
  DECLARE @StatusMsg as varchar(2500)
    
	SET @loop_count = 1;

	SELECT @total_count = Count(*)
	FROM DL_CNSMR_PHN_STGNG;

	SELECT @total_loops = Max(grp_nmbr)
	FROM DL_CNSMR_PHN_STGNG;

	SET @StatusMsg =  N'Number of passes (cnsmr_phn) : ' + convert(varchar,@total_loops)
  PRINT @StatusMsg
  EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Status', 'DL Consumer Phone Part 1', @StatusMsg
  
  WHILE @loop_count > 0
	BEGIN
		BEGIN TRY
			BEGIN TRAN

			SET @StatusMsg = N'Pass ' + convert(varchar,@loop_count) + ' of ' + convert(varchar,@total_loops) + ' at ' + convert(varchar, getdate(), 113)
			PRINT @StatusMsg
			EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Status', 'DL Consumer Phone Part 1', @StatusMsg
      
      INSERT INTO cnsmr_Phn (
				cnsmr_id
				,cnsmr_phn_connct_attmpt_cnt
				,cnsmr_phn_connct_attmpt_lst_dt
				,cnsmr_phn_connct_sccssfll_lst_dt
				,cnsmr_phn_crt_dttm
				,cnsmr_phn_nmbr_txt
				,cnsmr_phn_src_cd
				,cnsmr_phn_stts_cd
				,cnsmr_phn_typ_cd
				,cnsmr_phn_xtnsn_txt
				,cnsmr_phn_cntry_cd
				,cnsmr_phn_cntry_dial_cd_txt
				,cnsmr_phn_area_dial_cd_txt
				,cnsmr_phn_sft_dlt_flg
				,cnsmr_phn_qlty_score_nmbr
				,cnsmr_phn_cnsnt_flg
				,cnsmr_phn_cnsnt_dt
				,cnsmr_phn_tchnlgy_typ_cd
				,cnsmr_phn_cnfrmtn_dt
				,cnsmr_phn_sms_cnsnt_flg
				,cnsmr_phn_sms_cnsnt_dt
				,upsrt_dttm
				,upsrt_soft_comp_id
				,upsrt_trnsctn_nmbr
				,upsrt_usr_id
				) (
				SELECT cn.cnsmr_id
				,dl.CNSMR_PHN_CONNCT_ATTMPT_CNT
				,dl.CNSMR_PHN_CONNCT_ATTMPT_LST_DT
				,dl.C_P_C_S_LST_DT
				,dl.CNSMR_PHN_CRT_DTTM
				,dl.CNSMR_PHN_NMBR_TXT
				,src.phn_src_cd
				,std.phn_stts_cd
				,ptc.phn_typ_cd
				,dl.CNSMR_PHN_XTNSN_TXT
				,cc.cntry_cd
				,dl.CNSMR_PHN_CNTRY_DIAL_CD_TXT
				,dl.CNSMR_PHN_AREA_DIAL_CD_TXT
				,dl.cnsmr_phn_sft_dlt_flg
				,dl.cnsmr_phn_qlty_score_nmbr
				,dl.cnsmr_phn_cnsnt_flg
				,dl.cnsmr_phn_cnsnt_dt
				,ptt.phn_tchnlgy_typ_cd
				,dl.cnsmr_phn_cnfrmtn_dt
				,dl.cnsmr_phn_sms_cnsnt_flg
				,dl.cnsmr_phn_sms_cnsnt_dt
				,dl.upsrt_dttm
				,dl.upsrt_soft_comp_id
				,dl.upsrt_trnsctn_nmbr
				,dl.upsrt_usr_id
				FROM DL_CNSMR_PHN_STGNG dl
				     INNER JOIN cnsmr cn
					   ON cn.agncy_entty_crrltn_id = dl.agncy_entty_crrltn_id
					      AND cn.cnsmr_idntfr_drvr_lcns_txt = dl.cnsmr_idntfr_drvr_lcns_txt
					 LEFT OUTER JOIN Ref_phn_src_cd src
					   ON src.PHN_SRC_VAL_TXT = dl.cnsmr_phn_src_val_txt
					 LEFT OUTER JOIN Ref_phn_stts_cd std
					   ON std.PHN_STTS_VAL_TXT = dl.cnsmr_phn_stts_val_txt
					 LEFT OUTER JOIN Ref_phn_typ_cd ptc
					   ON ptc.PHN_TYP_VAL_TXT = dl.cnsmr_phn_typ_val_txt
					 LEFT OUTER JOIN Ref_cntry_cd cc
					   ON cc.CNTRY_VAL_TXT = dl.cnsmr_phn_cntry_val_txt
					 LEFT OUTER JOIN ref_phn_tchnlgy_typ_cd ptt
					   ON ptt.PHN_TCHNLGY_TYP_VAL_TXT = dl.cnsmr_phn_tchnlgy_typ_val_txt
			     WHERE dl.grp_nmbr = @loop_count
				)

			IF @loop_count < @total_loops
			BEGIN
				SET @loop_count = @loop_count + 2
			END
			ELSE
			BEGIN
				SET @loop_count = 0
			END

			COMMIT TRAN
		END TRY

		BEGIN CATCH
			SELECT @errNum = Error_number()

			SELECT @errMsg = CONVERT(VARCHAR, @errNum) + '-' + Error_message()

			SELECT Error_number() ErrorNBR
				,Error_severity() Severity
				,Error_line() ErrorLine
				,Error_message() Msg

			PRINT 'Error !!! - ' + @errMsg

			ROLLBACK TRAN

			SET ROWCOUNT 0

			RAISERROR (
					@errMsg
					,@errNum
					,1
					,'Insert cnsmr phone in Chunks'
					)
      EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Error', 'DL Consumer Phone Part 1', @errMsg;
		END CATCH
	END
END
------------------------------------------------------------------------------------------------------------------------
Print 'Finished Script: C:\Users\zktbnwi\Downloads\DB-scripts\DBAug2022\Batch3\034\12_sp_dl_cnsmr_phn_part1_boa.sql'

GO

