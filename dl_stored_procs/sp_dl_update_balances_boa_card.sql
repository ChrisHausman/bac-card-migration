/****** Object: Procedure [dbo].[sp_dl_update_balances_boa]   Script Date: 4/28/2023 1:40:45 PM ******/
USE [crs5_oltp];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
create   PROCEDURE [dbo].[sp_dl_update_balances_boa] @chunk_size bigint = 500000
AS
BEGIN

DECLARE @loop_count as bigint
DECLARE @total_count as integer
DECLARE @total_loops as integer
declare @start_key bigint
DECLARE @errNum int
DECLARE @errMsg varchar(1024)

declare @StatusMsg as varchar(2500)

---- DEFAULT VALUES
DECLARE @Default_upsrt_soft_comp_id as bigint
DECLARE @Default_upsrt_usr_id as bigint

SELECT @Default_upsrt_soft_comp_id = upsrt_soft_comp_id
FROM lkup_soft_comp
WHERE upsrt_soft_comp_nm = 'load'

SELECT @Default_upsrt_usr_id = usr_id
FROM usr
WHERE usr_usrnm = 'CNVRSN'

-------------
-- Account Balances

SET @loop_count = 1
SELECT @total_count = count(*) FROM dl_cnsmr_accnt_stgng

select @start_key = min(dl_cnsmr_accnt_id) from dl_cnsmr_accnt_stgng

SELECT @total_loops = @total_count / @chunk_size + 1

SET @StatusMsg =  N'Number of passes (cnsmr_accnt) : ' + convert(varchar,@total_loops)
PRINT @StatusMsg
EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Status', 'DL Update Balances', @StatusMsg

WHILE @loop_count > 0
BEGIN
	BEGIN TRY
		BEGIN TRAN
			SET @StatusMsg = N'Pass ' + convert(varchar,@loop_count) + ' of ' + convert(varchar,@total_loops) + ' at ' + convert(varchar, getdate(), 113)
			PRINT @StatusMsg
			EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Status', 'DL Update Balances', @StatusMsg
			
			--Populate the cnsmr_accnt_bal table with the CurBal for all migrated accounts.
			update cab
			set    cab.cnsmr_accnt_bal_amnt = cat.cnsmr_accnt_trnsctn_amnt,
			       cab.cnsmr_accnt_unrndd_bal_amnt = cat.cnsmr_accnt_trnsctn_amnt,
				   cab.upsrt_dttm = getdate(),
				   cab.upsrt_soft_comp_id = 99,
				   cab.upsrt_trnsctn_nmbr = cab.upsrt_trnsctn_nmbr + 1
			from   dl_cnsmr_accnt_stgng stg
			       inner join cnsmr_accnt ca
				     on ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
				   inner join cnsmr_accnt_bal cab
				     on ca.cnsmr_accnt_id = cab.cnsmr_accnt_id
					    and cab.bal_nm_id = (select bal_nm_id from bal_nm where bal_shrt_nm = 'CurBal')
				   inner join
				   (
						select cat.cnsmr_accnt_id,
						       sum(cat.cnsmr_accnt_trnsctn_amnt) cnsmr_accnt_trnsctn_amnt
						from   cnsmr_accnt_trnsctn cat
						group  by cat.cnsmr_accnt_id
				   ) cat
				     on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
			WHERE  stg.dl_cnsmr_accnt_id >= @start_key
				   AND stg.dl_cnsmr_accnt_id < @start_key + @chunk_size
			OPTION (MAXDOP 0)

			--Populate the cnsmr_accnt_bckt_bal_rprtng table with the current balances for all buckets coming across in the migration.
			update cabbr
			set    cabbr.cnsmr_accnt_bckt_crrnt_bal_amnt = cat.cnsmr_accnt_trnsctn_amnt,
			       cabbr.cnsmr_accnt_bckt_unrndd_crrntl_bal_amnt = cat.cnsmr_accnt_trnsctn_amnt,
				   cabbr.cnsmr_accnt_bckt_systm_bal_amnt = cat.cnsmr_accnt_trnsctn_amnt,
				   cabbr.cnsmr_accnt_bckt_unrndd_systm_bal_amnt = cat.cnsmr_accnt_trnsctn_amnt,
				   cabbr.upsrt_dttm = getdate(),
				   cabbr.upsrt_soft_comp_id = 99,
				   cabbr.upsrt_trnsctn_nmbr = cabbr.upsrt_trnsctn_nmbr + 1
			from   dl_cnsmr_accnt_stgng stg
			       inner join cnsmr_accnt ca
				     on ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
				   inner join cnsmr_accnt_bckt_bal_rprtng cabbr
				     on ca.cnsmr_accnt_id = cabbr.cnsmr_accnt_id
				   inner join
				   (
						select cat.cnsmr_accnt_id,
							   cat.bckt_id,
						       sum(cat.cnsmr_accnt_trnsctn_amnt) cnsmr_accnt_trnsctn_amnt
						from   cnsmr_accnt_trnsctn cat
						group  by cat.cnsmr_accnt_id,
						       cat.bckt_id
				   ) cat
				     on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
					    and cabbr.bckt_id = cat.bckt_id
			WHERE  stg.dl_cnsmr_accnt_id >= @start_key
				   AND stg.dl_cnsmr_accnt_id < @start_key + @chunk_size
			OPTION (MAXDOP 0)

			--Populate the cnsmr_accnt_bckt_bal_rprtng table with the paid balances for all buckets coming across in the migration.
			update cabbr
			set    cabbr.cnsmr_accnt_bckt_ttl_pymnt_amnt = cat.cnsmr_accnt_trnsctn_amnt,
			       cabbr.cnsmr_accnt_bckt_unrndd_ttl_pymnt_amnt = cat.cnsmr_accnt_trnsctn_amnt,
				   cabbr.upsrt_dttm = getdate(),
				   cabbr.upsrt_soft_comp_id = 99,
				   cabbr.upsrt_trnsctn_nmbr = cabbr.upsrt_trnsctn_nmbr + 1
			from   dl_cnsmr_accnt_stgng stg
			       inner join cnsmr_accnt ca
				     on ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
				   inner join cnsmr_accnt_bckt_bal_rprtng cabbr
				     on ca.cnsmr_accnt_id = cabbr.cnsmr_accnt_id
				   inner join crdtr_bckt cb
				     on ca.crdtr_id = cb.crdtr_id
					    and cabbr.bckt_id = cb.bckt_id
						and cb.crdtr_ovrpymnt_bckt_flg = 'N'
				   inner join
				   (
						select cat.cnsmr_accnt_id,
							   cat.bckt_id,
						       sum(cat.cnsmr_accnt_trnsctn_amnt) cnsmr_accnt_trnsctn_amnt
						from   cnsmr_accnt_trnsctn cat
						       inner join Ref_bckt_trnsctn_typ_cd ref
							     on cat.bckt_trnsctn_typ_cd = ref.bckt_trnsctn_typ_cd
								    and ref.bckt_trnsctn_val_txt in ('PAYMENT', 'PAYMENTREVERSAL', 'NSF', 'SUSPENDEDPAYMENT')
						group  by cat.cnsmr_accnt_id,
						       cat.bckt_id
				   ) cat
				     on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
					    and cabbr.bckt_id = cat.bckt_id
			WHERE  stg.dl_cnsmr_accnt_id >= @start_key
				   AND stg.dl_cnsmr_accnt_id < @start_key + @chunk_size
			OPTION (MAXDOP 0)

            -- advance the start_key for the next set of records in next loop
            set @start_key = @start_key + @chunk_size;

            -- advance the loop
            if @loop_count < @total_loops
                set @loop_count = @loop_count + 1;
            else
                set @loop_count = 0;
				
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH
		SELECT @errNum = error_number()
		SELECT @errMsg = convert(varchar,@errNum) + '-' + error_message()
		SELECT error_number() ErrorNBR, error_severity() Severity,
			error_line() ErrorLine, error_message() Msg
		PRINT 'Error !!! - ' + @errMsg
		ROLLBACK TRAN
		SET rowcount 0
		EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Error', 'DL Update Balances', @errMsg;
	END CATCH

END

END
GO

