/****** Object: Procedure [dbo].[sp_dl_insert_accounts_part2_boa]   Script Date: 9/28/2021 4:26:11 PM ******/
USE [crs5_oltp];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
DROP PROCEDURE IF EXISTS [dbo].[sp_dl_insert_accounts_part2_boa]
GO
create PROCEDURE [dbo].[sp_dl_insert_accounts_part2_boa] AS BEGIN

DECLARE @loop_count as bigint
DECLARE @total_count as integer
DECLARE @total_loops as integer 
DECLARE @errNum int
DECLARE @errMsg varchar(1024)

declare @StatusMsg as varchar(2500)

---- DEFAULT VALUES
DECLARE @Default_wrkgrp_id as bigint
DECLARE @Default_upsrt_soft_comp_id as bigint
DECLARE @Default_upsrt_usr_id as bigint
DECLARE @Default_new_bsnss_btch_id as bigint
DECLARE @tm datetime

SELECT @Default_wrkgrp_id = wrkgrp_id
FROM wrkgrp
WHERE wrkgrp_shrt_nm = 'DfltWkgp'

SELECT @Default_upsrt_soft_comp_id = upsrt_soft_comp_id
FROM lkup_soft_comp
WHERE upsrt_soft_comp_nm = 'load'

SELECT @Default_upsrt_usr_id = usr_id
FROM usr
WHERE usr_usrnm = 'CNVRSN'

select  @tm = dbo.getdate()



SELECT @default_new_bsnss_btch_id = MAX(new_bsnss_btch_id) FROM new_bsnss_btch

-------------


-- Consumer Account (Part 2 - Even)

SET @loop_count = 2
SELECT @total_count = count(*) FROM dl_cnsmr_accnt_stgng

SELECT @total_loops = max(grp_nmbr) FROM dl_cnsmr_accnt_stgng

SET @StatusMsg =  N'Number of passes (cnsmr_accnt) : ' + convert(varchar,@total_loops)
PRINT @StatusMsg
EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Status', 'DL Load Accounts Part 2', @StatusMsg

WHILE @loop_count > 0
BEGIN
	BEGIN TRY
		BEGIN TRAN
			SET @StatusMsg = N'Pass ' + convert(varchar,@loop_count) + ' of ' + convert(varchar,@total_loops) + ' at ' + convert(varchar, getdate(), 113)
			PRINT @StatusMsg
			EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Status', 'DL Load Accounts Part 2', @StatusMsg

			INSERT INTO cnsmr_accnt
				(
				cnsmr_id, 
				crdtr_id, 
				fee_schdl_id, 
				fxd_fee_id, 
				new_bsnss_btch_id, 
				cnsmr_accnt_age_at_plcmnt_nmbr, 
				cnsmr_accnt_chrg_off_dt, 
				cnsmr_accnt_crdt_grntd_dt, 
				cnsmr_accnt_crdtr_lst_srvc_dt, 
				cnsmr_accnt_crdtr_rfrnc_id_txt, 
				cnsmr_accnt_crdtr_rfrnc_corltn_id_txt, 
				cnsmr_accnt_dscrptn_txt, 
				cnsmr_accnt_frwrd_rcll_cd, 
				cnsmr_accnt_idntfr_agncy_id, 
				cnsmr_accnt_idntfr_lgcy_txt, 
				cnsmr_accnt_idntfr_lgcy_CBR_txt, 
				cnsmr_accnt_is_actv_per_crdtr_flg, 
				cnsmr_accnt_is_bght_flg, 
				cnsmr_accnt_is_bnkrpt_flg, 
				cnsmr_accnt_is_chrgd_off_flg, 
				cnsmr_accnt_is_clsd_flg, 
				cnsmr_accnt_is_dsptd_flg, 
				cnsmr_accnt_is_intrnl_flg, 
				cnsmr_accnt_is_pyble_flg, 
				cnsmr_accnt_is_rllvr_flg, 
				cnsmr_accnt_is_rtrnd_flg, 
				cnsmr_accnt_intrst_prcntg, 
				cnsmr_accnt_intrst_typ_cd, 
				cnsmr_accnt_lwr_sttlmnt_mngr_apprvd_prcntg, 
				cnsmr_accnt_lwr_sttlmnt_mngr_apprvd_end_dttm, 
				cnsmr_accnt_orgnl_nominal_bal, 
				cnsmr_accnt_plcmnt_date, 
				cnsmr_accnt_plcmnt_usr_id, 
				cnsmr_accnt_rlsd_dt, 
				cnsmr_accnt_rlsd_usr_id, 
				cnsmr_accnt_rtrn_dt, 
				cnsmr_accnt_sprss_crdt_bureau_rprtng_flg, 
				cnsmr_accnt_sprss_cb_rprtng_rsn_txt, 
				cnsmr_accnt_pif_flg, 
				cnsmr_accnt_pif_dttm, 
				cnsmr_accnt_sif_flg, 
				cnsmr_accnt_sif_dttm, 
				cb_rprtng_strt_dt, 
				cnsmr_accnt_rprtng_max_sprss_dt, 
				cnsmr_accnt_is_unrprtbl_flg, 
				cb_rpt_accnt_typ_cd, 
				cb_rpt_accnt_stts_cd, 
				cb_rpt_accnt_prtfl_cd, 
				cb_rpt_accnt_spcl_cmmnt_cd, 
				cnsmr_accnt_dlqncy_dt, 
				cnsmr_orgnl_crdtr_nme_txt, 
				cnsmr_stts_info_ind_nmbr, 
				bnkrptcy_fltr_dt_cd, 
				agncy_entty_crrltn_id, 
				cb_frst_rprtd_dt, 
				cb_lst_rprtd_dt, 
				cb_rprtng_incld_flg, 
				cnsmr_accnt_is_updtd_flg, 
				cnsmr_accnt_is_frwrd_flg, 
				cb_cmplnc_cndtn_cd, 
				cnsmr_accnt_lgl_jdgmnt_flg, 
				cnsmr_accnt_incld_in_cnsmr_bal_flg, 
				cnsmr_accnt_pre_cllct_flg, 
				upsrt_dttm, 
				upsrt_soft_comp_id, 
				upsrt_trnsctn_nmbr, 
				upsrt_usr_id, 
				cnsmr_accnt_is_refi_flg, 
				cnsmr_accnt_stt_of_lmttns_dt, 
				cb_rpt_accnt_rule_spprsn_rsn_txt, 
				cnsmr_accnt_usr_spcfd_mk_whl_chrg_flg, 
				cnsmr_accnt_refi_dt, 
				effctv_fee_schdl_id, 
				cb_rpt_accnt_fnl_stts_no_chng_msg_txt, 
				crrncy_cd
				)
				(SELECT
					cs.cnsmr_id, 
					cr.crdtr_id, 
					(CASE WHEN f.fee_schdl_id IS NULL THEN cr.fee_schdl_id ELSE f.fee_schdl_id END), 
					(CASE WHEN ff.fxd_fee_id IS NULL THEN cr.fxd_fee_id ELSE ff.fxd_fee_id END),
					@Default_new_bsnss_btch_id, 
					cnsmr_accnt_age_at_plcmnt_nmbr, 
					cnsmr_accnt_chrg_off_dt, 
					cnsmr_accnt_crdt_grntd_dt, 
					cnsmr_accnt_crdtr_lst_srvc_dt, 
					cnsmr_accnt_crdtr_rfrnc_id_txt, 
					cnsmr_accnt_crdtr_rfrnc_corltn_id_txt, 
					cnsmr_accnt_dscrptn_txt, 
					(CASE WHEN rf.frwrd_rcll_cd IS NULL THEN 0 ELSE rf.frwrd_rcll_cd END), 
					cnsmr_accnt_idntfr_agncy_id, 
					cnsmr_accnt_idntfr_lgcy_txt, 
					cnsmr_accnt_idntfr_lgcy_CBR_txt, 
					cnsmr_accnt_is_actv_per_crdtr_flg, 
					cnsmr_accnt_is_bght_flg, 
					cnsmr_accnt_is_bnkrpt_flg, 
					cnsmr_accnt_is_chrgd_off_flg, 
					cnsmr_accnt_is_clsd_flg, 
					cnsmr_accnt_is_dsptd_flg, 
					cnsmr_accnt_is_intrnl_flg, 
					cnsmr_accnt_is_pyble_flg, 
					cnsmr_accnt_is_rllvr_flg, 
					cnsmr_accnt_is_rtrnd_flg, 
					cnsmr_accnt_intrst_prcntg, 
					(CASE WHEN ri.intrst_typ_cd IS NULL THEN 0 ELSE ri.intrst_typ_cd END), 
					cnsmr_accnt_lwr_sttlmnt_mngr_apprvd_prcntg, 
					cnsmr_accnt_lwr_sttlmnt_mngr_apprvd_end_dttm, 
					cnsmr_accnt_orgnl_nominal_bal, 
					cnsmr_accnt_plcmnt_date, 
					(CASE WHEN ru.usr_id IS NULL THEN 0 ELSE ru.usr_id END), 
					cnsmr_accnt_rlsd_dt, 
					(CASE WHEN rr.usr_id IS NULL THEN 0 ELSE rr.usr_id END), 
					cnsmr_accnt_rtrn_dt, 
					cnsmr_accnt_sprss_crdt_bureau_rprtng_flg, 
					cnsmr_accnt_sprss_cb_rprtng_rsn_txt, 
					cnsmr_accnt_pif_flg, 
					cnsmr_accnt_pif_dttm, 
					cnsmr_accnt_sif_flg, 
					cnsmr_accnt_sif_dttm, 
					cb_rprtng_strt_dt, 
					cnsmr_accnt_rprtng_max_sprss_dt,
					cnsmr_accnt_is_unrprtbl_flg,
					(CASE WHEN rt.cb_rpt_accnt_typ_cd IS NULL THEN 0 ELSE rt.cb_rpt_accnt_typ_cd END), 
					(CASE WHEN rs.cb_rpt_accnt_stts_cd IS NULL THEN 0 ELSE rs.cb_rpt_accnt_stts_cd END),
					(CASE WHEN rp.cb_rpt_accnt_prtfl_cd IS NULL THEN 0 ELSE rp.cb_rpt_accnt_prtfl_cd END),
					(CASE WHEN rc.cb_rpt_accnt_spcl_cmmnt_cd IS NULL THEN 0 ELSE rc.cb_rpt_accnt_spcl_cmmnt_cd END),
					cnsmr_accnt_dlqncy_dt, 
					cnsmr_orgnl_crdtr_nme_txt, 
					cnsmr_stts_info_ind_nmbr, 
					(CASE WHEN rb.bnkrptcy_fltr_dt_cd IS NULL THEN 0 ELSE rb.bnkrptcy_fltr_dt_cd END), 
					stg.agncy_entty_crrltn_id, 
					cb_frst_rprtd_dt, 
					cb_lst_rprtd_dt,
					cb_rprtng_incld_flg,
					cnsmr_accnt_is_updtd_flg,
					cnsmr_accnt_is_frwrd_flg,
					(CASE WHEN rcc.cb_cmplnc_cndtn_cd IS NULL THEN 0 ELSE rcc.cb_cmplnc_cndtn_cd END),
					cnsmr_accnt_lgl_jdgmnt_flg,
					cnsmr_accnt_incld_in_cnsmr_bal_flg, 
					(CASE WHEN cnsmr_accnt_pre_cllct_flg IS NULL THEN 'N' WHEN cnsmr_accnt_pre_cllct_flg = ' ' THEN 'N' ELSE cnsmr_accnt_pre_cllct_flg END), 
					getdate(),
					@Default_upsrt_soft_comp_id,
					0,
					@Default_upsrt_usr_id,
					cnsmr_accnt_is_refi_flg, 
					cnsmr_accnt_stt_of_lmttns_dt, 
					cb_rpt_accnt_rule_spprsn_rsn_txt,
					(CASE WHEN cnsmr_accnt_usr_spcfd_mk_whl_chrg_flg IS NULL THEN 'N' WHEN cnsmr_accnt_usr_spcfd_mk_whl_chrg_flg = ' ' THEN 'N' ELSE cnsmr_accnt_usr_spcfd_mk_whl_chrg_flg END),
					cnsmr_accnt_refi_dt, 
					(CASE WHEN ef.fee_schdl_id IS NULL THEN NULL ELSE ef.fee_schdl_id END), 
					cb_rpt_accnt_fnl_stts_no_chng_msg_txt, 
					null --(CASE WHEN c.crrncy_cd IS NULL THEN 0 ELSE c.crrncy_cd END)
				FROM dl_cnsmr_accnt_stgng stg
					INNER JOIN cnsmr cs ON cs.agncy_entty_crrltn_id = stg.agncy_entty_crrltn_id AND cs.cnsmr_idntfr_drvr_lcns_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
					INNER JOIN crdtr cr ON cr.crdtr_shrt_nm = stg.crdtr_shrt_nm
					LEFT OUTER JOIN fee_schdl f ON f.fee_schdl_shrt_nm = stg.fee_schdl_shrt_nm AND f.fee_schdl_is_hddn_flg= 'N'
					LEFT OUTER JOIN fxd_fee ff ON ff.fxd_fee_shrt_nm = stg.fxd_fee_shrt_nm 
									AND ff.fxd_fee_sft_dlt_flg='N' AND ff.fxd_fee_actv_flg= 'Y'
					LEFT OUTER JOIN Ref_frwrd_rcll_cd rf ON rf.frwrd_rcll_val_txt = stg.frwrd_rcll_val_txt
					LEFT OUTER JOIN Ref_intrst_typ_cd ri ON ri.intrst_typ_val_txt = stg.intrst_typ_val_txt
					LEFT OUTER JOIN usr ru ON ru.usr_usrnm = stg.plcmnt_usrnm
					LEFT OUTER JOIN usr rr ON rr.usr_usrnm = stg.rlsd_usrnm
					LEFT OUTER JOIN ref_cb_rpt_accnt_typ_cd rt ON rt.cb_rpt_accnt_typ_val_txt = stg.cb_rpt_accnt_typ_val_txt
					LEFT OUTER JOIN ref_cb_rpt_accnt_stts_cd rs ON rs.cb_rpt_accnt_stts_val_txt = stg.cb_rpt_accnt_stts_val_txt
					LEFT OUTER JOIN ref_cb_rpt_accnt_prtfl_cd rp ON rp.cb_rpt_accnt_prtfl_val_txt = stg.cb_rpt_accnt_prtfl_val_txt
					LEFT OUTER JOIN ref_cb_rpt_accnt_spcl_cmmnt_cd rc ON rc.cb_rpt_accnt_spcn_cmmnt_val_txt = stg.cb_rpt_accnt_spcl_cmmnt_txt
					LEFT OUTER JOIN ref_bnkrptcy_fltr_dt_cd rb ON rb.bnkrptcy_fltr_dt_val_txt = stg.bnkrptcy_fltr_dt_val_txt
					LEFT OUTER JOIN ref_cb_cmplnc_cndtn_cd rcc ON rcc.cb_cmplnc_cndtn_val_txt = stg.cb_cmplnc_cndtn_val_txt
					-- LEFT OUTER JOIN Ref_crrncy_cd c ON c.crrncy_val_txt = stg.crrncy_val_txt
					LEFT OUTER JOIN fee_schdl ef ON ef.fee_schdl_shrt_nm = stg.effctv_fee_schdl_shrt_nm AND ef.fee_schdl_is_hddn_flg= 'N'
				WHERE stg.grp_nmbr = @loop_count
				)
   
			INSERT INTO cnsmr_accnt_wrk_actn
				(
				cnsmr_accnt_id, 
				cnsmr_accnt_lst_actvty_dt, 
				cnsmr_accnt_lst_intrst_dt, 
				cnsmr_accnt_lst_pymnt_amnt, 
				cnsmr_accnt_lst_pymnt_dttm, 
				cnsmr_accnt_bal_config_chng_sync_flg, 
				cnsmr_accnt_intrst_calc_strt_dt, 
				cnsmr_accnt_gnrt_intrst_flg, 
				cnsmr_accnt_hld_flg, 
				cnsmr_accnt_hld_strt_dt, 
				cnsmr_accnt_hld_end_dttm, 
				cnsmr_accnt_end_int_accmltn_flg, 
				cnsmr_accnt_sttlmnt_wrt_off_amnt, 
				--cnsmr_accnt_lst_frwrd_rcll_dtl_id, 
				cnsmr_accnt_bal_lst_sync_dttm, 
				cnsmr_accnt_tag_high_prrty_nmbr, 
				cnsmr_accnt_actvty_stts_cd, 
				cnsmr_accnt_nxt_intrst_chrg_dttm, 
				cnsmr_accnt_wrkd_lst_dt, 
				cnsmr_accnt_cntct_lst_dttm, 
				cnsmr_accnt_cntct_nxt_dttm, 
				upsrt_dttm, 
				upsrt_soft_comp_id, 
				upsrt_trnsctn_nmbr, 
				upsrt_usr_id, 
				cnsmr_accnt_intrst_strt_dt, 
				cnsmr_accnt_tax_jrsdtn_prcssd_dt, 
				cnsmr_accnt_mk_whl_plcmnt_chrg_dt, 
				cnsmr_accnt_lttr_cnt, 
				cnsmr_accnt_lttr_lst_dt, 
				cnsmr_accnt_invcd_on_pif_flg, 
				ca_brk_blnc_rdctn_pln_flg
				)
				(SELECT
					ca.cnsmr_accnt_id, 
					cnsmr_accnt_lst_actvty_dt, 
					cnsmr_accnt_lst_intrst_dt, 
					cnsmr_accnt_lst_pymnt_amnt, 
					cnsmr_accnt_lst_pymnt_dttm, 
					cnsmr_accnt_bal_config_chng_sync_flg, 
					cnsmr_accnt_intrst_calc_strt_dt, 
					cnsmr_accnt_gnrt_intrst_flg, 
					cnsmr_accnt_hld_flg, 
					cnsmr_accnt_hld_strt_dt, 
					cnsmr_accnt_hld_end_dttm, 
					cnsmr_accnt_end_int_accmltn_flg, 
					cnsmr_accnt_sttlmnt_wrt_off_amnt, 
					--cnsmr_accnt_lst_frwrd_rcll_dtl_id, 
					cnsmr_accnt_bal_lst_sync_dttm, 
					cnsmr_accnt_tag_high_prrty_nmbr, 
					(CASE WHEN r.cnsmr_accnt_actvty_stts_cd IS NULL THEN NULL ELSE r.cnsmr_accnt_actvty_stts_cd END),
					cnsmr_accnt_nxt_intrst_chrg_dttm, 
					cnsmr_accnt_wrkd_lst_dt, 
					cnsmr_accnt_cntct_lst_dttm, 
					cnsmr_accnt_cntct_nxt_dttm, 
					GETDATE(), 
					@Default_upsrt_soft_comp_id, 
					0, 
					@Default_upsrt_usr_id, 
					cnsmr_accnt_intrst_strt_dt, 
					cnsmr_accnt_tax_jrsdtn_prcssd_dt, 
					cnsmr_accnt_mk_whl_plcmnt_chrg_dt, 
					cnsmr_accnt_lttr_cnt, 
					cnsmr_accnt_lttr_lst_dt, 
					cnsmr_accnt_invcd_on_pif_flg, 
					ca_brk_blnc_rdctn_pln_flg
				FROM dl_cnsmr_accnt_wrk_actn_stgng stg
					INNER JOIN cnsmr_accnt ca ON ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
					LEFT OUTER JOIN Ref_cnsmr_accnt_actvty_stts_cd r ON r.cnsmr_accnt_actvty_stts_val_txt = stg.cnsmr_accnt_actvty_stts_val_txt
				WHERE stg.grp_nmbr = @loop_count
				)

			INSERT INTO cnsmr_accnt_ownrs
				(
				cnsmr_id, 
				cnsmr_accnt_id, 
				cnsmr_accnt_ownrshp_typ_cd, 
				cnsmr_accnt_ownrshp_dttm, 
				cnsmr_accnt_ownrshp_sft_dlt_flg, 
				cnsmr_accnt_ownrshp_end_dttm, 
				cnsmr_accnt_ownrshp_wght_nmbr, 
				upsrt_dttm, 
				upsrt_soft_comp_id, 
				upsrt_trnsctn_nmbr, 
				upsrt_usr_id, 
				accnt_ownrshp_typ_qual_cd
				)
				(SELECT
					cs.cnsmr_id, 
					ca.cnsmr_accnt_id, 
					(CASE WHEN r.accnt_ownrshp_typ_cd IS NULL THEN 1 ELSE r.accnt_ownrshp_typ_cd END), 
					cnsmr_accnt_ownrshp_dttm, 
					cnsmr_accnt_ownrshp_sft_dlt_flg, 
					cnsmr_accnt_ownrshp_end_dttm, 
					cnsmr_accnt_ownrshp_wght_nmbr, 
					GETDATE(), 
					@Default_upsrt_soft_comp_id, 
					0, 
					@Default_upsrt_usr_id, 
					(CASE WHEN rq.accnt_ownrshp_typ_qual_cd IS NULL THEN 1 ELSE rq.accnt_ownrshp_typ_qual_cd END)
				FROM dl_cnsmr_accnt_ownrs_stgng stg
					INNER JOIN cnsmr_accnt ca ON ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
					INNER JOIN cnsmr cs ON cs.agncy_entty_crrltn_id = stg.agncy_entty_crrltn_id AND cs.cnsmr_idntfr_drvr_lcns_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
					LEFT OUTER JOIN ref_accnt_ownrshp_typ_cd r ON r.accnt_ownrshp_typ_val_txt = stg.accnt_ownrshp_typ_val_txt
					LEFT OUTER JOIN ref_accnt_ownrshp_typ_qual_cd rq ON rq.accnt_ownrshp_typ_qual_val_txt = stg.accnt_ownrshp_typ_qual_val_txt
				WHERE stg.grp_nmbr = @loop_count
				)

			INSERT INTO cnsmr_accnt_trnsctn
				(
				bckt_id,
				bckt_trnsctn_typ_cd, 
				cnsmr_accnt_id, 
				cnsmr_accnt_pymnt_jrnl_id, 
				cnsmr_accnt_trnsctn_stts_cd, 
				fee_typ_id, 
				wrkgrp_id, 
				cnsmr_accnt_trnsctn_amnt, 
				cnsmr_accnt_trnsctn_bckt_rnng_bal_amnt, 
				cnsmr_accnt_trnsctn_cmssn_prcntg, 
				cnsmr_accnt_trnsctn_cmssn_amnt, 
				cnsmr_accnt_trnsctn_cmssn_isdflt_flg, 
				cnsmr_accnt_trnsctn_cmssn_isfxd_flg, 
				cnsmr_accnt_trnsctn_pst_dt, 
				cnsmr_accnt_ovr_pymnt_amnt, 
				rcvr_id, 
				cnsmr_accnt_trnsctn_rcvr_cmssn_prcntg, 
				cnsmr_accnt_trnsctn_rcvr_cmssn_amnt, 
				cnsmr_accnt_trnsctn_rcvr_cmssn_isfxd_flg, 
				cnsmr_accnt_trnsctn_rcvr_cmssn_isdflt_flg, 
				cnsmr_accnt_trnsctn_lctn_cd, 
				cnsmr_accnt_trnsctn_invcd_flg, 
				cnsmr_accnt_trsnctn_entrd_dttm, 
				cnsmr_accnt_trnsctn_cntxt_cd, 
				upsrt_dttm, 
				upsrt_soft_comp_id, 
				upsrt_trnsctn_nmbr, 
				upsrt_usr_id, 
				crtd_dttm, 
				crtd_usr_id, 
				cnsmr_accnt_lst_dly_intrst_accmltn_trnsctn_flg, 
				crdtr_id, 
				rcvr_cnsmr_accnt_trnsctn_hld_end_dttm, 
				rcvr_rcvbl_sttmnt_of_accnt_btch_id, 
				rcvr_pybl_sttmnt_of_accnt_btch_id, 
				cnsmr_accnt_trnsctn_is_systm_adjstd_flg, 
				cnsmr_accnt_trnsctn_orgnl_cmssn_amnt, 
				cnsmr_accnt_trnsctn_orgnl_cmssn_prcntg, 
				cnsmr_accnt_trnsctn_rcvr_is_systm_adjstd_flg, 
				cnsmr_accnt_trnsctn_rcvr_orgnl_cmssn_amnt, 
				cnsmr_accnt_trnsctn_rcvr_orgnl_cmssn_prcntg, 
				cnsmr_accnt_orgnl_trnsctn_id, 
				cnsmr_accnt_trnsctn_lnkd_id, 
				crrncy_cd, 
				cat_plcmnt_chrg_ind_flg
				)
				(SELECT
					b.bckt_id,
					(CASE WHEN rt.bckt_trnsctn_typ_cd IS NULL THEN 1 ELSE rt.bckt_trnsctn_typ_cd END), 
					ca.cnsmr_accnt_id, 
					NULL,			--cnsmr_accnt_pymnt_jrnl_id, 
					(CASE WHEN rs.cnsmr_accnt_trnsctn_stts_cd IS NULL THEN 1 ELSE rs.cnsmr_accnt_trnsctn_stts_cd END), 
					(CASE WHEN ft.fee_typ_id IS NULL THEN NULL ELSE ft.fee_typ_id END),
					(CASE WHEN w.wrkgrp_id IS NULL THEN @Default_wrkgrp_id ELSE w.wrkgrp_id END),
					0.00, --cnsmr_accnt_trnsctn_amnt, 9/28/21 - ChrisH - Change for CARD conversion, load $0 to buckets initially.
					cnsmr_accnt_trnsctn_bckt_rnng_bal_amnt, 
					cnsmr_accnt_trnsctn_cmssn_prcntg, 
					cnsmr_accnt_trnsctn_cmssn_amnt, 
					cnsmr_accnt_trnsctn_cmssn_isdflt_flg, 
					cnsmr_accnt_trnsctn_cmssn_isfxd_flg, 
					cnsmr_accnt_trnsctn_pst_dt, 
					cnsmr_accnt_ovr_pymnt_amnt, 
					(CASE WHEN r.rcvr_id IS NULL THEN NULL ELSE r.rcvr_id END),
					cnsmr_accnt_trnsctn_rcvr_cmssn_prcntg, 
					cnsmr_accnt_trnsctn_rcvr_cmssn_amnt, 
					cnsmr_accnt_trnsctn_rcvr_cmssn_isfxd_flg, 
					cnsmr_accnt_trnsctn_rcvr_cmssn_isdflt_flg, 
					(CASE WHEN rl.pymnt_lctn_cd IS NULL THEN 1 ELSE rl.pymnt_lctn_cd END), 
					cnsmr_accnt_trnsctn_invcd_flg, 
					cnsmr_accnt_trsnctn_entrd_dttm, 
					(CASE WHEN rc.trnsctn_cntxt_cd IS NULL THEN 1 ELSE rc.trnsctn_cntxt_cd END), 
					GETDATE(), 
					@Default_upsrt_soft_comp_id, 
					0, 
					@Default_upsrt_usr_id, 
					GETDATE(), 
					@Default_upsrt_usr_id, 
					cnsmr_accnt_lst_dly_intrst_accmltn_trnsctn_flg, 
					(CASE WHEN cr.crdtr_id IS NULL THEN ca.crdtr_id ELSE cr.crdtr_id END), 
					rcvr_cnsmr_accnt_trnsctn_hld_end_dttm, 
					rcvr_rcvbl_sttmnt_of_accnt_btch_id, 
					rcvr_pybl_sttmnt_of_accnt_btch_id, 
					cnsmr_accnt_trnsctn_is_systm_adjstd_flg, 
					cnsmr_accnt_trnsctn_orgnl_cmssn_amnt, 
					cnsmr_accnt_trnsctn_orgnl_cmssn_prcntg, 
					cnsmr_accnt_trnsctn_rcvr_is_systm_adjstd_flg, 
					cnsmr_accnt_trnsctn_rcvr_orgnl_cmssn_amnt, 
					cnsmr_accnt_trnsctn_rcvr_orgnl_cmssn_prcntg, 
					cnsmr_accnt_orgnl_trnsctn_id, 
					cnsmr_accnt_trnsctn_lnkd_id, 
					crrncy_cd, 
					cat_plcmnt_chrg_ind_flg
				FROM dl_cnsmr_accnt_trnsctn_stgng stg
					INNER JOIN cnsmr_accnt ca ON ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
					INNER JOIN bckt b ON b.bckt_shrt_nm = stg.bckt_shrt_nm
					LEFT OUTER JOIN Ref_bckt_trnsctn_typ_cd rt ON rt.bckt_trnsctn_val_txt = stg.bckt_trnsctn_typ_val_txt
					LEFT OUTER JOIN ref_cnsmr_accnt_trnsctn_stts_cd rs ON rs.cnsmr_accnt_trnsctn_stts_val_txt = stg.cnsmr_accnt_trnsctn_stts_val_txt
					LEFT OUTER JOIN fee_typ ft ON ft.fee_typ_nm = stg.fee_typ_shrt_nm
					LEFT OUTER JOIN wrkgrp w ON w.wrkgrp_shrt_nm = stg.wrkgrp_shrt_nm
					LEFT OUTER JOIN rcvr r ON r.rcvr_shrt_nm = stg.rcvr_shrt_nm
					LEFT OUTER JOIN Ref_pymnt_lctn_cd rl ON rl.pymnt_lctn_val_txt = stg.pymnt_lctn_val_txt
					LEFT OUTER JOIN ref_trnsctn_cntxt_cd rc ON rc.trnsctn_cntxt_val_txt = stg.trnsctn_cntxt_val_txt
					LEFT OUTER JOIN crdtr cr ON cr.crdtr_shrt_nm = stg.crdtr_shrt_nm
				WHERE stg.grp_nmbr = @loop_count
				)
				
			--TITAN-38673
			INSERT INTO cnsmr_accnt_cnfg
				   (cnsmr_accnt_id
				   ,cnsmr_accnt_sttlmnt_athrztn_id
				   ,cnsmr_accnt_loan_bpcm_cnfg_id
				   ,mk_whl_cap_rt_prcntg
				   ,cnsmr_accnt_delay_mk_whl_plcmnt_chrg_flg
				   ,cnsmr_accnt_blnc_lmt_amnt
				   ,cnsmr_accnt_blnc_shdw_lmt_amnt
				   ,upsrt_dttm
				   ,upsrt_soft_comp_id
				   ,upsrt_trnsctn_nmbr
				   ,upsrt_usr_id
				   ,crtd_usr_id
				   ,crtd_dttm)
					(SELECT
						ca.cnsmr_accnt_id, 
						NULL, 
						NULL, 
						NULL, 
						'N', 
						NULL,
						NULL,					
						dbo.getdate(), 
						@Default_upsrt_soft_comp_id, 
						0, 
						@Default_upsrt_usr_id, 
						@Default_upsrt_usr_id,
						dbo.getdate()
						FROM dl_cnsmr_accnt_stgng stg
						INNER JOIN cnsmr_accnt ca ON ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
						--INNER JOIN cnsmr_accnt_cnfg caf ON caf.cnsmr_accnt_id = ca.cnsmr_accnt_id
						WHERE stg.grp_nmbr = @loop_count
				)	
				
					--TITAN-39971							
					INSERT INTO dbo.cnsmr_accnt_bal
					 (bal_nm_id
					 ,cnsmr_accnt_id
					 ,cnsmr_accnt_bal_amnt
					 ,cnsmr_accnt_unrndd_bal_amnt
					 ,cnsmr_accnt_bal_dt
					 ,upsrt_dttm
					 ,upsrt_soft_comp_id
					 ,upsrt_trnsctn_nmbr
					 ,upsrt_usr_id)
					 (SELECT distinct bal.bal_nm_id
					 ,ca.CNSMR_ACCNT_ID
					 ,0.00
					 ,0.00
					 ,@tm
					 ,@tm
					,@Default_upsrt_soft_comp_id 
					,0
					,@Default_upsrt_usr_id
					  from dl_cnsmr_accnt_stgng stg
					  INNER JOIN cnsmr_accnt ca ON ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt AND stg.grp_nmbr = @loop_count
					  INNER JOIN crdtr_bal_nm_config cbnc on ca.crdtr_id = cbnc.crdtr_id
					  INNER JOIN bal_nm bal on cbnc.bal_nm_id = bal.bal_nm_id and bal.bal_nm_actv_flg='Y' and bal.bal_calc_on_dmnd_flg = 'N'
                      --LEFT OUTER JOIN cnsmr_accnt_bal cab on ca.cnsmr_accnt_id = cab.cnsmr_accnt_id and cbnc.bal_nm_id = cab.bal_nm_id
                      --where cab.cnsmr_accnt_bal_id is null and grp_nmbr = @loop_count
					 )
					 
					 INSERT INTO dbo.cnsmr_accnt_bckt_bal_rprtng
						 (bckt_id
						 ,cnsmr_accnt_id
						 ,cnsmr_accnt_bckt_orgnl_bal_amnt
						 ,cnsmr_accnt_bckt_crrnt_bal_amnt
						 ,cnsmr_accnt_bckt_ttl_pymnt_amnt
						 ,cnsmr_accnt_bckt_unrndd_crrntl_bal_amnt
						 ,cnsmr_accnt_bckt_unrndd_orgnl_bal_amnt
						 ,cnsmr_accnt_bckt_unrndd_ttl_pymnt_amnt
						 ,cnsmr_accnt_bckt_systm_bal_amnt
						 ,cnsmr_accnt_bckt_unrndd_systm_bal_amnt
						 ,cnsmr_accnt_bckt_bal_dt
						 ,cnsmr_accnt_bckt_pybl_bal_amnt
						 ,cnsmr_accnt_bckt_unrndd_pybl_bal_amnt
						 ,upsrt_dttm
						 ,upsrt_soft_comp_id
						 ,upsrt_trnsctn_nmbr
						 ,upsrt_usr_id)
						 (
						 SELECT crbkt.bckt_id
						 ,ca.CNSMR_ACCNT_ID
						 ,0.00
						 ,0.00
						 ,0.00
						 ,0.00
						 ,0.00
						 ,0.00
						 ,0.00
						 ,0.00
						 ,dbo.getdate()
						 ,0.00
						 ,0.00
						 ,dbo.getdate()
						 ,@Default_upsrt_soft_comp_id 
						 ,0
						 ,@Default_upsrt_usr_id 
						 FROM dl_cnsmr_accnt_stgng stg
						 INNER JOIN cnsmr_accnt ca ON ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt AND stg.grp_nmbr = @loop_count
						 INNER JOIN crdtr_bckt crbkt on ca.crdtr_id = crbkt.crdtr_id
						 --left outer join cnsmr_accnt_bckt_bal_rprtng cabbr on crbkt.bckt_id = cabbr.bckt_id	and cabbr.cnsmr_accnt_id = ca.cnsmr_accnt_id					  
						 --WHERE grp_nmbr = @loop_count and cabbr.cnsmr_accnt_bckt_bal_id is null
						 )
						 
				--TITAN-43380
				INSERT INTO cnsmr_accnt_strtgy_wrk_actn
							(cnsmr_accnt_id
							,cnsmr_accnt_lck_usr_id
							,cnsmr_accnt_lck_flg
							,strtgy_id
							,cnsmr_accnt_strtgy_lst_run_dt
							,cnsmr_accnt_strtgy_lst_stp_id
							,cnsmr_accnt_strtgy_nxt_stp_id
							,upsrt_dttm
							,upsrt_soft_comp_id
							,upsrt_trnsctn_nmbr
							,upsrt_usr_id
							,crtd_usr_id
							,crtd_dttm )
							(select ca.cnsmr_accnt_id,
							case when stg.cnsmr_accnt_lck_flg = 'Y' then coalesce(u.usr_id,@Default_upsrt_usr_id)
							else null end cnsmr_accnt_lck_usr_id,
							coalesce(stg.cnsmr_accnt_lck_flg,'N') cnsmr_accnt_lck_flg,
							s.strtgy_id,
							case when sl.strtgy_stp_id is null then null
							else coalesce(stg.cnsmr_accnt_strtgy_lst_run_dt,dbo.getdate()) end cnsmr_accnt_strtgy_lst_run_dt,
							sl.strtgy_stp_id cnsmr_accnt_strtgy_lst_stp_id,
							sn.strtgy_stp_id cnsmr_accnt_strtgy_nxt_stp_id,
							dbo.getdate(),
							@Default_upsrt_soft_comp_id ,
							0,
							@Default_upsrt_usr_id,
							@Default_upsrt_usr_id,
							dbo.getdate()
							from dl_ca_strtgy_wrk_actn_stgng stg
							INNER JOIN cnsmr_accnt ca ON ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
							LEFT OUTER JOIN strtgy s on s.STRTGY_SHRT_NM = stg.STRTGY_SHRT_NM
							LEFT OUTER JOIN strtgy_stp sl on sl.strtgy_stp_shrt_nm = stg.strtgy_lst_stp_shrt_nm
							LEFT OUTER JOIN strtgy_stp sn on sn.strtgy_stp_shrt_nm = stg.strtgy_nxt_stp_shrt_nm
							LEFT OUTER JOIN usr u on u.usr_usrnm = stg.cnsmr_accnt_lck_usrnm
							WHERE stg.grp_nmbr = @loop_count)


		--Titan-46445
			INSERT INTO cnsmr_accnt_crdt_bru_cnfg
           (cnsmr_accnt_id
           ,cnsmr_accnt_crdt_bru_prgd_flg
           ,upsrt_dttm
           ,upsrt_soft_comp_id
           ,upsrt_trnsctn_nmbr
           ,upsrt_usr_id)
		   select ca.cnsmr_accnt_id
		   ,'N'				
		   , ca.upsrt_dttm
		   , ca.upsrt_soft_comp_id
		   , ca.upsrt_trnsctn_nmbr
		   , ca.upsrt_usr_id
		   FROM dl_cnsmr_accnt_stgng stg
					INNER JOIN cnsmr_accnt ca ON ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
		   WHERE stg.grp_nmbr = @loop_count;


			IF @loop_count < @total_loops
			BEGIN
				SET @loop_count = @loop_count + 4
			END
			ELSE
			BEGIN
				SET @loop_count = 0
			END
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH
		SELECT @errNum = error_number()
		SELECT @errMsg = convert(varchar,@errNum) + '-' + error_message()
		SELECT error_number() ErrorNBR, error_severity() Severity,
			error_line() ErrorLine, error_message() Msg
		PRINT 'Error !!! - ' + @errMsg
		ROLLBACK TRAN
		SET rowcount 0
		EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Error', 'DL Load Accounts Part 2', @errMsg;
	END CATCH

END

END
GO

