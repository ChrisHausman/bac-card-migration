/****** Object: Procedure [dbo].[sp_dl_insert_consumers_part1_boa]   Script Date: 4/28/2023 1:24:46 PM ******/
USE [crs5_oltp];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
create PROCEDURE [dbo].[sp_dl_insert_consumers_part1_boa] @PassPhraseKey VARCHAR(128)
AS
BEGIN

DECLARE @loop_count as bigint
DECLARE @total_count as integer
DECLARE @total_loops as integer 
DECLARE @errNum int
DECLARE @errMsg varchar(1024)

DECLARE @StatusMsg as varchar(2500)

---- DEFAULT VALUES
DECLARE @Default_wrkgrp_id as bigint
DECLARE @Default_upsrt_soft_comp_id as bigint
DECLARE @Default_upsrt_usr_id as bigint
DECLARE @Default_new_bsnss_btch_id as bigint
DECLARE @tm datetime

SELECT @Default_wrkgrp_id = wrkgrp_id
FROM wrkgrp
WHERE wrkgrp_shrt_nm = 'DfltWkgp'

SELECT @Default_upsrt_soft_comp_id = upsrt_soft_comp_id
FROM lkup_soft_comp
WHERE upsrt_soft_comp_nm = 'load'

SELECT @Default_upsrt_usr_id = usr_id
FROM usr
WHERE usr_usrnm = 'CNVRSN'

select  @tm = dbo.getdate()

-------------
 

-- Consumer (Part 1 - Odd)

SET @loop_count = 1
SELECT @total_count = count(*) FROM dl_cnsmr_stgng

SELECT @total_loops = max(grp_nmbr) FROM dl_cnsmr_stgng


SET @StatusMsg =  N'Number of passes (cnsmr) : ' + convert(varchar,@total_loops)
PRINT @StatusMsg
EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Status', 'DL Load Consumers Part 1', @StatusMsg
 

WHILE @loop_count > 0
BEGIN
	BEGIN TRY
		BEGIN TRAN
			SET @StatusMsg =  N'Pass ' + convert(varchar,@loop_count) + ' of ' + convert(varchar,@total_loops) + ' at ' + convert(varchar, getdate(), 113)
			PRINT @StatusMsg
			EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Status', 'DL Load Consumers Part 1', @StatusMsg
   
   			INSERT INTO cnsmr
				(
				wrkgrp_id, 
				cnsmr_lst_wrkgrp_id, 
				cnsmr_bhvrl_score_nmbr, 
				cnsmr_brth_dt, 
				cnsmr_email_txt, 
				cnsmr_email_vldty_cd, 
				cnsmr_email_effctv_dttm, 
				cnsmr_idntfr_lgcy_txt, 
				cnsmr_idntfr_agncy_id, 
				cnsmr_idntfr_drvr_lcns_txt, 
				cnsmr_idntfr_drvr_lcns_issr_txt, 
				cnsmr_idntfr_pssprt_txt, 
				cnsmr_idntfr_ssn_txt,
				cnsmr_idntfr_scrd_ssn_txt,
				cnsmr_idntfr_hshd_ssn_txt,
				cnsmr_idntfr_cmmrcl_txt, 
				cnsmr_iscmmrcl_flg, 
				cnsmr_lang_spkn_cd, 
				cnsmr_lttr_mthd_cd, 
				cnsmr_mrtl_stts_cd, 
				cnsmr_nm_cmmrcl_txt, 
				cnsmr_nm_frst_txt, 
				cnsmr_nm_full_txt, 
				cnsmr_nm_full_altrnt_txt, 
				cnsmr_nm_lst_txt, 
				cnsmr_nm_mddl_txt, 
				cnsmr_nm_prfx_txt, 
				cnsmr_nm_sffx_txt, 
				cnsmr_use_attrny_flg, 
				agncy_entty_crrltn_id, 
				cnsmr_accss_pin_nmbr_txt, 
				upsrt_dttm, 
				upsrt_soft_comp_id, 
				upsrt_trnsctn_nmbr, 
				upsrt_usr_id, 
				cnsmr_email_optn_flg, 
				cnsmr_email_optn_dt, 
				cnsmr_tmzn_ovrrd_cd, 
				cnsmr_email_cnfrmtn_dt
				)
				(SELECT
					(CASE WHEN w.wrkgrp_id IS NULL THEN @Default_wrkgrp_id ELSE w.wrkgrp_id END), 
					(CASE WHEN lw.wrkgrp_id IS NULL THEN NULL ELSE lw.wrkgrp_id END), 
					(CASE WHEN cnsmr_bhvrl_score_nmbr IS NULL THEN 0 ELSE cnsmr_bhvrl_score_nmbr END), 
					cnsmr_brth_dt, 
					cnsmr_email_txt, 
					(CASE WHEN re.email_vldty_cd IS NULL THEN NULL ELSE re.email_vldty_cd END), 
					cnsmr_email_effctv_dttm, 
					cnsmr_idntfr_lgcy_txt, 
					cnsmr_idntfr_agncy_id, 
					cnsmr_idntfr_drvr_lcns_txt, 
					cnsmr_idntfr_drvr_lcns_issr_txt, 
					cnsmr_idntfr_pssprt_txt, 
					RIGHT(cnsmr_idntfr_ssn_txt, 4),
					dbo.fn_encrypt_data(cnsmr_idntfr_ssn_txt, @PassPhraseKey,'PERSONID'),
					LOWER(CONVERT(NVARCHAR(MAX),HASHBYTES('SHA1',REVERSE(cnsmr_idntfr_ssn_txt) + @PassPhraseKey), 2)),
					cnsmr_idntfr_cmmrcl_txt, 
					(CASE WHEN cnsmr_iscmmrcl_flg IS NULL THEN 'N' WHEN cnsmr_iscmmrcl_flg = ' ' THEN 'N' ELSE cnsmr_iscmmrcl_flg END), 
					(CASE WHEN rl.lang_cd IS NULL THEN 0 ELSE rl.lang_cd END),
					(CASE WHEN rm.lttr_mthd_cd IS NULL THEN 0 ELSE rm.lttr_mthd_cd END), 
					(CASE WHEN rs.mrtl_stts_cd IS NULL THEN 0 ELSE rs.mrtl_stts_cd END),
					cnsmr_nm_cmmrcl_txt, 
					cnsmr_nm_frst_txt, 
					cnsmr_nm_full_txt, 
					cnsmr_nm_full_altrnt_txt, 
					cnsmr_nm_lst_txt, 
					cnsmr_nm_mddl_txt, 
					cnsmr_nm_prfx_txt, 
					cnsmr_nm_sffx_txt, 
					(CASE WHEN cnsmr_use_attrny_flg IS NULL THEN 'N' WHEN cnsmr_use_attrny_flg  = ' ' THEN 'N' ELSE cnsmr_use_attrny_flg END), 
					agncy_entty_crrltn_id, 
					cnsmr_accss_pin_nmbr_txt, 
					GETDATE(),
					@Default_upsrt_soft_comp_id, 
					0, 
					@Default_upsrt_usr_id, 
					cnsmr_email_optn_flg, 
					cnsmr_email_optn_dt, 
					(CASE WHEN rt.tmzn_cd IS NULL THEN 0 ELSE rt.tmzn_cd END), 
					cnsmr_email_cnfrmtn_dt
				FROM dl_cnsmr_stgng stg
					LEFT OUTER JOIN wrkgrp w ON w.wrkgrp_shrt_nm = stg.wrkgrp_shrt_nm
					LEFT OUTER JOIN wrkgrp lw ON lw.wrkgrp_shrt_nm = stg.lst_wrkgrp_shrt_nm
					LEFT OUTER JOIN ref_email_vldty_cd re ON re.email_vldty_val_txt = stg.email_vldty_val_txt
					LEFT OUTER JOIN Ref_lang_cd rl ON rl.lang_val_txt = stg.lang_val_txt
					LEFT OUTER JOIN Ref_lttr_mthd_cd rm ON rm.lttr_mthd_val_txt = stg.lttr_mthd_val_txt
					LEFT OUTER JOIN Ref_mrtl_stts_cd rs ON rs.mrtl_stts_val_txt = stg.mrtl_stts_val_txt
					LEFT OUTER JOIN Ref_tmzn_cd rt ON rt.tmzn_val_txt = stg.tmzn_val_txt					
				WHERE grp_nmbr = @loop_count
				)	

			-- Create cnsmr_wrk_actn table
			INSERT INTO cnsmr_wrk_actn
				(
				cnsmr_id, 
				strtgy_id, 
				cnsmr_cntct_lst_dt, 
				cnsmr_cntct_nxt_dt, 
				cnsmr_frwrdd_flg, 
				cnsmr_hld_end_dt, 
				cnsmr_hld_unhld_rsn_txt, 
				cnsmr_is_hld_flg, 
				cnsmr_lck_flg, 
				cnsmr_lck_usrid, 
				cnsmr_lttr_cnt, 
				cnsmr_lttr_lst_dt, 
				cnsmr_lst_dial_dttm, 
				cnsmr_lst_dial_spprss_dttm, 
				cnsmr_lst_live_cnnct_dttm, 
				cnsmr_lst_pymnt_amnt, 
				cnsmr_lst_pymnt_dttm, 
				cnsmr_lst_sent_to_dialer_dttm, 
				cnsmr_strtgy_lst_run_dt, 
				cnsmr_strtgy_lst_stp_id, 
				cnsmr_strtgy_nxt_stp_id, 
				cnsmr_strtgy_schdld_pymnt_smmry_id, 
				cnsmr_skptrc_cd, 
				cnsmr_wrkd_lst_dt, 
				cnsmr_is_cllbl_flg, 
				cnsmr_prtl_lst_sccssfl_lgn_dttm, 
				cnsmr_prtl_lst_unsccssfl_lgn_dttm, 
				cnsmr_prtl_lckd_until_dttm, 
				cnsmr_prtl_lgn_attmpts_cnt, 
				cnsmr_is_pyble_flg, 
				cnsmr_crrnt_bal_amnt, 
				cnsmr_bal_dttm, 
				upsrt_dttm, 
				upsrt_soft_comp_id, 
				upsrt_trnsctn_nmbr, 
				upsrt_usr_id
				)
				(SELECT
					cnsmr_id, 
					NULL strtgy_id, 
					NULL cnsmr_cntct_lst_dt, 
					NULL cnsmr_cntct_nxt_dt, 
					'N' cnsmr_frwrdd_flg, 
					NULL cnsmr_hld_end_dt, 
					NULL cnsmr_hld_unhld_rsn_txt, 
					'N' cnsmr_is_hld_flg, 
					'N' cnsmr_lck_flg, 
					NULL usr_id, 
					0 cnsmr_lttr_cnt, 
					NULL cnsmr_lttr_lst_dt, 
					NULL cnsmr_lst_dial_dttm, 
					NULL cnsmr_lst_dial_spprss_dttm, 
					NULL cnsmr_lst_live_cnnct_dttm, 
					NULL cnsmr_lst_pymnt_amnt, 
					NULL cnsmr_lst_pymnt_dttm, 
					NULL cnsmr_lst_sent_to_dialer_dttm, 
					NULL cnsmr_strtgy_lst_run_dt, 
					NULL cnsmr_strtgy_lst_stp_id,
					NULL cnsmr_strtgy_nxt_stp_id,
					NULL, 
					NULL skptrc_cd,
					NULL cnsmr_wrkd_lst_dt, 
					'Y' cnsmr_is_cllbl_flg, 
					NULL cnsmr_prtl_lst_sccssfl_lgn_dttm, 
					NULL cnsmr_prtl_lst_unsccssfl_lgn_dttm, 
					NULL cnsmr_prtl_lckd_until_dttm, 
					NULL cnsmr_prtl_lgn_attmpts_cnt, 
					'Y' cnsmr_is_pyble_flg, 
					0 cnsmr_crrnt_bal_amnt, 
					NULL cnsmr_bal_dttm, 
					GETDATE(), 
					@Default_upsrt_soft_comp_id, 
					0, 
					@Default_upsrt_usr_id
				FROM dl_cnsmr_stgng stg
					 INNER JOIN cnsmr cs
					   ON cs.agncy_entty_crrltn_id = stg.agncy_entty_crrltn_id
					      AND cs.cnsmr_idntfr_drvr_lcns_txt = stg.cnsmr_idntfr_drvr_lcns_txt
				WHERE grp_nmbr = @loop_count
				)	
				
-- Titan-39971
            -- Create cnsmr_pymnt_wrk_actn table
			INSERT INTO dbo.cnsmr_pymnt_wrk_actn
				(cnsmr_id
				,cnsmr_pymnt_wrk_actn_trnsctn_dttm
				,upsrt_dttm
				,upsrt_soft_comp_id
				,upsrt_trnsctn_nmbr
				,upsrt_usr_id
				,crtd_usr_id
				,crtd_dttm)
				(SELECT cs.cnsmr_id
				,@tm
				,@tm
				,@Default_upsrt_soft_comp_id 
                ,0
                ,@Default_upsrt_usr_id 
				,@Default_upsrt_usr_id 
				,@tm
			FROM dl_cnsmr_stgng stg
			    INNER JOIN cnsmr cs
				  ON cs.agncy_entty_crrltn_id = stg.agncy_entty_crrltn_id
				     AND cs.cnsmr_idntfr_drvr_lcns_txt = stg.cnsmr_idntfr_drvr_lcns_txt
					 and stg.grp_nmbr = @loop_count
				--LEFT OUTER JOIN cnsmr_pymnt_wrk_actn cpwa on cpwa.cnsmr_id = cs.cnsmr_id
				--WHERE grp_nmbr = @loop_count and cpwa.cnsmr_id is null					
				)
		
			IF @loop_count < @total_loops
			BEGIN
				SET @loop_count = @loop_count + 2
			END
			ELSE
			BEGIN
				SET @loop_count = 0
			END
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH
		SELECT @errNum = error_number()
		SELECT @errMsg = convert(varchar,@errNum) + '-' + error_message()
		SELECT error_number() ErrorNBR, error_severity() Severity,
			error_line() ErrorLine, error_message() Msg
		PRINT 'Error !!! - ' + @errMsg
		ROLLBACK TRAN
		SET rowcount 0
		EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Error', 'DL Load Consumers Part 1', @errMsg
	END CATCH

END

END


GO

