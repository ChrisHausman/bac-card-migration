select distinct ca.cnsmr_accnt_id from cnsmr_accnt ca
inner join crdtr c on ca.crdtr_id = c.crdtr_id and c.crdtr_shrt_nm = 'CNSMRCRD'
inner join cnsmr_accnt_ownrs cao on ca.cnsmr_accnt_id = cao.cnsmr_accnt_id and cao.cnsmr_accnt_ownrshp_sft_dlt_flg = 'N' and cao.cnsmr_accnt_ownrshp_typ_cd = 1
inner join cnsmr cn on cao.cnsmr_id = cn.cnsmr_id
inner join cnsmr_accnt_wrk_actn cawa on ca.cnsmr_accnt_id = cawa.cnsmr_accnt_id
inner join cnsmr_accnt_frwrd_rcll_dtl cafrd on cawa.cnsmr_accnt_lst_frwrd_rcll_dtl_id = cafrd.cnsmr_accnt_frwrd_rcll_dtl_id
inner join rcvr r on cafrd.rcvr_id = r.rcvr_id and r.rcvr_typ_cd = (select rcvr_typ_cd from ref_rcvr_typ_cd where rcvr_typ_val_txt = 'Agency Vendor')
inner join rcvr_tag rt1 on r.rcvr_id = rt1.rcvr_id and rt1.rcvr_tag_sft_delete_flg = 'N' and rt1.tag_id = (select t1.tag_id from tag t1 where t1.tag_shrt_nm = 'DSA')
where 1 = 1
and not exists
(
	select 1
	from   cnsmr cn2
	       inner join cnsmr_accnt_ownrs cao2 on cn2.cnsmr_id = cao2.cnsmr_id and cao2.cnsmr_accnt_ownrshp_sft_dlt_flg = 'N'
		   inner join cnsmr_accnt_wrk_actn cawa2 on cao2.cnsmr_accnt_id = cawa2.cnsmr_accnt_id
		   left join UDEFCBR_ACCOUNT cbr on cao2.cnsmr_accnt_id = cbr.cnsmr_accnt_id
		   inner join cnsmr_accnt_frwrd_rcll_dtl cafrd2 on cawa2.cnsmr_accnt_lst_frwrd_rcll_dtl_id = cafrd2.cnsmr_accnt_frwrd_rcll_dtl_id
		   inner join cnsmr_accnt_tag fwd
             on cao2.cnsmr_accnt_id = fwd.cnsmr_accnt_id
                and fwd.cnsmr_accnt_sft_delete_flg = 'N'
	            and fwd.tag_id = (select tag_id from tag where tag_shrt_nm = 'FORWARD')
		   left join cnsmr_accnt_tag tagexc
		     on cao2.cnsmr_accnt_id = tagexc.cnsmr_accnt_id
				and tagexc.cnsmr_accnt_sft_delete_flg = 'N'
				and tagexc.tag_id in (select tag_id from tag where tag_shrt_nm in ('AGYPNSIF', 'FUTRPYMT'))
		   left join cnsmr_accnt_tag pndrc
             on cao2.cnsmr_accnt_id = pndrc.cnsmr_accnt_id
                and pndrc.cnsmr_accnt_sft_delete_flg = 'N'
	            and pndrc.tag_id = (select tag_id from tag where tag_shrt_nm = 'AGYPNDRC')
	where  cn.cnsmr_idntfr_lgcy_txt = cn2.cnsmr_idntfr_lgcy_txt
	       and len(rtrim(ltrim(isnull(cn2.cnsmr_idntfr_lgcy_txt, '')))) > 0
	       and cafrd.rcvr_id = cafrd2.rcvr_id
		   and (datediff(day, fwd.cnsmr_accnt_tag_assgn_dt, getdate()) < 125
		        or datediff(day, isnull(cbr.UDEFBASE_27_DT_LAST_PYMNT, ''), getdate()) <= 50
                or tagexc.cnsmr_accnt_tag_id is not null
				or pndrc.cnsmr_accnt_tag_id is null)
)
and day(getdate()) = 1