select distinct ca.cnsmr_accnt_id from cnsmr_accnt ca
inner join crdtr c on ca.crdtr_id = c.crdtr_id and c.crdtr_shrt_nm = 'CNSMRCRD'
inner join cnsmr_accnt_wrk_actn cawa on ca.cnsmr_accnt_id = cawa.cnsmr_accnt_id
left join UDEFCBR_ACCOUNT cbr on ca.cnsmr_accnt_id = cbr.cnsmr_accnt_id
inner join cnsmr_accnt_frwrd_rcll_dtl cafrd on cawa.cnsmr_accnt_lst_frwrd_rcll_dtl_id = cafrd.cnsmr_accnt_frwrd_rcll_dtl_id
inner join rcvr r on cafrd.rcvr_id = r.rcvr_id
inner join rcvr_tag rt1 on r.rcvr_id = rt1.rcvr_id and rt1.rcvr_tag_sft_delete_flg = 'N' and rt1.tag_id = (select t1.tag_id from tag t1 where t1.tag_shrt_nm = 'AGYRBP')
inner join rcvr_tag rt2 on r.rcvr_id = rt2.rcvr_id and rt2.rcvr_tag_sft_delete_flg = 'N' and rt2.tag_id = (select t2.tag_id from tag t2 where t2.tag_shrt_nm = 'Single')
inner join cnsmr_accnt_tag fwd
  on ca.cnsmr_accnt_id = fwd.cnsmr_accnt_id
     and fwd.cnsmr_accnt_sft_delete_flg = 'N'
	 and fwd.tag_id = (select tag_id from tag where tag_shrt_nm = 'FORWARD')
left join cnsmr_accnt_tag pends
  on ca.cnsmr_accnt_id = pends.cnsmr_accnt_id
     and pends.cnsmr_accnt_sft_delete_flg = 'N'
	 and pends.tag_id = (select tag_id from tag where tag_shrt_nm = 'AGYPNSIF')
left join cnsmr_accnt_tag futpy
  on ca.cnsmr_accnt_id = futpy.cnsmr_accnt_id
     and futpy.cnsmr_accnt_sft_delete_flg = 'N'
	 and futpy.tag_id = (select tag_id from tag where tag_shrt_nm = 'FUTRPYMT')
inner join cnsmr_accnt_tag pndrc
  on ca.cnsmr_accnt_id = pndrc.cnsmr_accnt_id
     and pndrc.cnsmr_accnt_sft_delete_flg = 'N'
	 and pndrc.tag_id = (select tag_id from tag where tag_shrt_nm = 'AGYPNDRC')
where datediff(day, fwd.cnsmr_accnt_tag_assgn_dt, getdate()) >= 95
      and datediff(day, isnull(cbr.UDEFBASE_27_DT_LAST_PYMNT, ''), getdate()) > 50
      and pends.cnsmr_accnt_tag_id is null
      and futpy.cnsmr_accnt_tag_id is null
	  and day(getdate()) = 1