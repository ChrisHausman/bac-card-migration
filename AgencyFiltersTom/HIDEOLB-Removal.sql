--Filter to remove HIDEOLB tag from accounts
select distinct ca.cnsmr_accnt_id from cnsmr_accnt ca
--select distinct ca.cnsmr_accnt_id, ca.cnsmr_accnt_chrg_off_dt, sr.REFUDEFKEY, cat.cnsmr_accnt_tag_id, cathst.cnsmr_accnt_tag_id, cadd.cnsmr_addrss_st_txt, uvl.udf_val_txt from cnsmr_accnt ca
inner join crdtr c on ca.crdtr_id = c.crdtr_id and c.crdtr_shrt_nm = 'CNSMRCRD'
inner join cnsmr_accnt_ownrs cao on ca.cnsmr_accnt_id = cao.cnsmr_accnt_id and cao.cnsmr_accnt_ownrshp_sft_dlt_flg = 'N' and cao.cnsmr_accnt_ownrshp_typ_cd = 1
inner join cnsmr_addrss cadd on cao.cnsmr_id = cadd.cnsmr_id
left join UDEFACCT_DETAILS ad on ca.cnsmr_accnt_id = ad.cnsmr_accnt_id
left join udf_val_lst uvl on ad.UDEFTBD_DOO = uvl.udf_val_list_id
inner join cnsmr_accnt_tag hide
  on ca.cnsmr_accnt_id = hide.cnsmr_accnt_id
     and hide.cnsmr_accnt_sft_delete_flg = 'N'
	 and hide.tag_id = (select tag_id from tag where tag_shrt_nm = 'HIDEOLB')
left join cnsmr_accnt_tag cat
  on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
     and cat.cnsmr_accnt_sft_delete_flg = 'N'
	 and cat.tag_id in (select tag_id from tag where tag_shrt_nm in (select REFUDEFTAG_SHRT_NM from REFUDEFOLB_EXCLSN_CC_TAGS where REFUDEFINCLD_HIST = 'N' and LNKD_ID is null))
left join cnsmr_accnt_tag cathst
  on ca.cnsmr_accnt_id = cathst.cnsmr_accnt_id
	 and cathst.tag_id in (select tag_id from tag where tag_shrt_nm in (select REFUDEFTAG_SHRT_NM from REFUDEFOLB_EXCLSN_CC_TAGS where REFUDEFINCLD_HIST = 'Y' and LNKD_ID is null))
where isnull(ca.cnsmr_accnt_chrg_off_dt, '') >= '2015-02-05'
      and cat.cnsmr_accnt_tag_id IS NULL
	  and (isnull(cadd.cnsmr_addrss_st_txt, '') not in ('WV', 'WI', 'MS') or isnull(uvl.udf_val_txt, '') not in ('Time Barred Debt', 'Date of Obsolescence'))
      and cathst.cnsmr_accnt_tag_id IS NULL
	  and not exists (select 1
	                  from UDEFTSYS_CC_STS_RSN_CDS udef
					       inner join REFUDEFOLB_EXCLSN_CC_TSYS_STSRSN sr
						     on udef.UDEFSTS_CD = sr.REFUDEFSTS_CD and udef.UDEFRSN_CD = sr.REFUDEFRSN_CD and sr.LNKD_ID is null
					  where udef.cnsmr_accnt_id = ca.cnsmr_accnt_id)