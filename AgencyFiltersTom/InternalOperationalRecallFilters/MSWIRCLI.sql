select distinct ca.cnsmr_accnt_id from cnsmr_accnt ca
inner join crdtr c on ca.crdtr_id = c.crdtr_id and c.crdtr_shrt_nm = 'CNSMRCRD'
inner join cnsmr_accnt_ownrs cao on ca.cnsmr_accnt_id = cao.cnsmr_accnt_id and cao.cnsmr_accnt_ownrshp_sft_dlt_flg = 'N' and cao.cnsmr_accnt_ownrshp_typ_cd = 1
inner join cnsmr_addrss cadd on cao.cnsmr_id = cadd.cnsmr_id and isnull(cadd.cnsmr_addrss_st_txt, '') in ('MS', 'WI')
inner join cnsmr_accnt_wrk_actn cawa on ca.cnsmr_accnt_id = cawa.cnsmr_accnt_id
inner join cnsmr_accnt_frwrd_rcll_dtl cafrd on cawa.cnsmr_accnt_lst_frwrd_rcll_dtl_id = cafrd.cnsmr_accnt_frwrd_rcll_dtl_id
inner join rcvr r on cafrd.rcvr_id = r.rcvr_id
inner join rcvr_grp rg on r.rcvr_grp_id = rg.rcvr_grp_id and rg.rcvr_grp_shrt_nm in ('INTLRCVY')
inner join cnsmr_accnt_tag cat
  on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
	 and cat.cnsmr_accnt_sft_delete_flg = 'N'
	 and cat.tag_id in (select tag_id from tag where tag_shrt_nm in ('AGYSOL'))
