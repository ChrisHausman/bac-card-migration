select distinct ca.cnsmr_accnt_id from cnsmr_accnt ca
inner join crdtr c on ca.crdtr_id = c.crdtr_id and c.crdtr_shrt_nm = 'CNSMRCRD'
inner join cnsmr_accnt_ownrs cao on ca.cnsmr_accnt_id = cao.cnsmr_accnt_id and cao.cnsmr_accnt_ownrshp_sft_dlt_flg = 'N' and cao.cnsmr_accnt_ownrshp_typ_cd = 1
inner join cnsmr cn on cao.cnsmr_id = cn.cnsmr_id
inner join cnsmr_accnt_wrk_actn cawa on ca.cnsmr_accnt_id = cawa.cnsmr_accnt_id
inner join cnsmr_accnt_frwrd_rcll_dtl cafrd on cawa.cnsmr_accnt_lst_frwrd_rcll_dtl_id = cafrd.cnsmr_accnt_frwrd_rcll_dtl_id
inner join rcvr r on cafrd.rcvr_id = r.rcvr_id
inner join rcvr_grp rg on r.rcvr_grp_id = rg.rcvr_grp_id and rg.rcvr_grp_shrt_nm in ('INTLRCVY')
left join cnsmr_accnt_tag cat
  on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
	 and cat.cnsmr_accnt_sft_delete_flg = 'N'
	 and cat.tag_id in (select tag_id from tag where tag_shrt_nm in ('CND', 'PENDDECD', 'DECEASED'))
where 1 = 1
and cat.cnsmr_accnt_tag_id is null
and exists
(
	select 1
	from   cnsmr cn2
	       inner join cnsmr_accnt_ownrs cao2 on cn2.cnsmr_id = cao2.cnsmr_id and cao2.cnsmr_accnt_ownrshp_sft_dlt_flg = 'N'
		   inner join cnsmr_accnt_wrk_actn cawa2 on cao2.cnsmr_accnt_id = cawa2.cnsmr_accnt_id
		   inner join cnsmr_accnt_frwrd_rcll_dtl cafrd2 on cawa2.cnsmr_accnt_lst_frwrd_rcll_dtl_id = cafrd2.cnsmr_accnt_frwrd_rcll_dtl_id
		   inner join rcvr r2 on cafrd2.rcvr_id = r2.rcvr_id
		   inner join rcvr_grp rg2 on r2.rcvr_grp_id = rg2.rcvr_grp_id and rg2.rcvr_grp_shrt_nm in ('INTLRCVY')
		   left join cnsmr_accnt_tag cat2
		     on cao2.cnsmr_accnt_id = cat2.cnsmr_accnt_id
				and cat2.cnsmr_accnt_sft_delete_flg = 'N'
				and cat2.tag_id in (select tag_id from tag where tag_shrt_nm in ('INCMPTNT', 'PERMHDSP'))
	where  cn.cnsmr_idntfr_lgcy_txt = cn2.cnsmr_idntfr_lgcy_txt
	       and (len(rtrim(ltrim(isnull(cn2.cnsmr_idntfr_lgcy_txt, '')))) > 0 or cn.cnsmr_id = cn2.cnsmr_id)
		   and cat2.cnsmr_accnt_tag_id is not null
)
