--A AND (B OR C OR D)
/*
A	Include all accounts not placed to a receiver
	Include accounts without a judgment (i.e., no JUDGMENT account tag)
	Include accounts without pending litigation (i.e., no DEFLIT AND LGESCLTN account tags)
	Include accounts without any closure tag (Tag Type = ‘CLOSURE’) currently present on the account
	
 	Previously Legal Population
B	Include accounts with a PREVLGL account tag if it charged off greater than or equal to 3 years ago
    AND has not made a payment in the last 3 years (Last Payment Date field is BASE_27_DT_LAST_PYMNT on CBR_ACCOUNT UDP)
	
 	Incapacity Population
C	Include accounts with a INCMPTNT account tag if it charged off greater than or equal to 3 years ago
    AND has not made a payment in the last 3 years (Last Payment Date field is BASE_27_DT_LAST_PYMNT on CBR_ACCOUNT UDP)
	
 	Remaining Population
D	Include accounts which charged off greater than or equal to 7 years ago
    AND has not made a payment in the last 7 years (Last Payment Date field is BASE_27_DT_LAST_PYMNT on CBR_ACCOUNT UDP)
*/

select distinct ca.cnsmr_accnt_id
from   cnsmr_accnt ca
       inner join cnsmr_accnt_wrk_actn cawa
	     on ca.cnsmr_accnt_id = cawa.cnsmr_accnt_id
		    and cawa.cnsmr_accnt_lst_frwrd_rcll_dtl_id is null
	   left join UDEFCBR_ACCOUNT cbra
	     on ca.cnsmr_accnt_Id = cbra.cnsmr_accnt_id
	   left join cnsmr_accnt_tag cat
	     on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
		    and cat.cnsmr_accnt_sft_delete_flg = 'N'
			and cat.tag_id in (select tag_id from tag where tag_shrt_nm in ('JUDGMENT', 'DEFLIT', 'LGESCLTN'))
	   left join cnsmr_accnt_tag previnc
	     on ca.cnsmr_accnt_id = previnc.cnsmr_accnt_id
		    and previnc.cnsmr_accnt_sft_delete_flg = 'N'
			and previnc.tag_id in (select tag_id from tag where tag_shrt_nm in ('PREVLGL', 'INCMPTNT'))
where  1 = 1
       and cat.cnsmr_accnt_tag_id is null
	   and ((previnc.cnsmr_accnt_tag_id is not null and isnull(cbra.UDEFBASE_27_DT_LAST_PYMNT, '') < getdate() - 1095 and isnull(ca.cnsmr_accnt_chrg_off_dt, '') <= getdate() - 1095)
	        or (isnull(cbra.UDEFBASE_27_DT_LAST_PYMNT, '') < getdate() - 2555 and isnull(ca.cnsmr_accnt_chrg_off_dt, '') <= getdate() - 2555))
	   and not exists (select 1
	                   from cnsmr_accnt_tag cat2
					   inner join tag t2 on cat2.tag_id = t2.tag_id
					   inner join tag_typ tt2 on t2.tag_typ_Id = tt2.tag_typ_id and tt2.tag_typ_shrt_nm = 'CLOSURE'
					   where ca.cnsmr_accnt_id = cat2.cnsmr_accnt_id and cat2.cnsmr_accnt_sft_delete_flg = 'N')


