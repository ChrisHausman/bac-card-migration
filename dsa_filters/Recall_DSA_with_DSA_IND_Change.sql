select distinct ca.cnsmr_accnt_id from cnsmr_accnt ca
inner join crdtr c on ca.crdtr_id = c.crdtr_id and c.crdtr_shrt_nm = 'CNSMRCRD'
inner join cnsmr_accnt_ownrs cao on ca.cnsmr_accnt_id = cao.cnsmr_accnt_id and cao.cnsmr_accnt_ownrshp_sft_dlt_flg = 'N' and cao.cnsmr_accnt_ownrshp_typ_cd = 1
inner join cnsmr cn on cao.cnsmr_id = cn.cnsmr_id
inner join cnsmr_accnt_wrk_actn cawa on ca.cnsmr_accnt_id = cawa.cnsmr_accnt_id
inner join cnsmr_accnt_frwrd_rcll_dtl cafrd on cawa.cnsmr_accnt_lst_frwrd_rcll_dtl_id = cafrd.cnsmr_accnt_frwrd_rcll_dtl_id
inner join rcvr r on cafrd.rcvr_id = r.rcvr_id and r.rcvr_typ_cd <> (select rcvr_typ_cd from ref_rcvr_typ_cd where rcvr_typ_val_txt = 'LEGAL_VENDOR')
inner join rcvr_tag rt on r.rcvr_id = rt.rcvr_id and rt.rcvr_tag_sft_delete_flg = 'N' and rt.tag_id = (select tag_id from tag where tag_shrt_nm = 'DSA')
inner join UDEFDEBT_SETTLEMENT_AGENCY udef on ca.cnsmr_accnt_id = udef.cnsmr_accnt_id
inner join
(
	select udef.id,
	       udef.UDEFDSA_IND,
		   row_number() over (partition by udef.id order by adt_dttm desc) rnk
	from   UDEFDEBT_SETTLEMENT_AGENCY_hst udef
	where  udef.upsrt_chng_fld_txt like '%UDEFDSA_IND%'
) hst
  on udef.id = hst.id
     and hst.rnk = 1
where 1 = 1
      and ((isnull(udef.UDEFDSA_IND, '') in ('I', 'L', 'U', 'V') AND isnull(hst.UDEFDSA_IND, '') = 'C')
	       or (isnull(udef.UDEFDSA_IND, '') = 'C' AND isnull(hst.UDEFDSA_IND, '') in ('I', 'L', 'U', 'V')))
and not exists
(
	select 1
	from   cnsmr cn2
	       inner join cnsmr_accnt_ownrs cao2 on cn2.cnsmr_id = cao2.cnsmr_id and cao2.cnsmr_accnt_ownrshp_sft_dlt_flg = 'N'
		   inner join cnsmr_accnt_wrk_actn cawa2 on cao2.cnsmr_accnt_id = cawa2.cnsmr_accnt_id
		   left join cnsmr_accnt_frwrd_rcll_dtl cafrd2 on cawa2.cnsmr_accnt_lst_frwrd_rcll_dtl_id = cafrd2.cnsmr_accnt_frwrd_rcll_dtl_id
		   left join rcvr r2 on cafrd2.rcvr_id = r2.rcvr_id and r2.rcvr_typ_cd <> (select rcvr_typ_cd from ref_rcvr_typ_cd where rcvr_typ_val_txt = 'LEGAL_VENDOR')
		   left join rcvr_tag rt2 on cafrd2.rcvr_id = rt2.rcvr_id and rt2.rcvr_tag_sft_delete_flg = 'N' and rt2.tag_id = (select tag_id from tag where tag_shrt_nm = 'DSA')
		   left join UDEFDEBT_SETTLEMENT_AGENCY udef on cao2.cnsmr_accnt_id = udef.cnsmr_accnt_id
		   left join cnsmr_accnt_tag cls
		     on cao2.cnsmr_accnt_id = cls.cnsmr_accnt_id
			    and cls.cnsmr_accnt_sft_delete_flg = 'N'
				and cls.tag_id in (select tag_id from tag where tag_typ_id in (select tag_typ_id from tag_typ where tag_typ_shrt_nm = 'CLOSURE'))
	where  cn.cnsmr_idntfr_lgcy_txt = cn2.cnsmr_idntfr_lgcy_txt
		   and len(rtrim(ltrim(isnull(cn2.cnsmr_idntfr_lgcy_txt, '')))) > 0
		   and cls.cnsmr_accnt_tag_id is null
		   and (isnull(udef.UDEFDSA_IND, '') not in ('I', 'L', 'U', 'V', 'C') or r2.rcvr_id is null or rt2.rcvr_tag_id is null)
)