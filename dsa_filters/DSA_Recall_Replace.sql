select distinct ca.cnsmr_accnt_id from cnsmr_accnt ca
inner join crdtr c on ca.crdtr_id = c.crdtr_id and c.crdtr_shrt_nm = 'CNSMRCRD'
inner join cnsmr_accnt_wrk_actn cawa on ca.cnsmr_accnt_id = cawa.cnsmr_accnt_id
inner join cnsmr_accnt_frwrd_rcll_dtl cafrd on cawa.cnsmr_accnt_lst_frwrd_rcll_dtl_id = cafrd.cnsmr_accnt_frwrd_rcll_dtl_id
inner join rcvr r on cafrd.rcvr_id = r.rcvr_id and r.rcvr_shrt_nm in ('DSA', 'ISU')
inner join UDEFVENDOR_PLACEMENT udef on ca.cnsmr_accnt_id = udef.cnsmr_accnt_id
inner join udf_val_lst uvl on udef.UDEFRCVR_SHRT_NM = uvl.udf_val_list_id
inner join rcvr r2 on isnull(uvl.udf_val_txt, '') = r2.rcvr_shrt_nm
inner join rcvr_tag rt on r2.rcvr_id = rt.rcvr_id and rt.rcvr_tag_sft_delete_flg = 'N'
inner join tag t on rt.tag_id = t.tag_id and t.tag_shrt_nm = 'DSA'
where 1 = 1
and isnull(uvl.udf_val_txt, '') not in ('DSA', 'ISU')