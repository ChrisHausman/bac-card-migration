use dm_oltp;
go

-- drop existing staging table if present
if object_id('ETL.mu_cnsmr_accnt_Tag', 'U') is not null
    drop table ETL.mu_cnsmr_accnt_tag;

-- create the staging table
create table ETL.mu_cnsmr_accnt_tag
(
	cnsmr_accnt_tag_id              bigint identity(1, 1) not null
    ,cnsmr_accnt_idntfr_lgcy_txt    varchar(40) not null
    ,tag_shrt_nm                    varchar(8) not null
    ,cnsmr_accnt_tag_assgn_dt       datetime null
    ,cnsmr_accnt_tag_ordr_nmbr      int null
    ,cnsmr_accnt_tag_sft_delete_flg char(1) not null
    ,upsrt_btch_nmbr                bigint null
);

alter table ETL.mu_cnsmr_accnt_tag
add constraint df_mu_cnsmr_accnt_tag_sft_delete_flg default 'N' for cnsmr_accnt_tag_sft_delete_flg;
go

-- create the processing stored procedure
use dm_oltp;
go

if object_id('ETL.sp_mu_cnsmr_accnt_Tag', 'P') is not null
    drop procedure ETL.sp_mu_cnsmr_accnt_tag;
go

/* ****************************************************************************************************************** */
/*   Name: Migration Utility - cnsmr_accnt_Tag - Processing                                                           */
/*   Staging Table: ETL.mu_cnsmr_accnt_Tag                                                                            */
/*   Processing Stored Procedure: ETL.sp_mu_cnsmr_accnt_Tag                                                           */
/*   Date:  07/07/2020                                                                                                */
/*   Purpose:  This script is designed to create the staging table and processing stored procedure                    */
/*             for the migration utility (MU) to migrate the cnsmr_accnt_Tag entity.                                  */
/*   Change Log:                                                                                                      */
/*       Version     Description                         Developer               Date                                 */
/*       ------------------------------------------------------------------------------------------------------------ */
/*       11.1        Initial - for DM 11.1               Doug Barlett            07/07/2020                           */
/*       11.1.BAC    Updated for BAC's TSYS Migration    Chris Hausman           12/29/2020                           */
/*       11.1.BAC    Fixed bug with error logging.       Chris Hausman           04/29/2022                           */
/*       11.2.BAC    Direct to table logging.            Andrew Sheldon          04/11/2023                           */
/*       ------------------------------------------------------------------------------------------------------------ */
/*  TO DO LIST                                                                                                        */
/*  ----------                                                                                                        */
/*   (1) Are any account level system codes being used? If not the second half of this script can be removed.         */
/*   (2) Would it be more efficient to implement a temp table that houses each chunk, and join to that rather than    */
/*       the main MU staging (that'll be huge).                                                                       */
/* ****************************************************************************************************************** */

create   procedure [etl].[sp_mu_cnsmr_accnt_tag] @chunk_size bigint = 10000, @process_system_cd bit = 1
as
    begin
        declare @err_msg nvarchar(4000)= N'';
        declare @err_severity int;
        declare @err_num int;

        declare @msg_tbl_nm nvarchar(50) = N'cnsmr_accnt_tag';

        declare @start_time datetime;
        declare @run_time varchar(20);
        declare @loop_count int;
        declare @total_count bigint;
        declare @total_loops int;
        declare @start_key bigint;

        declare @default_upsrt_soft_comp_id bigint;
        declare @default_upsrt_usr_id bigint;

        declare @first_party_collections_agncy_config bigint;
        declare @first_party_collections char(1);
        declare @defaulted_and_small_balance_write_off smallint;
        declare @paid_in_full smallint;
        declare @deleted_account smallint;
        declare @not_defaulted_and_small_balance_write_off_or_pif smallint;
        declare @account_closed_due_to_refinance smallint;
        
        declare @current_datetime datetime = dbo.getcurrdt();

        /**************************************************************************
            Create permanent logging table just for secondary ownership step.  
        **************************************************************************/

        --This table will keep track of ETL progress.
        if not exists (select 1 from information_schema.tables where table_name = 'mu_Processing_Log')
        begin
            create table ETL.mu_processing_log
            (
				pklog         numeric(18, 0) identity(1, 1) not null
                ,messagetype  nvarchar(50) not null
                ,logmessage   nvarchar(1000) null
                ,severity     int null
                ,logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
                ,logtime      datetime not null default getdate()
            );
        end;

        /************************************************************************
            Create local logging table.
        ************************************************************************/

        -- The following table variable lives outside the overall transaction and allows us to keep log messages through roll back.
        declare @logsystem table
        (
			pklog         numeric(18, 0) not null identity(1, 1)
            ,messagetype  nvarchar(50) not null
            ,logmessage   nvarchar(1000) not null
            ,severity     int null
            ,logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
            ,logtime      datetime not null default getdate()
        );

        -- The following table variable is used to determine system code status on tags being assigned and apply that status to the cnsmr_wrk_actn record.
        declare @cnsmr_accnt_list table
        (
			cnsmr_accnt_id bigint not null
            ,primary key(cnsmr_accnt_id)
        );

        declare @system_code_flags table
        (
			cnsmr_accnt_id                   bigint not null
            ,systm_cd_is_entty_actv_flg      char(1) not null
            ,systm_cd_is_pybl_flg            char(1) not null
            ,systm_cd_incld_in_cnsmr_bal_flg char(1) not null
        );

        select @first_party_collections_agncy_config = config_item_id
        from config_item
        where config_item_rsrc_ky_txt = 'config.credit.bureau.first.party.account.status.key';

        select @first_party_collections = agncy_config_item_crrnt_val_txt
        from agncy_config
        where config_item_id = @first_party_collections_agncy_config;

        select @defaulted_and_small_balance_write_off = cb_rpt_accnt_stts_cd
        from ref_cb_rpt_accnt_stts_cd
        where cb_rpt_accnt_stts_val_txt = '64';

        select @paid_in_full = cb_rpt_accnt_stts_cd
        from ref_cb_rpt_accnt_stts_cd
        where cb_rpt_accnt_stts_val_txt = '62';

        select @deleted_account = cb_rpt_accnt_stts_cd
        from ref_cb_rpt_accnt_stts_cd
        where cb_rpt_accnt_stts_val_txt = 'DA';

        select @not_defaulted_and_small_balance_write_off_or_pif = cb_rpt_accnt_stts_cd
        from ref_cb_rpt_accnt_stts_cd
        where cb_rpt_accnt_stts_val_txt = '13';

        select @account_closed_due_to_refinance = cb_rpt_accnt_spcl_cmmnt_cd
        from ref_cb_rpt_accnt_spcl_cmmnt_cd
        where cb_rpt_accnt_spcn_cmmnt_val_txt = 'AS';

        insert into @cnsmr_accnt_list (cnsmr_accnt_id)
        select distinct ca.cnsmr_accnt_id
        from   ETL.mu_cnsmr_accnt_tag stg
               inner join dbo.cnsmr_accnt ca
			     on ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt;

        -- create an index on the staging table as recommended by the Explain Plan
        if not exists (select 1 from sys.indexes where object_id = object_id('ETL.mu_cnsmr_accnt_Tag') and name = 'idx01_mu_cnsmr_accnt_Tag')
            create nonclustered index idx01_mu_cnsmr_accnt_tag on ETL.mu_cnsmr_accnt_tag(cnsmr_accnt_tag_id) include(cnsmr_accnt_idntfr_lgcy_txt);
        else
            alter index idx01_mu_cnsmr_accnt_tag on ETL.mu_cnsmr_accnt_tag rebuild;


        begin try
            select @start_time = @current_datetime;

            select @default_upsrt_soft_comp_id = upsrt_soft_comp_id
            from lkup_soft_comp
            where upsrt_soft_comp_nm = 'load';

            select @default_upsrt_usr_id = usr_id
            from usr
            where usr_usrnm = 'system';

            -- setup the looping variables to process in chucks
            set @loop_count = 1;

            select @total_count = count(*) from ETL.mu_cnsmr_accnt_tag;

            select @start_key = min(cnsmr_accnt_tag_id) from ETL.mu_cnsmr_accnt_tag;

            set @total_loops = @total_count / @chunk_size + 1;

            insert into ETL.mu_processing_log(messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - START',
				'Starting ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

            while @loop_count > 0
            begin
                begin transaction;

                -- create the cnsmr_accnt_Tag record
                
                insert into dbo.cnsmr_accnt_tag
				(
					cnsmr_accnt_id
                    ,tag_id
                    ,cnsmr_accnt_tag_assgn_dt
                    ,cnsmr_accnt_tag_ordr_nmbr
                    ,cnsmr_accnt_sft_delete_flg
                    ,upsrt_btch_nmbr
                    ,upsrt_dttm
                    ,upsrt_soft_comp_id
                    ,upsrt_trnsctn_nmbr
                    ,upsrt_usr_id
				)
                select ca.cnsmr_accnt_id as                                cnsmr_accnt_id
                       ,t.tag_id as                                        tag_id
					   ,isnull(stg.cnsmr_accnt_tag_assgn_dt, getdate()) as cnsmr_accnt_tag_assgn_dt
                       ,stg.cnsmr_accnt_tag_ordr_nmbr as                   cnsmr_accnt_tag_ordr_nmbr
                       ,stg.cnsmr_accnt_tag_sft_delete_flg as              cnsmr_accnt_sft_delete_flg
                       ,stg.upsrt_btch_nmbr as                             upsrt_btch_nmbr
                       ,@current_datetime as                               upsrt_dttm
                       ,@default_upsrt_soft_comp_id as                     upsrt_soft_comp_id
                       ,1 as                                               upsrt_trnsctn_nmbr
                       ,@default_upsrt_usr_id as                           upsrt_usr_id
                from etl.mu_cnsmr_accnt_tag stg
                     inner join dbo.cnsmr_accnt ca
					   on ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
                     inner join dbo.tag t
					   on t.tag_shrt_nm = stg.tag_shrt_nm
                where stg.cnsmr_accnt_tag_id >= @start_key
                      and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                insert into ETL.mu_processing_log(messagetype, logmessage, logginglevel)
                values
				(
					@msg_tbl_nm,
					'Completed insert into ' + @msg_tbl_nm
					 + ',  ' + convert(nvarchar, @@rowcount)
					 + ' rows inserted.  Loop: ' + convert(nvarchar, @loop_count),
					3
				);

                -- Based on the passed in "@process_system_cd" (defaults to 1 (true))
                -- update the cnsmr_accnt records for those accounts assigned tags with system codes attached
                if @process_system_cd = 1
                begin
                    -- bought flag
                    update ca
                        set 
                          ca.cnsmr_accnt_is_bght_flg = 'Y'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where sc.systm_cd_shrt_nm = 'Bought'
                            and cat.cnsmr_accnt_sft_delete_flg = 'N'
                            and t.tag_actv_flg = 'Y'
                            and sc.systm_cd_is_actv_flg = 'Y'
                            and ca.cnsmr_accnt_is_bght_flg <> 'Y'
                            and stg.cnsmr_accnt_tag_id >= @start_key
                            and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt Bought flag = Y, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    update ca
                        set 
                          ca.cnsmr_accnt_is_bght_flg = 'N'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where not exists
                    (
                        select 1
                        from cnsmr_accnt_tag cat
                                inner join tag t on cat.tag_id = t.tag_id
                                                    and cat.cnsmr_accnt_id = cl.cnsmr_accnt_id
                                inner join systm_cd sc on t.systm_cd = sc.systm_cd
                        where sc.systm_cd_shrt_nm = 'Bought'
                                and cat.cnsmr_accnt_sft_delete_flg <> 'Y'
                                and t.tag_actv_flg = 'Y'
                                and sc.systm_cd_is_actv_flg = 'Y'
                    )
                        and ca.cnsmr_accnt_is_bght_flg <> 'N'
                        and stg.cnsmr_accnt_tag_id >= @start_key
                        and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt Bought flag = N, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    -- bankrupt flag
                    update ca
                        set 
                          ca.cnsmr_accnt_is_bnkrpt_flg = 'Y'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where sc.systm_cd_shrt_nm = 'Bankrupt'
                            and cat.cnsmr_accnt_sft_delete_flg = 'N'
                            and t.tag_actv_flg = 'Y'
                            and sc.systm_cd_is_actv_flg = 'Y'
                            and ca.cnsmr_accnt_is_bnkrpt_flg <> 'Y'
                            and stg.cnsmr_accnt_tag_id >= @start_key
                            and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt Bankrupt flag = Y, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    update ca
                        set 
                          ca.cnsmr_accnt_is_bnkrpt_flg = 'N'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where not exists
                    (
                        select 1
                        from cnsmr_accnt_tag cat
                                inner join tag t on cat.tag_id = t.tag_id
                                                    and cat.cnsmr_accnt_id = cl.cnsmr_accnt_id
                                inner join systm_cd sc on t.systm_cd = sc.systm_cd
                        where sc.systm_cd_shrt_nm = 'Bankrupt'
                                and cat.cnsmr_accnt_sft_delete_flg <> 'Y'
                                and t.tag_actv_flg = 'Y'
                                and sc.systm_cd_is_actv_flg = 'Y'
                    )
                        and ca.cnsmr_accnt_is_bnkrpt_flg <> 'N'
                        and stg.cnsmr_accnt_tag_id >= @start_key
                        and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt Bankrupt flag = N, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    -- chargeoff flag
                    update ca
                        set 
                          ca.cnsmr_accnt_is_chrgd_off_flg = 'Y'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where sc.systm_cd_shrt_nm = 'ChargOff'
                            and cat.cnsmr_accnt_sft_delete_flg = 'N'
                            and t.tag_actv_flg = 'Y'
                            and sc.systm_cd_is_actv_flg = 'Y'
                            and ca.cnsmr_accnt_is_chrgd_off_flg <> 'Y'
                            and stg.cnsmr_accnt_tag_id >= @start_key
                            and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt ChargeOff flag = Y, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    update ca
                        set 
                          ca.cnsmr_accnt_is_chrgd_off_flg = 'N'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where not exists
                    (
                        select 1
                        from cnsmr_accnt_tag cat
                                inner join tag t on cat.tag_id = t.tag_id
                                                    and cat.cnsmr_accnt_id = cl.cnsmr_accnt_id
                                inner join systm_cd sc on t.systm_cd = sc.systm_cd
                        where sc.systm_cd_shrt_nm = 'ChargOff'
                                and cat.cnsmr_accnt_sft_delete_flg <> 'Y'
                                and t.tag_actv_flg = 'Y'
                                and sc.systm_cd_is_actv_flg = 'Y'
                    )
                        and ca.cnsmr_accnt_is_chrgd_off_flg <> 'N'
                        and stg.cnsmr_accnt_tag_id >= @start_key
                        and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt ChargeOff flag = N, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    update ca
                        set 
                          ca.cb_rpt_accnt_stts_cd = @defaulted_and_small_balance_write_off
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where sc.systm_cd_shrt_nm = 'ChargOff'
                            and ca.cb_rpt_accnt_stts_cd not in(@deleted_account, @paid_in_full, @not_defaulted_and_small_balance_write_off_or_pif, @defaulted_and_small_balance_write_off)
                    and @first_party_collections = 'Y'
                    and stg.cnsmr_accnt_tag_id >= @start_key
                    and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt CB Status for ChargeOff, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    -- closed flag
                    update ca
                        set 
                          ca.cnsmr_accnt_is_clsd_flg = 'Y'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where sc.systm_cd_shrt_nm = 'Closed'
                            and cat.cnsmr_accnt_sft_delete_flg = 'N'
                            and t.tag_actv_flg = 'Y'
                            and sc.systm_cd_is_actv_flg = 'Y'
                            and ca.cnsmr_accnt_is_clsd_flg <> 'Y'
                            and stg.cnsmr_accnt_tag_id >= @start_key
                            and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt Closed flag = Y, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    update ca
                        set 
                          ca.cnsmr_accnt_is_clsd_flg = 'N'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where not exists
                    (
                        select 1
                        from cnsmr_accnt_tag cat
                                inner join tag t on cat.tag_id = t.tag_id
                                                    and cat.cnsmr_accnt_id = cl.cnsmr_accnt_id
                                inner join systm_cd sc on t.systm_cd = sc.systm_cd
                        where sc.systm_cd_shrt_nm = 'Closed'
                                and cat.cnsmr_accnt_sft_delete_flg <> 'Y'
                                and t.tag_actv_flg = 'Y'
                                and sc.systm_cd_is_actv_flg = 'Y'
                    )
                        and ca.cnsmr_accnt_is_clsd_flg <> 'N'
                        and stg.cnsmr_accnt_tag_id >= @start_key
                        and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt Closed flag = N, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    -- returned flag
                    update ca
                        set 
                          ca.cnsmr_accnt_is_rtrnd_flg = 'Y'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where sc.systm_cd_shrt_nm = 'Returned'
                            and cat.cnsmr_accnt_sft_delete_flg = 'N'
                            and t.tag_actv_flg = 'Y'
                            and sc.systm_cd_is_actv_flg = 'Y'
                            and ca.cnsmr_accnt_is_rtrnd_flg <> 'Y'
                            and stg.cnsmr_accnt_tag_id >= @start_key
                            and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt Returned flag = Y, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    update ca
                        set 
                          ca.cnsmr_accnt_is_rtrnd_flg = 'N'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where not exists
                    (
                        select 1
                        from cnsmr_accnt_tag cat
                                inner join tag t on cat.tag_id = t.tag_id
                                                    and cat.cnsmr_accnt_id = cl.cnsmr_accnt_id
                                inner join systm_cd sc on t.systm_cd = sc.systm_cd
                        where sc.systm_cd_shrt_nm = 'Returned'
                                and cat.cnsmr_accnt_sft_delete_flg <> 'Y'
                                and t.tag_actv_flg = 'Y'
                                and sc.systm_cd_is_actv_flg = 'Y'
                    )
                        and ca.cnsmr_accnt_is_rtrnd_flg <> 'N'
                        and stg.cnsmr_accnt_tag_id >= @start_key
                        and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt Returned flag = N, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    -- disput flag
                    update ca
                        set 
                          ca.cnsmr_accnt_is_dsptd_flg = 'Y'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where sc.systm_cd_shrt_nm = 'Disput'
                            and cat.cnsmr_accnt_sft_delete_flg = 'N'
                            and t.tag_actv_flg = 'Y'
                            and sc.systm_cd_is_actv_flg = 'Y'
                            and ca.cnsmr_accnt_is_dsptd_flg <> 'Y'
                            and stg.cnsmr_accnt_tag_id >= @start_key
                            and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt Disput flag = Y, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    update ca
                        set 
                          ca.cnsmr_accnt_is_dsptd_flg = 'N'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where not exists
                    (
                        select 1
                        from cnsmr_accnt_tag cat
                                inner join tag t on cat.tag_id = t.tag_id
                                                    and cat.cnsmr_accnt_id = cl.cnsmr_accnt_id
                                inner join systm_cd sc on t.systm_cd = sc.systm_cd
                        where sc.systm_cd_shrt_nm = 'Disput'
                                and cat.cnsmr_accnt_sft_delete_flg <> 'Y'
                                and t.tag_actv_flg = 'Y'
                                and sc.systm_cd_is_actv_flg = 'Y'
                    )
                        and ca.cnsmr_accnt_is_dsptd_flg <> 'N'
                        and stg.cnsmr_accnt_tag_id >= @start_key
                        and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt Disput flag = N, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    -- rollover flag
                    update ca
                        set 
                          ca.cnsmr_accnt_is_rllvr_flg = 'Y'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where sc.systm_cd_shrt_nm = 'RollOver'
                            and cat.cnsmr_accnt_sft_delete_flg = 'N'
                            and t.tag_actv_flg = 'Y'
                            and sc.systm_cd_is_actv_flg = 'Y'
                            and ca.cnsmr_accnt_is_rllvr_flg <> 'Y'
                            and stg.cnsmr_accnt_tag_id >= @start_key
                            and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt RollOver flag = Y, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    update ca
                        set 
                          ca.cnsmr_accnt_is_rllvr_flg = 'N'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where not exists
                    (
                        select 1
                        from cnsmr_accnt_tag cat
                                inner join tag t on cat.tag_id = t.tag_id
                                                    and cat.cnsmr_accnt_id = cl.cnsmr_accnt_id
                                inner join systm_cd sc on t.systm_cd = sc.systm_cd
                        where sc.systm_cd_shrt_nm = 'RollOver'
                                and cat.cnsmr_accnt_sft_delete_flg <> 'Y'
                                and t.tag_actv_flg = 'Y'
                                and sc.systm_cd_is_actv_flg = 'Y'
                    )
                        and ca.cnsmr_accnt_is_rllvr_flg <> 'N'
                        and stg.cnsmr_accnt_tag_id >= @start_key
                        and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt RollOver flag = N, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    -- refin flag
                    update ca
                        set 
                          ca.cnsmr_accnt_is_refi_flg = 'Y'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where sc.systm_cd_shrt_nm = 'Refin'
                            and cat.cnsmr_accnt_sft_delete_flg = 'N'
                            and t.tag_actv_flg = 'Y'
                            and sc.systm_cd_is_actv_flg = 'Y'
                            and ca.cnsmr_accnt_is_refi_flg <> 'Y'
                            and stg.cnsmr_accnt_tag_id >= @start_key
                            and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt Refin flag = Y, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    update ca
                        set 
                          ca.cnsmr_accnt_is_refi_flg = 'N'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where not exists
                    (
                        select 1
                        from cnsmr_accnt_tag cat
                                inner join tag t on cat.tag_id = t.tag_id
                                                    and cat.cnsmr_accnt_id = cl.cnsmr_accnt_id
                                inner join systm_cd sc on t.systm_cd = sc.systm_cd
                        where sc.systm_cd_shrt_nm = 'Refin'
                                and cat.cnsmr_accnt_sft_delete_flg <> 'Y'
                                and t.tag_actv_flg = 'Y'
                                and sc.systm_cd_is_actv_flg = 'Y'
                    )
                        and ca.cnsmr_accnt_is_refi_flg <> 'N'
                        and stg.cnsmr_accnt_tag_id >= @start_key
                        and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt Refin flag = N, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    update ca
                        set 
                          ca.cb_rpt_accnt_stts_cd = @defaulted_and_small_balance_write_off
                        , ca.cb_rpt_accnt_spcl_cmmnt_cd = @account_closed_due_to_refinance
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where sc.systm_cd_shrt_nm = 'Refin'
                            and ca.cb_rpt_accnt_stts_cd not in(@deleted_account, @paid_in_full, @not_defaulted_and_small_balance_write_off_or_pif, @defaulted_and_small_balance_write_off)
                    and @first_party_collections = 'Y'
                    and stg.cnsmr_accnt_tag_id >= @start_key
                    and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt CB Status for Refin, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    -- precollt flag
                    update ca
                        set 
                          ca.cnsmr_accnt_pre_cllct_flg = 'Y'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where sc.systm_cd_shrt_nm = 'PreCollt'
                            and cat.cnsmr_accnt_sft_delete_flg = 'N'
                            and t.tag_actv_flg = 'Y'
                            and sc.systm_cd_is_actv_flg = 'Y'
                            and ca.cnsmr_accnt_pre_cllct_flg <> 'Y'
                            and stg.cnsmr_accnt_tag_id >= @start_key
                            and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt PreCollt flag = Y, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    update ca
                        set 
                          ca.cnsmr_accnt_pre_cllct_flg = 'N'
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                        , ca.upsrt_dttm = getdate()
                    from cnsmr_accnt ca
                            inner join @cnsmr_accnt_list cl on ca.cnsmr_accnt_id = cl.cnsmr_accnt_id
                            inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                            inner join cnsmr_accnt_tag cat on ca.cnsmr_accnt_id = cat.cnsmr_accnt_id
                            inner join tag t on cat.tag_id = t.tag_id
                            inner join systm_cd sc on t.systm_cd = sc.systm_cd
                    where not exists
                    (
                        select 1
                        from cnsmr_accnt_tag cat
                                inner join tag t on cat.tag_id = t.tag_id
                                                    and cat.cnsmr_accnt_id = cl.cnsmr_accnt_id
                                inner join systm_cd sc on t.systm_cd = sc.systm_cd
                        where sc.systm_cd_shrt_nm = 'PreCollt'
                                and cat.cnsmr_accnt_sft_delete_flg <> 'Y'
                                and t.tag_actv_flg = 'Y'
                                and sc.systm_cd_is_actv_flg = 'Y'
                    )
                        and ca.cnsmr_accnt_pre_cllct_flg <> 'N'
                        and stg.cnsmr_accnt_tag_id >= @start_key
                        and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt PreCollt flag = N, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    -- others
                    delete from @system_code_flags;

                    insert into @system_code_flags(cnsmr_accnt_id
                                                    , systm_cd_is_entty_actv_flg
                                                    , systm_cd_is_pybl_flg
                                                    , systm_cd_incld_in_cnsmr_bal_flg)
                    (
                        select cal.cnsmr_accnt_id
                                , case
                                    when exists
                                        (
                                            select sc.systm_cd_is_entty_actv_flg
                                            from cnsmr_accnt_tag cat
                                                    inner join tag t on t.tag_id = cat.tag_id
                                                                        and cat.cnsmr_accnt_id = cal.cnsmr_accnt_id
                                                                        and cat.cnsmr_accnt_sft_delete_flg = 'N'
                                                    inner join systm_cd sc on sc.systm_cd = t.systm_cd
                                                                            and sc.systm_cd_is_entty_actv_flg = 'N'
                                        )
                                    then 'N'
                                    else 'Y'
                                end
                                , case
                                    when exists
                        (
                            select sc.systm_cd_is_pybl_flg
                            from cnsmr_accnt_tag cat
                                    inner join tag t on t.tag_id = cat.tag_id
                                                        and cat.cnsmr_accnt_id = cal.cnsmr_accnt_id
                                                        and cat.cnsmr_accnt_sft_delete_flg = 'N'
                                    inner join systm_cd sc on sc.systm_cd = t.systm_cd
                                                            and sc.systm_cd_is_pybl_flg = 'N'
                        )
                                    then 'N'
                                    else 'Y'
                                end
                                , case
                                    when exists
                                        (
                                            select sc.systm_cd_incld_in_cnsmr_bal_flg
                                            from cnsmr_accnt_tag cat
                                                    inner join tag t on t.tag_id = cat.tag_id
                                                                        and cat.cnsmr_accnt_id = cal.cnsmr_accnt_id
                                                                        and cat.cnsmr_accnt_sft_delete_flg = 'N'
                                                    inner join systm_cd sc on sc.systm_cd = t.systm_cd
                                                                            and sc.systm_cd_incld_in_cnsmr_bal_flg = 'N'
                                        )
                                    then 'N'
                                    else 'Y'
                                end
                        from @cnsmr_accnt_list cal
                                inner join cnsmr_accnt ca on ca.cnsmr_accnt_id = cal.cnsmr_accnt_id
                                inner join ETL.mu_cnsmr_accnt_tag stg on stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
                        where stg.cnsmr_accnt_tag_id >= @start_key
                                and stg.cnsmr_accnt_tag_id < @start_key + @chunk_size
                    );

                    update ca
                        set 
                          ca.cnsmr_accnt_is_actv_per_crdtr_flg = scf.systm_cd_is_entty_actv_flg
                        , ca.cnsmr_accnt_is_pyble_flg = scf.systm_cd_is_pybl_flg
                        , ca.cnsmr_accnt_incld_in_cnsmr_bal_flg = scf.systm_cd_incld_in_cnsmr_bal_flg
                        , ca.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , ca.upsrt_trnsctn_nmbr = ca.upsrt_trnsctn_nmbr + 1
                        , ca.upsrt_usr_id = @default_upsrt_usr_id
                    from cnsmr_accnt ca
                            inner join @system_code_flags scf on scf.cnsmr_accnt_id = ca.cnsmr_accnt_id;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_accnt Other flags, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);
                end;

                -- advance the start_key for the next set of records in next loop
                set @start_key = @start_key + @chunk_size;

                -- advance the loop
                if @loop_count < @total_loops
                    set @loop_count = @loop_count + 1;
                else
                    set @loop_count = 0;

                if @@trancount > 0
                    commit transaction;
            end;

            insert into ETL.mu_processing_log(messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - FINISHED',
				'Completed ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

            set @run_time = convert(nvarchar, dateadd(ms, datediff(second, @start_time, dbo.getcurrdt()) * 1000, 0), 114);

            insert into ETL.mu_processing_log(messagetype, logmessage, logginglevel)
            values(@msg_tbl_nm + ' - RUN TIME', @msg_tbl_nm + ' Total Run Time: ' + @run_time, 3);
        end try

        begin catch
            if @@trancount > 0
                rollback;

            select @err_msg = error_message()
                 , @err_severity = error_severity()
                 , @err_num = error_number();

            insert into ETL.mu_processing_log(messagetype, logmessage, severity)
            values('ERROR', cast(@err_num as varchar) + ' ' + @err_msg, @err_severity);

            insert into ETL.mu_processing_log(messagetype, logmessage, logginglevel)
            values('ERROR', 'cnsmr_accnt_Tag Load Rolled Back.', 0);

            raiserror(@err_msg, @err_severity, 16);
        end catch;

        /***********************************************
            Dump the logging to a permanent table.  
        ***********************************************/
/*
        insert into ETL.mu_processing_log
		(
			messagetype
            ,logmessage
            ,severity
            ,logginglevel
            ,logtime
		)
        select messagetype
               ,logmessage
               ,severity
               ,logginglevel
               ,logtime
        from   @logsystem;
*/
    end;
GO

