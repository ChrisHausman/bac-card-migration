use crs5_oltp;
go

-- drop existing staging table if present
if object_id('ETL.mu_cnsmr_accnt_frwrd_btch', 'U') is not null
    drop table etl.mu_cnsmr_accnt_frwrd_btch;

if object_id('ETL.mu_cnsmr_accnt_frwrd_rcll_dtl', 'U') is not null
    drop table etl.mu_cnsmr_accnt_frwrd_rcll_dtl;

-- mu_cnsmr_accnt_frwrd_rcll_dtl staging table
create table etl.mu_cnsmr_accnt_frwrd_rcll_dtl
( 
	cnsmr_accnt_frwrd_rcll_dtl_id     bigint identity(1, 1) not null
    , rcvr_shrt_nm                    varchar(8) not null
    , cnsmr_accnt_idntfr_lgcy_txt     varchar(20) not null
    , frwrd_rcll_mode_val_txt         varchar(32) not null
    , frwrd_rcll_stts_val_txt         varchar(32) not null
    , cnsmr_accnt_sngl_frwrd_rqst_flg char(1) null
    , cnsmr_accnt_frwrd_rqst_dttm     datetime not null
    , cnsmr_accnt_frwrd_dttm          datetime null
    , cnsmr_accnt_frwrd_bal_amnt      decimal(16, 6) not null
    , cnsmr_accnt_frwrd_btch_id       bigint null
    , cnsmr_accnt_sngl_rcll_rqst_flg  char(1) null
    , cnsmr_accnt_rcll_rqst_dttm      datetime null
    , cnsmr_accnt_rcll_dttm           datetime null
    , cnsmr_accnt_rcll_bal_amnt       decimal(16, 6) null
    , cnsmr_accnt_rcll_btch_id        bigint null
    , cnsmr_accnt_expctd_rcll_dttm    datetime null
    , cnsmr_accnt_frwd_hrd_rcll_flg   char(1) not null
    , upsrt_dttm                      datetime null
    , upsrt_soft_comp_id              int null
    , upsrt_trnsctn_nmbr              int null
    , upsrt_usr_id                    bigint null
    , crtd_usr_id                     bigint null
    , crtd_dttm                       datetime null
);
go

-- create the processing stored procedure
if object_id('ETL.sp_mu_frwrd_rcll', 'P') is not null
    drop procedure etl.sp_mu_frwrd_rcll;
go

/* ****************************************************************************************************************** */
/*   Name: Migration Utility - Forward/Recall - Processing                                                            */
/*   Staging Table: ETL.mu_cnsmr_accnt_frwrd_rcll                                                                     */
/*   Processing Stored Procedure: ETL.sp_mu_cnsmr_accnt_frwrd_rcll                                                    */
/*   Date:  07/07/2020                                                                                                */
/*   Purpose:  This script is designed to create the staging table and processing stored procedure                    */
/*             for the migration utility (MU) to migrate the cnsmr_accnt_frwrd_btch, cnsmr_accnt_frwrd_rcll,          */
/*             and cnsmr_accnt_frwrd_rcll_dtl entities for Forward/Recall Processing.                                 */
/*   Change Log:                                                                                                      */
/*       Version     Description                         Developer               Date                                 */
/*       ------------------------------------------------------------------------------------------------------------ */
/*       11.1        Initial - for DM 11.1               Doug Barlett            07/07/2020                           */
/*       11.1.BAC    Updated for BAC's TSYS Migration    Chris Hausman           12/29/2020                           */
/*       11.2.BAC    Added insert into fee schdl table   Chris Hausman           09/10/2021                           */
/*       11.3.BAC    Default both single rqst flgs to Y  Chris Hausman           02/24/2022                           */
/*       11.3.BAC    Fixed bug with error logging.       Chris Hausman           04/29/2022                           */
/*       11.4.BAC    Log direct to table.                Andrew Sheldon          04/11/2023                           */
/*       11.5.BAC    Remove logic to bypass soft         Chris Hausman           09/11/2023                           */
/*                   deleted & inactive receivers.                                                                    */
/*       11.6        Update Forward tag assign date      Chris Hausman           10/11/2023                           */
/*       ------------------------------------------------------------------------------------------------------------ */
/*  TO DO LIST                                                                                                        */
/*  ----------                                                                                                        */
/*   (1) Should we post any tags to accounts that are currently forwarded at time of migration? YES - ADDED           */
/*   (2) Confirm if recall history will be migrated... but I think so...                                              */
/*        6/17/21 - NOPE. Only currently forwarded.                                                                   */
/* ****************************************************************************************************************** */

create   procedure etl.sp_mu_frwrd_rcll @chunk_size bigint = 10000
as
    begin
        declare @err_msg nvarchar(4000)= N'';
        declare @err_severity int;
        declare @err_num int;

        declare @msg_tbl_nm nvarchar(50)= N'cnsmr_accnt_frwrd_rcll_dtl';

        declare @start_time datetime;
        declare @run_time varchar(20);
        declare @loop_count int;
        declare @total_count bigint;
        declare @total_loops int;
        declare @start_key bigint;

        declare @default_upsrt_soft_comp_id bigint;
        declare @default_upsrt_usr_id bigint;

        declare @forward_tag bigint;
        declare @forward_workgroup bigint;
        declare @forward_count as bigint;
        declare @recall_count as bigint;
        
        declare @current_datetime as datetime = dbo.getcurrdt();

        /**************************************************************************************
            Create permanent logging table just for account forwarding step.  
        **************************************************************************************/

        --This table will keep track of ETL progress.
        if not exists (select 1 from information_schema.tables where table_name = 'mu_Processing_Log')
        begin
            create table etl.mu_processing_log
            (
				pklog        numeric(18, 0) identity(1, 1) not null
			  , messagetype  nvarchar(50) not null
              , logmessage   nvarchar(1000) null
              , severity     int null
              , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
              , logtime      datetime not null default getdate()
            );
        end;

        /************************************************************************************
            Create local logging table.
        ************************************************************************************/

        declare @logsystem table
        (
			pklog        numeric(18, 0) not null identity(1, 1)
		  , messagetype  nvarchar(50) not null
		  , logmessage   nvarchar(1000) not null
		  , severity     int null
		  , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
		  , logtime      datetime not null default getdate()
        );

        begin try
            select @start_time = @current_datetime;

            -- set some default values
            select @default_upsrt_soft_comp_id = upsrt_soft_comp_id
            from dbo.lkup_soft_comp
            where upsrt_soft_comp_nm = 'load';

            select @default_upsrt_usr_id = usr_id
            from dbo.usr
            where usr_usrnm = 'system';

            select @forward_tag = tag.tag_id
            from dbo.tag
            where tag.tag_shrt_nm = 'Forward';

            select @forward_workgroup = wrkgrp.wrkgrp_id
            from dbo.wrkgrp
            where wrkgrp_shrt_nm in('DfltWkgp');  --Transition work group id to which the consumers owning the forwarded accounts should be assigned while the account is in forwarded status so the collectors under the previous workgroup need not work on those accounts as they are forwarded.

            select @forward_count = count(*)
            from etl.mu_cnsmr_accnt_frwrd_rcll_dtl
            where frwrd_rcll_mode_val_txt = 'FORWARD';

            select @recall_count = count(*)
            from etl.mu_cnsmr_accnt_frwrd_rcll_dtl
            where frwrd_rcll_mode_val_txt = 'RECALL';

            -- setup the looping variables to process in chucks
            set @loop_count = 1;

            select @total_count = count(*)
            from etl.mu_cnsmr_accnt_frwrd_rcll_dtl;

            select @start_key = min(cnsmr_accnt_frwrd_rcll_dtl_id)
            from etl.mu_cnsmr_accnt_frwrd_rcll_dtl;
            set @total_loops = @total_count / @chunk_size + 1;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - START',
				'Starting ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

            begin transaction;

            -- create the cnsmr_accnt_frwrd_btch record for each receiver in the MU staging table.
            insert into dbo.cnsmr_accnt_frwrd_btch
			(
				rcvr_id
              , cnsmr_accnt_frwrd_btch_nm
              , cnsmr_accnt_frwrd_btch_dscrptn_txt
              , cnsmr_accnt_frwrd_assgn_tag_id
              , cnsmr_accnt_frwrd_trnstn_wrkgrp_id
              , cnsmr_accnt_frwrd_scan_lst_id
              , cnsmr_accnt_expctd_rcll_dttm
              , cnsmr_accnt_frwrd_prcss_dttm
              , cnsmr_accnt_frwrd_sft_dlt_flg
              , cnsmr_accnt_frwrd_btch_cnt_nmbr
              , cnsmr_accnt_frwrd_btch_lmt_nmbr
              , cnsmr_accnt_frwrd_btch_mode_cd
              , cnsmr_accnt_frwrd_btch_stts_cd
              , cnsmr_archvd_flg
              , upsrt_dttm
              , upsrt_soft_comp_id
              , upsrt_trnsctn_nmbr
              , upsrt_usr_id
              , crtd_usr_id
              , crtd_dttm
			)
            select r.rcvr_id as rcvr_id
                  ,rtrim(ltrim(stg.rcvr_shrt_nm)) + '_' + convert(nvarchar, @current_datetime, 126) as cnsmr_accnt_frward_btch_nm
                  ,rtrim(ltrim(stg.rcvr_shrt_nm)) + '_Migration_Forward_'
				    + convert(nvarchar, @current_datetime, 126) as cnsmr_accnt_frwrd_btch_dscrptn_txt
                  ,@forward_tag as cnsmr_accnt_frwrd_assgn_tag_id
                  ,@forward_workgroup as cnsmr_accnt_frwrd_trnstn_wrkgrp_id
                  ,null as cnsmr_accnt_frwrd_scan_lst_id
                  ,null as cnsmr_accnt_expctd_rcll_dttm
                  ,@current_datetime as cnsmr_accnt_frwrd_prcss_dttm
                  ,'N' as cnsmr_accnt_frwrd_sft_dlt_flg
                  ,@forward_count as cnsmr_accnt_frwrd_btch_cnt_nmbr
                  ,null as cnsmr_accnt_frwrd_btch_lmt_nmbr
                  ,2 as cnsmr_accnt_frwrd_btch_mode_cd
                  ,3 as cnsmr_accnt_frwrd_btch_stts_cd
                  ,'N' as cnsmr_archvd_flg
                  ,@current_datetime as upsrt_dttm
                  ,@default_upsrt_soft_comp_id as  upsrt_soft_comp_id
                  ,1 as upsrt_trnsctn_nmbr
                  ,@default_upsrt_usr_id as upsrt_usr_id
                  ,@default_upsrt_usr_id as crtd_usr_id
                  ,@current_datetime as crtd_dttm
            from  etl.mu_cnsmr_accnt_frwrd_rcll_dtl stg
                  inner join dbo.rcvr r
				    on r.rcvr_shrt_nm = stg.rcvr_shrt_nm
                       --and r.rcvr_actv_flg = 'Y'
                       --and r.rcvr_sft_dlt_flg = 'N'
            group by stg.rcvr_shrt_nm
                    ,r.rcvr_id;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm,
			    'Completed insert into cnsmr_accnt_frwrd_btch, '
				 + convert(nvarchar, @@rowcount) + ' rows inserted.  Loop: ' 
				 + convert(nvarchar, @loop_count),
				3
			);

            -- create the cnsmr_accnt_rcll_btch record for all recall receivers in MU staging.
            insert into dbo.cnsmr_accnt_rcll_btch
			(
				rcvr_id
                ,cnsmr_accnt_rcll_btch_nm
                ,cnsmr_accnt_rcll_btch_dscrptn_txt
                ,cnsmr_accnt_rcll_assgn_tag_id
                ,cnsmr_accnt_rcll_trnsctn_wrkgrp_id
                ,cnsmr_accnt_rcll_prcss_dttm
                ,cnsmr_accnt_rcll_sft_dlt_flg
                ,cnsmr_accnt_rcll_btch_cnt_nmbr
                ,cnsmr_accnt_rcll_btch_lmt_nmbr
                ,cnsmr_accnt_rcll_btch_mode_cd
                ,cnsmr_accnt_rcll_btch_stts_cd
                ,cnsmr_archvd_flg
                ,cnsmr_accnt_rcll_rsn_cd
                ,cnsmr_accnt_rcll_btch_hrd_rcll_flg
                ,upsrt_dttm
                ,upsrt_soft_comp_id
                ,upsrt_trnsctn_nmbr
                ,upsrt_usr_id
                ,crtd_usr_id
                ,crtd_dttm
			)
            select r.rcvr_id as rcvr_id
                   ,rtrim(ltrim(stg.rcvr_shrt_nm)) + '_' + convert(nvarchar, @current_datetime, 126) as cnsmr_accnt_rcll_btch_nm
                   ,rtrim(ltrim(stg.rcvr_shrt_nm)) + '_Migration_Recall_'
				    + convert(nvarchar, @current_datetime, 126) as cnsmr_accnt_rcll_btch_dscrptn_txt
                   ,null as cnsmr_accnt_rcll_assgn_tag_id
                   ,null as cnsmr_accnt_rcll_trnsctn_wrkgrp_id
                   ,@current_datetime as cnsmr_accnt_rcll_prcss_dttm
                   ,'N' as cnsmr_accnt_rcll_sft_dlt_flg
                   ,@recall_count as cnsmr_accnt_rcll_btch_cnt_nmbr
                   ,null as cnsmr_accnt_rcll_btch_lmt_nmbr
                   ,3 as cnsmr_accnt_rcll_btch_mode_cd
                   ,3 as cnsmr_accnt_rcll_btch_stts_cd
                   ,'N' as cnsmr_archvd_flg
                   ,(SELECT rcll_rsn_cd FROM ref_rcll_rsn_cd WHERE rcll_rsn_val_txt = 'NO_RSN_GVN') as cnsmr_accnt_rcll_rsn_cd
                   ,'N' as cnsmr_accnt_rcll_btch_hrd_rcll_flg
                   ,@current_datetime as upsrt_dttm
                   ,@default_upsrt_soft_comp_id as upsrt_soft_comp_id
                   ,1 as upsrt_trnsctn_nmbr
                   ,@default_upsrt_usr_id as upsrt_usr_id
                   ,@default_upsrt_usr_id as crtd_usr_id
                   ,@current_datetime as crtd_dttm
            from   etl.mu_cnsmr_accnt_frwrd_rcll_dtl stg
                   inner join dbo.rcvr r
				     on r.rcvr_shrt_nm = stg.rcvr_shrt_nm
                        --and r.rcvr_actv_flg = 'Y'
                        --and r.rcvr_sft_dlt_flg = 'N'
            where  stg.frwrd_rcll_mode_val_txt = 'RECALL'
            group  by stg.rcvr_shrt_nm
                   ,r.rcvr_id;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm,
				'Completed insert into cnsmr_accnt_rcll_btch, '
				 + convert(nvarchar, @@rowcount) + ' rows inserted.  Loop: '
				 + convert(nvarchar, @loop_count),
				3
			);

            if @@trancount > 0
                commit transaction;

            while @loop_count > 0
            begin
                begin transaction;

                -- create the cnsmr_accnt_frwrd_rcll_dtl record
                insert into dbo.cnsmr_accnt_frwrd_rcll_dtl
				(
					rcvr_id
                    ,cnsmr_accnt_id
                    ,cnsmr_accnt_frwrd_rcll_mode_cd
                    ,cnsmr_accnt_frwrd_rcll_stts_cd
                    ,cnsmr_accnt_sngl_frwrd_rqst_flg
                    ,cnsmr_accnt_frwrd_rqst_dttm
                    ,cnsmr_accnt_frwrd_dttm
                    ,cnsmr_accnt_frwrd_bal_amnt
                    ,cnsmr_accnt_frwrd_btch_id
                    ,cnsmr_accnt_sngl_rcll_rqst_flg
                    ,cnsmr_accnt_rcll_rqst_dttm
                    ,cnsmr_accnt_rcll_dttm
                    ,cnsmr_accnt_rcll_bal_amnt
                    ,cnsmr_accnt_rcll_btch_id
                    ,cnsmr_accnt_expctd_rcll_dttm
                    ,cnsmr_accnt_frwd_hrd_rcll_flg
                    ,upsrt_dttm
                    ,upsrt_soft_comp_id
                    ,upsrt_trnsctn_nmbr
                    ,upsrt_usr_id
                    ,crtd_usr_id
                    ,crtd_dttm
				)
                select r.rcvr_id as                            rcvr_id
                       ,ca.cnsmr_accnt_id as                   cnsmr_accnt_id
                       ,frmc.frwrd_rcll_mode_cd as             cnsmr_accnt_frwrd_rcll_mode_cd
                       ,frsc.frwrd_rcll_stts_cd as             cnsmr_accnt_frwrd_rcll_stts_cd
                       ,'N' as                                 cnsmr_accnt_sngl_frwrd_rqst_flg
                       ,stg.cnsmr_accnt_frwrd_dttm as          cnsmr_accnt_frwrd_rqst_dttm
                       ,stg.cnsmr_accnt_frwrd_dttm as          cnsmr_accnt_frwrd_dttm
                       ,stg.cnsmr_accnt_frwrd_bal_amnt as      cnsmr_accnt_frwrd_bal_amnt
                       ,cafb.cnsmr_accnt_frwrd_btch_id as      cnsmr_accnt_frwrd_btch_id
                       ,'N' as                                 cnsmr_accnt_sngl_rcll_rqst_flg
                       ,stg.cnsmr_accnt_rcll_rqst_dttm as      cnsmr_accnt_rcll_rqst_dttm
                       ,stg.cnsmr_accnt_rcll_dttm as           cnsmr_accnt_rcll_dttm
                       ,stg.cnsmr_accnt_rcll_bal_amnt as       cnsmr_accnt_rcll_bal_amnt
                       ,carb.cnsmr_accnt_rcll_btch_id as       cnsmr_accnt_rcll_btch_id
                       ,stg.cnsmr_accnt_expctd_rcll_dttm as    cnsmr_accnt_expctd_rcll_dttm
                       ,stg.cnsmr_accnt_frwd_hrd_rcll_flg as   cnsmr_accnt_frwd_hrd_rcll_flg
                       ,@current_datetime as                   upsrt_dttm
                       ,@default_upsrt_soft_comp_id as         upsrt_soft_comp_id
                       ,1 as                                   upsrt_trnsctn_nmbr
                       ,@default_upsrt_usr_id as               upsrt_usr_id
                       ,@default_upsrt_usr_id as               crtd_usr_id
                       ,@current_datetime as                   crtd_dttm
                from   ETL.mu_cnsmr_accnt_frwrd_rcll_dtl stg
                       inner join dbo.rcvr r
					     on r.rcvr_shrt_nm = stg.rcvr_shrt_nm
                       inner join dbo.cnsmr_accnt ca
					     on ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
                       inner join dbo.ref_frwrd_rcll_mode_cd frmc
					     on frmc.frwrd_rcll_mode_val_txt = stg.frwrd_rcll_mode_val_txt
                       inner join dbo.ref_frwrd_rcll_stts_cd frsc
					     on frsc.frwrd_rcll_stts_val_txt = stg.frwrd_rcll_stts_val_txt
                       inner join dbo.cnsmr_accnt_frwrd_btch cafb
					     on cafb.cnsmr_accnt_frwrd_btch_nm = stg.rcvr_shrt_nm + '_' + convert(nvarchar, @current_datetime, 126)
                       left join dbo.cnsmr_accnt_rcll_btch carb
					     on carb.cnsmr_accnt_rcll_btch_nm = stg.rcvr_shrt_nm + '_' + convert(nvarchar, @current_datetime, 126)
                where stg.cnsmr_accnt_frwrd_rcll_dtl_id >= @start_key
                      and stg.cnsmr_accnt_frwrd_rcll_dtl_id < @start_key + @chunk_size
				ORDER BY stg.cnsmr_accnt_frwrd_rqst_dttm, frmc.frwrd_rcll_mode_cd;

                insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
                values
				(
					@msg_tbl_nm,
					'Completed insert into ' + @msg_tbl_nm + ', '
					 + convert(nvarchar, @@rowcount) + ' rows inserted.  Loop: '
					 + convert(nvarchar, @loop_count),
					3
				);

				INSERT INTO cnsmr_accnt_tag
				(
					cnsmr_accnt_id,
					tag_id,
					cnsmr_accnt_tag_assgn_dt,
					cnsmr_accnt_sft_delete_flg,
					upsrt_dttm,
					upsrt_soft_comp_id,
					upsrt_trnsctn_nmbr,
					upsrt_usr_id
				)
				select ca.cnsmr_accnt_id
				       ,(select tag_id from tag where tag_shrt_nm = 'Forward')
					   ,stg.cnsmr_accnt_frwrd_dttm
					   ,'N'
                       ,@current_datetime as                   upsrt_dttm
                       ,@default_upsrt_soft_comp_id as         upsrt_soft_comp_id
                       ,1 as                                   upsrt_trnsctn_nmbr
                       ,@default_upsrt_usr_id as               upsrt_usr_id					   
                from   ETL.mu_cnsmr_accnt_frwrd_rcll_dtl stg
                       inner join dbo.cnsmr_accnt ca
					     on ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
                where stg.cnsmr_accnt_frwrd_rcll_dtl_id >= @start_key
                      and stg.cnsmr_accnt_frwrd_rcll_dtl_id < @start_key + @chunk_size
					  and stg.frwrd_rcll_mode_val_txt = 'FORWARD'
					  and stg.frwrd_rcll_stts_val_txt = 'COMPLETED';

                insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
                values
				(
					@msg_tbl_nm + '_tag',
					'Completed insert into ' + @msg_tbl_nm + '_tag, '
					 + convert(nvarchar, @@rowcount) + ' rows inserted.  Loop: '
					 + convert(nvarchar, @loop_count),
					3
				);
				
				INSERT INTO cnsmr_accnt_effctv_fee_schdl
				(
					cnsmr_accnt_id,
					fee_schdl_id,
					rcvr_id,
					cnsmr_accnt_fee_schdl_effctv_dttm,
					upsrt_dttm,
					upsrt_soft_comp_id,
					upsrt_trnsctn_nmbr,
					upsrt_usr_id,
					crtd_usr_id,
					crtd_dttm
				)
				SELECT ca.cnsmr_accnt_id,
				       r.rcvr_fee_schdld_id,
					   r.rcvr_id,
					   stg.cnsmr_accnt_frwrd_dttm,
                       @current_datetime,
                       @default_upsrt_soft_comp_id,
                       1,
                       @default_upsrt_usr_id,
                       @default_upsrt_usr_id,
                       @current_datetime
				FROM   ETL.mu_cnsmr_accnt_frwrd_rcll_dtl stg
                       INNER JOIN dbo.cnsmr_accnt ca
					     ON ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
					   INNER JOIN rcvr r
						 ON stg.rcvr_shrt_nm = r.rcvr_shrt_nm
                            and r.rcvr_actv_flg = 'Y'
                            and r.rcvr_sft_dlt_flg = 'N'
                WHERE  stg.cnsmr_accnt_frwrd_rcll_dtl_id >= @start_key
                       AND stg.cnsmr_accnt_frwrd_rcll_dtl_id < @start_key + @chunk_size
					   AND stg.frwrd_rcll_mode_val_txt = 'FORWARD'
					   AND stg.frwrd_rcll_stts_val_txt = 'COMPLETED';

                insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
                values
				(
					@msg_tbl_nm + '_fee_schdld',
					'Completed insert into cnsmr_accnt_effctv_fee_schdl, '
					 + convert(nvarchar, @@rowcount) + ' rows inserted.  Loop: '
					 + convert(nvarchar, @loop_count),
					3
				);
								
                -- advance the start_key for the next set of records in next loop
                set @start_key = @start_key + @chunk_size;

                -- advance the loop
                if @loop_count < @total_loops
                    set @loop_count = @loop_count + 1;
                else
                    set @loop_count = 0;

                if @@trancount > 0
                    commit transaction;
            end;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - FINISHED',
				'Completed ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

            -- Update the cnsmr_accnt_wrk_actn table for those accounts that have been forwarded at the time of conversion
            select cafrd.cnsmr_accnt_frwrd_rcll_dtl_id
                   ,cafrd.cnsmr_accnt_id
                   ,cafrd.cnsmr_accnt_frwrd_rcll_mode_cd
                   ,row_number() over (partition by cafrd.cnsmr_accnt_id order by cafrd.cnsmr_accnt_frwrd_dttm desc) rnk
            into   #lst_id
            from   dbo.cnsmr_accnt_frwrd_rcll_dtl cafrd
            where  cafrd.cnsmr_accnt_frwrd_rcll_stts_cd = (select frwrd_rcll_stts_cd from ref_frwrd_rcll_stts_cd where frwrd_rcll_stts_val_txt = 'COMPLETED')
			       and cafrd.upsrt_dttm = @current_datetime;

            create nonclustered index #lst_id_idx_01 on #lst_id(cnsmr_accnt_id, rnk) include(cnsmr_accnt_frwrd_rcll_dtl_id);

            update cawa
            set    cawa.cnsmr_accnt_lst_frwrd_rcll_dtl_id = tmp.cnsmr_accnt_frwrd_rcll_dtl_id
            from   dbo.cnsmr_accnt_wrk_actn cawa
                   inner join #lst_id tmp
				     on cawa.cnsmr_accnt_id = tmp.cnsmr_accnt_id
                        and tmp.rnk = 1
            where  tmp.cnsmr_accnt_frwrd_rcll_mode_cd = 1; -- 'FORWARD'

            -- Update the cnsmr_accnt table to set the correct _is_frwrd_flg value on those accounts with cnsmr_accnt_frwrd_rcll_dtl records
            update ca
            set    ca.cnsmr_accnt_is_frwrd_flg = case
                                                  when tmp.cnsmr_accnt_frwrd_rcll_mode_cd = 1
                                                  then 'Y'
                                                  else 'N'
                                                 end
            from   dbo.cnsmr_accnt ca
                   inner join #lst_id tmp
				     on ca.cnsmr_accnt_id = tmp.cnsmr_accnt_id
                        and tmp.rnk = 1;

            set @run_time = convert(nvarchar, dateadd(ms, datediff(second, @start_time, dbo.getcurrdt()) * 1000, 0), 114);

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - RUN TIME',
				@msg_tbl_nm + ' Total Run Time: ' + @run_time,
				3
			);
        end try

        begin catch
            if @@trancount > 0
                rollback;

            select @err_msg = error_message()
                 , @err_severity = error_severity()
                 , @err_num = error_number();

            insert into etl.mu_processing_log (messagetype, logmessage, severity)
            values('ERROR', cast(@err_num as varchar) + ' ' + @err_msg, @err_severity);

            insert into etl.mu_processing_log(messagetype, logmessage, logginglevel)
            values('ERROR', 'Status Dispo Rolled Back.', 0);

            raiserror(@err_msg, @err_severity, 16);
        end catch;

        /***********************************************************
            Dump the logging to a permanent table.  
        ***********************************************************/

--        insert into etl.mu_processing_log(messagetype, logmessage, severity, logginglevel, logtime)
--        select messagetype, logmessage, severity, logginglevel, logtime
--        from @logsystem;
		
    end;
GO

