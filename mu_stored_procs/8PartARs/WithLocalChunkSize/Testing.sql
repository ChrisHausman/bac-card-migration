insert into etl.mgrtn_cnsmr_accnt_ar_log ([agncy_entty_crrltn_id], cnsmr_accnt_idntfr_lgcy_txt, rslt_cd_val_txt, cnsmr_accnt_ar_mssg_txt, grp_nmbr, upsrt_dttm)
select top(10000)
       1,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   'DfltScss',
	   'testing migration grouping logic',
	   42,
	   getdate()
from   cnsmr_accnt ca
where  isnumeric(ca.cnsmr_accnt_idntfr_lgcy_txt) = 1
       and ca.cnsmr_accnt_idntfr_agncy_id = ca.cnsmr_accnt_idntfr_lgcy_txt

select count(1) from cnsmr_accnt_ar_log where cnsmr_accnt_ar_mssg_txt = 'testing migration grouping logic'

select caal.cnsmr_accnt_id, count(1)
from   cnsmr_accnt_ar_log caal
where  caal.cnsmr_accnt_ar_mssg_txt = 'testing migration grouping logic'
group  by caal.cnsmr_accnt_id
having count(1) > 1

delete from cnsmr_accnt_ar_log where cnsmr_accnt_ar_mssg_txt = 'testing migration grouping logic'

