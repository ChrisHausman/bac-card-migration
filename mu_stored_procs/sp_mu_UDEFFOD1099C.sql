use crs5_oltp;
go

-- drop existing staging table if present
if object_id('ETL.mu_UDEFFOD1099C', 'U') is not null
    drop table etl.mu_UDEFFOD1099C;

-- mu_UDEFFOD1099C staging table
CREATE TABLE [ETL].[mu_UDEFFOD1099C](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cnsmr_accnt_idntfr_lgcy_txt] [varchar](20) NULL,
	[UDEFFODAMT] [decimal](16, 2) NULL,
	[UDEFLST_CNTCT_DT] [datetime] NULL,
	[UDEFFODEVTCD] [varchar](40) NULL,
	[UDEFFODDATE] [datetime] NULL,
	[UDEFFODCMNT] [varchar](1000) NULL,
	[UDEFFODRSN_VAL] [varchar](256) NULL
) ON [PRIMARY]

-- create the processing stored procedure
if object_id('ETL.sp_mu_UDEFFOD1099C', 'P') is not null
    drop procedure etl.sp_mu_UDEFFOD1099C;
go

/* ****************************************************************************************************************** */
/*   Name: Direct Load UDEFFOD1099C UDP                                                                               */
/*   Staging Table: ETL.mu_UDEFFOD1099C                                                                               */
/*   Processing Stored Procedure: ETL.sp_mu_UDEFFOD1099C                                                              */
/*   Date:  05/27/2022                                                                                                */
/*   Purpose:  Custom DL script for loading data into UDEFFOD1099C UDP                                                */
/*   Change Log:                                                                                                      */
/*       Version     Description                         Developer               Date                                 */
/*       ------------------------------------------------------------------------------------------------------------ */
/*       11.1        Initial - for BAC TSYS migration    Chris Hausman           05/27/2022                           */
/*       ------------------------------------------------------------------------------------------------------------ */
/*  TO DO LIST                                                                                                        */
/*  ----------                                                                                                        */
/*   (1) N/A                                                                                                          */
/* ****************************************************************************************************************** */

create procedure etl.sp_mu_UDEFFOD1099C @chunk_size bigint = 10000
as
    begin
        declare @err_msg nvarchar(4000)= N'';
        declare @err_severity int;
        declare @err_num int;

        declare @msg_tbl_nm nvarchar(50)= N'UDEFFOD1099C';

        declare @start_time datetime;
        declare @run_time varchar(20);
        declare @loop_count int;
        declare @total_count bigint;
        declare @total_loops int;
        declare @start_key bigint;

        declare @default_upsrt_soft_comp_id bigint;
        declare @default_upsrt_usr_id bigint;
		
        declare @current_datetime as datetime = dbo.GETDATE();

		declare @udef_adt_id as bigint;
		declare @fodrsn_id as bigint;
		
        /**************************************************************************************
            Create permanent logging table.  
        **************************************************************************************/

        --This table will keep track of ETL progress.
        if not exists (select 1 from information_schema.tables where table_name = 'mu_Processing_Log')
        begin
            create table etl.mu_processing_log
            (
				pklog        numeric(18, 0) identity(1, 1) not null
			  , messagetype  nvarchar(50) not null
              , logmessage   nvarchar(1000) null
              , severity     int null
              , logginglevel int not null
              , logtime      datetime not null
            );
        end;

        /************************************************************************************
            Create local logging table.
        ************************************************************************************/

        declare @logsystem table
        (
			pklog        numeric(18, 0) not null identity(1, 1)
		  , messagetype  nvarchar(50) not null
		  , logmessage   nvarchar(1000) not null
		  , severity     int null
		  , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
		  , logtime      datetime not null default getdate()
        );
		
        begin try
            select @start_time = @current_datetime;

            -- set some default values
            select @default_upsrt_soft_comp_id = upsrt_soft_comp_id
            from dbo.lkup_soft_comp
            where upsrt_soft_comp_nm = 'load';

            select @default_upsrt_usr_id = usr_id
            from dbo.usr
            where usr_usrnm = 'CNVRSN';

			--set the variables for the list of value fields
			select @udef_adt_id = agncy_dfnd_tbl_id
			from   agncy_Dfnd_Tbl
			where  agncy_dfnd_tbl_nm = 'UDEFFOD1099C'
			
			select @fodrsn_id = agncy_dfnd_clmn_id
			from   agncy_Dfnd_Clmn
			where  agncy_dfnd_clmn_nm = 'UDEFFODRSN'
			       and agncy_dfnd_tbl_id = @udef_adt_id

            -- setup the looping variables to process in chucks
            set @loop_count = 1;

            select @total_count = count(*)
            from etl.mu_UDEFFOD1099C;

            select @start_key = min(id)
            from etl.mu_UDEFFOD1099C;

            set @total_loops = @total_count / @chunk_size + 1;

            insert into @logsystem (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - START',
				'Starting ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

            while @loop_count > 0
            begin
				
				INSERT INTO [dbo].[UDEFFOD1099C]
				(
					[cnsmr_accnt_id],
					[UDEFFODAMT],
					[UDEFLST_CNTCT_DT],
					[UDEFFODEVTCD],
					[UDEFFODDATE],
					[UDEFFODCMNT],
					[UDEFFODRSN],
					[upsrt_dttm],
					[upsrt_soft_comp_id],
					[upsrt_trnsctn_nmbr],
					[upsrt_usr_id]
				)
				SELECT ca.cnsmr_accnt_id AS cnsmr_accnt_id,
					   stg.UDEFFODAMT,
					   stg.UDEFLST_CNTCT_DT,
					   stg.UDEFFODEVTCD,
					   stg.UDEFFODDATE,
					   stg.UDEFFODCMNT,
					   uvl_fodrsn.udf_val_list_id,
					   GETDATE() AS upsrt_dttm,
					   @Default_upsrt_soft_comp_id AS upsrt_soft_comp_id,
					   1 AS upsrt_trnsctn_nmbr,
					   @Default_upsrt_usr_id AS upsrt_usr_id
				FROM   ETL.mu_UDEFFOD1099C stg
					   INNER JOIN [dbo].[cnsmr_accnt] ca
						 ON ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
					   LEFT JOIN udf_val_lst uvl_fodrsn
					     ON stg.UDEFFODRSN_VAL = uvl_fodrsn.udf_val_txt
						    AND uvl_fodrsn.agncy_dfnd_clmn_id = @fodrsn_id
				WHERE  stg.id >= @start_key
					   AND stg.id < @start_key + @chunk_size;
									
				INSERT INTO @LogSystem (MessageType, LogMessage, LoggingLevel)
				VALUES
				(
					@msg_tbl_nm,
					'Completed insert into ' + @msg_tbl_nm + ', ' + convert(nvarchar, @@rowcount) + ' rows inserted.  Loop: ' + convert(nvarchar, @loop_count),
					3
				)

                -- advance the start_key for the next set of records in next loop
                set @start_key = @start_key + @chunk_size;

                -- advance the loop
                if @loop_count < @total_loops
                    set @loop_count = @loop_count + 1;
                else
                    set @loop_count = 0;

            end;

            insert into @logsystem (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - FINISHED',
				'Completed ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

			set @run_time = convert(nvarchar, dateadd(ms, datediff(second, @start_time, dbo.GETDATE()) * 1000, 0), 114);

            insert into @logsystem (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - RUN TIME',
				@msg_tbl_nm + ' Total Run Time: ' + ISNULL(@run_time, ''),
				3
			);

        end try

        begin catch
            if @@trancount > 0
                rollback;

            select @err_msg = error_message()
                 , @err_severity = error_severity()
                 , @err_num = error_number();

            insert into @logsystem (messagetype, logmessage, severity)
            values('ERROR', cast(@err_num as varchar) + ' ' + @err_msg, @err_severity);

            insert into @logsystem(messagetype, logmessage, logginglevel)
            values('ERROR', 'UDEFFOD1099C Rolled Back.', 0);

            raiserror(@err_msg, @err_severity, 16);
        end catch;

        /***********************************************************
            Dump the logging to a permanent table.  
        ***********************************************************/

        insert into etl.mu_processing_log(messagetype, logmessage, severity, logginglevel, logtime)
        select messagetype, logmessage, severity, logginglevel, logtime
        from @logsystem;
		
    end;
go
                                