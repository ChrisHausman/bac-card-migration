/****** Object: Procedure [etl].[sp_mu_post_exec_boa]   Script Date: 9/1/2023 1:46:14 PM ******/
USE [crs5_oltp];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
/* ****************************************************************************************************************** */
/*   Name: Migration Utility - Post MU Execution Process                                                              */
/*   Staging Table: N/A                                                                                               */
/*   Processing Stored Procedure: ETL.sp_mu_post_exec_boa                                                             */
/*   Date:  04/14/2021                                                                                                */
/*   Purpose:  Custom index maintanence after MU load.                                                                */
/*   Change Log:                                                                                                      */
/*       Version     Description                         Developer               Date                                 */
/*       ------------------------------------------------------------------------------------------------------------ */
/*       11.2        Initial                             Chris Hausman           12/13/2021                           */
/*       11.2        Fixed bug with error logging.       Chris Hausman           04/29/2022                           */
/*       11.3        Direct to table logging.            Andrew Sheldon          04/12/2023                           */
/*       11.3        Drop index on phone staging table   Andrew Sheldon          09/01/2023                           */
/*       11.3        Drop index on account legacy.       Chris Hausman           04/29/2024                           */
/*       ------------------------------------------------------------------------------------------------------------ */
/*  TO DO LIST                                                                                                        */
/*  ----------                                                                                                        */
/* ****************************************************************************************************************** */

create or alter procedure etl.sp_mu_post_exec_boa
as
    begin
        declare @err_msg nvarchar(4000)= N'';
        declare @err_severity int;
        declare @err_num int;
	
        declare @current_datetime as datetime = dbo.getcurrdt();
		
        /**************************************************************************************
            Create permanent logging table.
        **************************************************************************************/

        --This table will keep track of ETL progress.
        if not exists (select 1 from information_schema.tables where table_name = 'mu_Processing_Log')
        begin
            create table etl.mu_processing_log
            (
				pklog        numeric(18, 0) identity(1, 1) not null
			  , messagetype  nvarchar(50) not null
              , logmessage   nvarchar(1000) null
              , severity     int null
              , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
              , logtime      datetime not null default getdate()
            );
        end;

        /************************************************************************************
            Create local logging table.
        ************************************************************************************/

        declare @logsystem table
        (
			pklog        numeric(18, 0) not null identity(1, 1)
		  , messagetype  nvarchar(50) not null
		  , logmessage   nvarchar(1000) not null
		  , severity     int null
		  , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
		  , logtime      datetime not null default getdate()
        );
		
		begin try
			
			--Index for agncy_entty_crrltn_id & cnsmr_idntfr_drvr_lcns_txt on cnsmr table.
			IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'cnsmr_dl_num')
				DROP INDEX cnsmr_dl_num ON dbo.cnsmr
			
			INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
			VALUES ('sp_mu_post_exec_boa', 'Finished drop of cnsmr_dl_num index.', 3)

			--Index for cnsmr_accnt_idntfr_lgcy_txt on cnsmr_accnt table.
			IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'cnsmr_accnt_lgcy_txt')
				DROP INDEX cnsmr_accnt_lgcy_txt ON dbo.cnsmr_accnt
			
			INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
			VALUES ('sp_mu_post_exec_boa', 'Finished drop of cnsmr_accnt_lgcy_txt index.', 3)

			--Drop Index for phone_number & cnsmr_idntfr_drvr_lcns_txt on dl_cnsmr_phn_stgng table.
			IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'dl_cnsmr_phn_stgng_dl_num_phn_num')
				DROP INDEX dl_cnsmr_phn_stgng_dl_num_phn_num ON dbo.dl_cnsmr_phn_stgng
			
			INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
			VALUES ('sp_mu_post_exec_boa', 'Finished drop of dl_cnsmr_phn_stgng_dl_num_phn_num index.', 3)
      
		end try

        begin catch
            if @@trancount > 0
                rollback;

            select @err_msg = error_message()
                 , @err_severity = error_severity()
                 , @err_num = error_number();

            insert into etl.mu_processing_log (messagetype, logmessage, severity)
            values('ERROR', cast(@err_num as varchar) + ' ' + @err_msg, @err_severity);

            insert into etl.mu_processing_log(messagetype, logmessage, logginglevel)
            values('ERROR', 'Post MU Exec BOA Rolled Back.', 0);

            raiserror(@err_msg, @err_severity, 16);
        end catch;

        /***********************************************************
            Dump the logging to a permanent table.  
        ***********************************************************/

--        insert into etl.mu_processing_log(messagetype, logmessage, severity, logginglevel, logtime)
--        select messagetype, logmessage, severity, logginglevel, logtime
--        from @logsystem;
		
    end;

GO

