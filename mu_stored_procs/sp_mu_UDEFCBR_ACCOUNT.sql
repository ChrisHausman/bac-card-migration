use crs5_oltp;
go

-- drop existing staging table if present
if object_id('ETL.mu_UDEFCBR_ACCOUNT', 'U') is not null
    drop table etl.mu_UDEFCBR_ACCOUNT;

-- mu_UDEFCBR_ACCOUNT staging table
CREATE TABLE [ETL].[mu_UDEFCBR_ACCOUNT]
(
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cnsmr_accnt_idntfr_lgcy_txt] [varchar](20) NULL,
	[UDEFACCNT_STS_CD_CHG_DT] [datetime] NULL,
	[UDEFBASE_9_ACCT_TYPE_CD] [varchar](10) NULL,
	[UDEFBASE_8_PRTFL_TY] [varchar](10) NULL,
	[UDEFBASE_14_TERM_FREQ] [varchar](10) NULL,
	[UDEFLST_PAY_DT] [datetime] NULL,
	[UDEFBASE_18_PHP_CD] [varchar](24) NULL,
	[UDEFBASE_27_DT_LAST_PYMNT] [datetime] NULL,
	[UDEFCBR_CLOSE_DT] [datetime] NULL,
	[UDEFBASE_20_CCC_CD] [varchar](10) NULL,
	[UDEFSUPP_RSN_CD] [varchar](10) NULL,
	[UDEFRPT_ONE_TIME] [char](1) NULL,
	[UDEFBASE_11_CR_LIMIT] [decimal](18, 2) NULL,
	[UDEFBASE_17A_ACCT_STATUS_CD] [varchar](10) NULL,
	[UDEFPRE_CO_METRO_ST] [varchar](10) NULL,
	[UDEFCB_LST_PMNT_AMNT] [decimal](12, 3) NULL,
	[UDEFBASE_13_TERM_DUR] [varchar](10) NULL,
	[UDEFBASE_12_H_CR] [decimal](18, 2) NULL,
	[UDEFORG_CHRG_OFF_AMT] [decimal](18, 2) NULL
) ON [PRIMARY]

-- create the processing stored procedure
if object_id('ETL.sp_mu_UDEFCBR_ACCOUNT', 'P') is not null
    drop procedure etl.sp_mu_UDEFCBR_ACCOUNT;
go

/* ****************************************************************************************************************** */
/*   Name: Direct Load CBR_ACCOUNT UDP                                                                                */
/*   Staging Table: ETL.mu_UDEFCBR_ACCOUNT                                                                            */
/*   Processing Stored Procedure: ETL.sp_mu_UDEFCBR_ACCOUNT                                                           */
/*   Date:  02/09/2022                                                                                                */
/*   Purpose:  Custom DL script for loading data into CBR_ACCOUNT UDP                                                 */
/*   Change Log:                                                                                                      */
/*       Version     Description                         Developer               Date                                 */
/*       ------------------------------------------------------------------------------------------------------------ */
/*       1.0         Initial - for BAC TSYS migration    Chris Hausman           02/09/2022                           */
/*       1.1         Fixed bug with error logging.       Chris Hausman           04/29/2022                           */
/*       1.1         Direct to table logging.            Andrew Sheldon          04/12/2023                           */
/*       ------------------------------------------------------------------------------------------------------------ */
/*  TO DO LIST                                                                                                        */
/*  ----------                                                                                                        */
/*   (1) N/A                                                                                                          */
/* ****************************************************************************************************************** */

create   procedure etl.sp_mu_UDEFCBR_ACCOUNT @chunk_size bigint = 10000
as
    begin
        declare @err_msg nvarchar(4000)= N'';
        declare @err_severity int;
        declare @err_num int;

        declare @msg_tbl_nm nvarchar(50)= N'UDEFCBR_ACCOUNT';

        declare @start_time datetime;
        declare @run_time varchar(20);
        declare @loop_count int;
        declare @total_count bigint;
        declare @total_loops int;
        declare @start_key bigint;

        declare @default_upsrt_soft_comp_id bigint;
        declare @default_upsrt_usr_id bigint;
		
        declare @current_datetime as datetime = dbo.GETDATE();
		
        /**************************************************************************************
            Create permanent logging table.  
        **************************************************************************************/

        --This table will keep track of ETL progress.
        if not exists (select 1 from information_schema.tables where table_name = 'mu_Processing_Log')
        begin
            create table etl.mu_processing_log
            (
				pklog        numeric(18, 0) identity(1, 1) not null
			  , messagetype  nvarchar(50) not null
              , logmessage   nvarchar(1000) null
              , severity     int null
              , logginglevel int not null  default 0 --0 is the most severe climbing to 3 as informational messages
              , logtime      datetime not null default getdate()
            );
        end;

        /************************************************************************************
            Create local logging table.
        ************************************************************************************/

        declare @logsystem table
        (
			pklog        numeric(18, 0) not null identity(1, 1)
		  , messagetype  nvarchar(50) not null
		  , logmessage   nvarchar(1000) not null
		  , severity     int null
		  , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
		  , logtime      datetime not null default getdate()
        );
		
        begin try
            select @start_time = @current_datetime;

            -- set some default values
            select @default_upsrt_soft_comp_id = upsrt_soft_comp_id
            from dbo.lkup_soft_comp
            where upsrt_soft_comp_nm = 'load';

            select @default_upsrt_usr_id = usr_id
            from dbo.usr
            where usr_usrnm = 'CNVRSN';

            -- setup the looping variables to process in chucks
            set @loop_count = 1;

            select @total_count = count(*)
            from etl.mu_UDEFCBR_ACCOUNT;

            select @start_key = min(id)
            from etl.mu_UDEFCBR_ACCOUNT;

            set @total_loops = @total_count / @chunk_size + 1;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - START',
				'Starting ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

            while @loop_count > 0
            begin
				
				INSERT INTO [dbo].[UDEFCBR_ACCOUNT]
				(
					[cnsmr_accnt_id],
	                [UDEFACCNT_STS_CD_CHG_DT],
	                [UDEFBASE_9_ACCT_TYPE_CD],
	                [UDEFBASE_8_PRTFL_TY],
	                [UDEFBASE_14_TERM_FREQ],
	                [UDEFLST_PAY_DT],
	                [UDEFBASE_18_PHP_CD],
	                [UDEFBASE_27_DT_LAST_PYMNT],
	                [UDEFCBR_CLOSE_DT],
	                [UDEFBASE_20_CCC_CD],
	                [UDEFSUPP_RSN_CD],
	                [UDEFRPT_ONE_TIME],
	                [UDEFBASE_11_CR_LIMIT],
	                [UDEFBASE_17A_ACCT_STATUS_CD],
	                [UDEFPRE_CO_METRO_ST],
	                [UDEFCB_LST_PMNT_AMNT],
	                [UDEFBASE_13_TERM_DUR],
	                [UDEFBASE_12_H_CR],
	                [UDEFORG_CHRG_OFF_AMT],
					[upsrt_dttm],
					[upsrt_soft_comp_id],
					[upsrt_trnsctn_nmbr],
					[upsrt_usr_id]
				)
				SELECT ca.cnsmr_accnt_id,
	                   stg.UDEFACCNT_STS_CD_CHG_DT,
	                   stg.UDEFBASE_9_ACCT_TYPE_CD,
	                   stg.UDEFBASE_8_PRTFL_TY,
	                   stg.UDEFBASE_14_TERM_FREQ,
	                   stg.UDEFLST_PAY_DT,
	                   stg.UDEFBASE_18_PHP_CD,
	                   stg.UDEFBASE_27_DT_LAST_PYMNT,
	                   stg.UDEFCBR_CLOSE_DT,
	                   stg.UDEFBASE_20_CCC_CD,
	                   stg.UDEFSUPP_RSN_CD,
	                   stg.UDEFRPT_ONE_TIME,
	                   stg.UDEFBASE_11_CR_LIMIT,
	                   stg.UDEFBASE_17A_ACCT_STATUS_CD,
	                   stg.UDEFPRE_CO_METRO_ST,
	                   stg.UDEFCB_LST_PMNT_AMNT,
	                   stg.UDEFBASE_13_TERM_DUR,
	                   stg.UDEFBASE_12_H_CR,
	                   stg.UDEFORG_CHRG_OFF_AMT,
					   GETDATE() AS upsrt_dttm,
					   @Default_upsrt_soft_comp_id AS upsrt_soft_comp_id,
					   1 AS upsrt_trnsctn_nmbr,
					   @Default_upsrt_usr_id AS upsrt_usr_id
				FROM   ETL.mu_UDEFCBR_ACCOUNT stg
					   INNER JOIN [dbo].[cnsmr_accnt] ca
						 ON ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
				WHERE  stg.id >= @start_key
					   AND stg.id < @start_key + @chunk_size;
									
				INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
				VALUES
				(
					@msg_tbl_nm,
					'Completed insert into ' + @msg_tbl_nm + ', ' + convert(nvarchar, @@rowcount) + ' rows inserted.  Loop: ' + convert(nvarchar, @loop_count),
					3
				)

                -- advance the start_key for the next set of records in next loop
                set @start_key = @start_key + @chunk_size;

                -- advance the loop
                if @loop_count < @total_loops
                    set @loop_count = @loop_count + 1;
                else
                    set @loop_count = 0;

            end;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - FINISHED',
				'Completed ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

			set @run_time = convert(nvarchar, dateadd(ms, datediff(second, @start_time, dbo.GETDATE()) * 1000, 0), 114);

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - RUN TIME',
				@msg_tbl_nm + ' Total Run Time: ' + ISNULL(@run_time, ''),
				3
			);

        end try

        begin catch
            if @@trancount > 0
                rollback;

            select @err_msg = error_message()
                 , @err_severity = error_severity()
                 , @err_num = error_number();

            insert into etl.mu_processing_log (messagetype, logmessage, severity)
            values('ERROR', cast(@err_num as varchar) + ' ' + @err_msg, @err_severity);

            insert into etl.mu_processing_log(messagetype, logmessage, logginglevel)
            values('ERROR', 'UDEFCBR_ACCOUNT Rolled Back.', 0);

            raiserror(@err_msg, @err_severity, 16);
        end catch;

        /***********************************************************
            Dump the logging to a permanent table.  
        ***********************************************************/

--        insert into etl.mu_processing_log(messagetype, logmessage, severity, logginglevel, logtime)
--        select messagetype, logmessage, severity, logginglevel, logtime
--        from @logsystem;
		
    end;
GO

