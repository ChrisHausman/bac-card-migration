use crs5_oltp;
go

-- drop existing staging table if present

if object_id('ETL.mu_UDEFACCT_DETAILS', 'U') is not null
    drop table etl.mu_UDEFACCT_DETAILS;

-- mu_UDEFACCT_DETAILS staging table
CREATE TABLE [ETL].[mu_UDEFACCT_DETAILS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cnsmr_accnt_idntfr_lgcy_txt] [varchar](20) NULL,
	[UDEFNUM_PYMTS_REMAIN] [int] NULL,
	[UDEFTPC] [varchar](15) NULL,
	[UDEFSCRA_FLAG] [varchar](1) NULL,
	[UDEFNUM60DAY_DELQ] [int] NULL,
	[UDEFNUM30DAY_DELQ] [int] NULL,
	[UDEFINV_CD] [varchar](4) NULL,
	[UDEFCONV_ACCT_NUM] [varchar](25) NULL,
	[UDEFLN_ORIG_STATE] [varchar](5) NULL,
	[UDEFCONTRA_COST_CN] [varchar](7) NULL,
	[UDEFLST_NSF_DT] [datetime] NULL,
	[UDEFCRD_DELQ_CO] [varchar](15) NULL,
	[UDEFSUB_POOL] [varchar](3) NULL,
	[UDEFCR_LIFE_INS_VAL] [varchar](256) NULL,
	[UDEFFCS_CMPNY_COST_CNTR] [varchar](50) NULL,
	[UDEFDLR_CD] [varchar](16) NULL,
	[UDEFIND_FLG_VAL] [varchar](256) NULL,
	[UDEFTBD_DOO_VAL] [varchar](256) NULL,
	[UDEFPRE_ACCT_NUM] [varchar](25) NULL,
	[UDEFPRE_ACCT_NUM_OTHR] [varchar](25) NULL,
	[UDEFRSN_FOR_LOSS] [varchar](4) NULL,
	[UDEFCPC] [varchar](15) NULL,
	[UDEFCB_OVRD] [varchar](3) NULL,
	[UDEFNUM90DAY_DELQ] [int] NULL,
	[UDEFAPPL_CODE_VAL] [varchar](256) NULL,
	[UDEFLST_RTL_TRAN_DT] [datetime] NULL
) ON [PRIMARY]

-- create the processing stored procedure

if object_id('ETL.sp_mu_UDEFACCT_DETAILS', 'P') is not null
    drop procedure etl.sp_mu_UDEFACCT_DETAILS;
go

/* ****************************************************************************************************************** */
/*   Name: Direct Load ACCT_DETAILS UDP                                                                               */
/*   Staging Table: ETL.mu_UDEFACCT_DETAILS                                                                           */
/*   Processing Stored Procedure: ETL.sp_mu_UDEFACCT_DETAILS                                                          */
/*   Date:  12/14/2021                                                                                                */
/*   Purpose:  Custom DL script for loading data into ACCT_DETAILS UDP                                                */
/*   Change Log:                                                                                                      */
/*       Version     Description                         Developer               Date                                 */
/*       ------------------------------------------------------------------------------------------------------------ */
/*       11.1        Initial - for BAC TSYS migration    Chris Hausman           12/14/2021                           */
/*       11.1.1      Updated for new version of UDP.     Chris Hausman           12/20/2021                           */
/*       11.1.2      Fixed bug with error logging.       Chris Hausman           04/29/2022                           */
/*       ------------------------------------------------------------------------------------------------------------ */
/*  TO DO LIST                                                                                                        */
/*  ----------                                                                                                        */
/*   (1) N/A                                                                                                          */
/* ****************************************************************************************************************** */

create   procedure etl.sp_mu_UDEFACCT_DETAILS @chunk_size bigint = 10000
as
    begin
        declare @err_msg nvarchar(4000)= N'';
        declare @err_severity int;
        declare @err_num int;

        declare @msg_tbl_nm nvarchar(50)= N'UDEFACCT_DETAILS';

        declare @start_time datetime;
        declare @run_time varchar(20);
        declare @loop_count int;
        declare @total_count bigint;
        declare @total_loops int;
        declare @start_key bigint;

        declare @default_upsrt_soft_comp_id bigint;
        declare @default_upsrt_usr_id bigint;
		
        declare @current_datetime as datetime = dbo.GETDATE();

		declare @udef_adt_id as bigint;
		declare @cr_life_ins_adc_id as bigint;
		declare @ind_flg_adc_id as bigint;
		declare @tbd_doo_adc_id as bigint;
		declare @appl_code_adc_id as bigint;
		
        /**************************************************************************************
            Create permanent logging table.  
        **************************************************************************************/

        --This table will keep track of ETL progress.
        if not exists (select 1 from information_schema.tables where table_name = 'mu_Processing_Log')
        begin
            create table etl.mu_processing_log
            (
				pklog        numeric(18, 0) identity(1, 1) not null
			  , messagetype  nvarchar(50) not null
              , logmessage   nvarchar(1000) null
              , severity     int null
              , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
              , logtime      datetime not null default getdate()
            );
        end;

        /************************************************************************************
            Create local logging table.
        ************************************************************************************/

        declare @logsystem table
        (
			pklog        numeric(18, 0) not null identity(1, 1)
		  , messagetype  nvarchar(50) not null
		  , logmessage   nvarchar(1000) not null
		  , severity     int null
		  , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
		  , logtime      datetime not null default getdate()
        );
		
        begin try
            select @start_time = @current_datetime;

            -- set some default values
            select @default_upsrt_soft_comp_id = upsrt_soft_comp_id
            from dbo.lkup_soft_comp
            where upsrt_soft_comp_nm = 'load';

            select @default_upsrt_usr_id = usr_id
            from dbo.usr
            where usr_usrnm = 'CNVRSN';

			--set the variables for the list of value fields
			select @udef_adt_id = agncy_dfnd_tbl_id
			from   agncy_Dfnd_Tbl
			where  agncy_dfnd_tbl_nm = 'UDEFACCT_DETAILS'
			
			select @cr_life_ins_adc_id = agncy_dfnd_clmn_id
			from   agncy_Dfnd_Clmn
			where  agncy_dfnd_clmn_nm = 'UDEFCR_LIFE_INS'
			       and agncy_dfnd_tbl_id = @udef_adt_id

			select @ind_flg_adc_id = agncy_dfnd_clmn_id
			from   agncy_Dfnd_Clmn
			where  agncy_dfnd_clmn_nm = 'UDEFIND_FLG'
			       and agncy_dfnd_tbl_id = @udef_adt_id

			select @tbd_doo_adc_id = agncy_dfnd_clmn_id
			from   agncy_Dfnd_Clmn
			where  agncy_dfnd_clmn_nm = 'UDEFTBD_DOO'
			       and agncy_dfnd_tbl_id = @udef_adt_id

			select @appl_code_adc_id = agncy_dfnd_clmn_id
			from   agncy_Dfnd_Clmn
			where  agncy_dfnd_clmn_nm = 'UDEFAPPL_CODE'
			       and agncy_dfnd_tbl_id = @udef_adt_id

            -- setup the looping variables to process in chucks
            set @loop_count = 1;

            select @total_count = count(*)
            from etl.mu_UDEFACCT_DETAILS;

            select @start_key = min(id)
            from etl.mu_UDEFACCT_DETAILS;

            set @total_loops = @total_count / @chunk_size + 1;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - START',
				'Starting ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

            while @loop_count > 0
            begin
				
				INSERT INTO [dbo].[UDEFACCT_DETAILS]
				(
					[cnsmr_accnt_id],
					[UDEFNUM_PYMTS_REMAIN],
					[UDEFTPC],
					[UDEFSCRA_FLAG],
					[UDEFNUM60DAY_DELQ],
					[UDEFNUM30DAY_DELQ],
					[UDEFINV_CD],
					[UDEFCONV_ACCT_NUM],
					[UDEFLN_ORIG_STATE],
					[UDEFCONTRA_COST_CN],
					[UDEFLST_NSF_DT],
					[UDEFCRD_DELQ_CO],
					[UDEFSUB_POOL],
					[UDEFCR_LIFE_INS],
					[UDEFFCS_CMPNY_COST_CNTR],
					[UDEFDLR_CD],
					[UDEFIND_FLG],
					[UDEFTBD_DOO],
					[UDEFPRE_ACCT_NUM],
					[UDEFPRE_ACCT_NUM_OTHR],
					[UDEFRSN_FOR_LOSS],
					[UDEFCPC],
					[UDEFCB_OVRD],
					[UDEFNUM90DAY_DELQ],
					[UDEFAPPL_CODE],
					[UDEFLST_RTL_TRAN_DT],
					[upsrt_dttm],
					[upsrt_soft_comp_id],
					[upsrt_trnsctn_nmbr],
					[upsrt_usr_id]
				)
				SELECT ca.cnsmr_accnt_id AS cnsmr_accnt_id,
					   stg.[UDEFNUM_PYMTS_REMAIN],
					   stg.[UDEFTPC],
					   stg.[UDEFSCRA_FLAG],
					   stg.[UDEFNUM60DAY_DELQ],
					   stg.[UDEFNUM30DAY_DELQ],
					   stg.[UDEFINV_CD],
					   stg.[UDEFCONV_ACCT_NUM],
					   stg.[UDEFLN_ORIG_STATE],
					   stg.[UDEFCONTRA_COST_CN],
					   stg.[UDEFLST_NSF_DT],
					   stg.[UDEFCRD_DELQ_CO],
					   stg.[UDEFSUB_POOL],
					   uvl_cr_life_ins.udf_val_list_id,
					   stg.[UDEFFCS_CMPNY_COST_CNTR],
					   stg.[UDEFDLR_CD],
					   uvl_ind_flg.udf_val_list_id,
					   uvl_tbd_doo.udf_val_list_id,
					   stg.[UDEFPRE_ACCT_NUM],
					   stg.[UDEFPRE_ACCT_NUM_OTHR],
					   stg.[UDEFRSN_FOR_LOSS],
					   stg.[UDEFCPC],
					   stg.[UDEFCB_OVRD],
					   stg.[UDEFNUM90DAY_DELQ],
					   uvl_appl_code.udf_val_list_id,
					   stg.[UDEFLST_RTL_TRAN_DT],
					   GETDATE() AS upsrt_dttm,
					   @Default_upsrt_soft_comp_id AS upsrt_soft_comp_id,
					   1 AS upsrt_trnsctn_nmbr,
					   @Default_upsrt_usr_id AS upsrt_usr_id
				FROM   ETL.mu_UDEFACCT_DETAILS stg
					   INNER JOIN [dbo].[cnsmr_accnt] ca
						 ON ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
					   LEFT JOIN udf_val_lst uvl_cr_life_ins
					     ON stg.UDEFCR_LIFE_INS_VAL = uvl_cr_life_ins.udf_val_txt
						    AND uvl_cr_life_ins.agncy_dfnd_clmn_id = @cr_life_ins_adc_id
					   LEFT JOIN udf_val_lst uvl_ind_flg
					     ON stg.UDEFIND_FLG_VAL = uvl_ind_flg.udf_val_txt
						    AND uvl_ind_flg.agncy_dfnd_clmn_id = @ind_flg_adc_id
					   LEFT JOIN udf_val_lst uvl_tbd_doo
					     ON stg.UDEFTBD_DOO_VAL = uvl_tbd_doo.udf_val_txt
						    AND uvl_tbd_doo.agncy_dfnd_clmn_id = @tbd_doo_adc_id
					   LEFT JOIN udf_val_lst uvl_appl_code
					     ON stg.UDEFAPPL_CODE_VAL = uvl_appl_code.udf_val_txt
						    AND uvl_appl_code.agncy_dfnd_clmn_id = @appl_code_adc_id
				WHERE  stg.id >= @start_key
					   AND stg.id < @start_key + @chunk_size;
									
				INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
				VALUES
				(
					@msg_tbl_nm,
					'Completed insert into ' + @msg_tbl_nm + ', ' + convert(nvarchar, @@rowcount) + ' rows inserted.  Loop: ' + convert(nvarchar, @loop_count),
					3
				)

                -- advance the start_key for the next set of records in next loop
                set @start_key = @start_key + @chunk_size;

                -- advance the loop
                if @loop_count < @total_loops
                    set @loop_count = @loop_count + 1;
                else
                    set @loop_count = 0;

            end;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - FINISHED',
				'Completed ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

			set @run_time = convert(nvarchar, dateadd(ms, datediff(second, @start_time, dbo.GETDATE()) * 1000, 0), 114);

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - RUN TIME',
				@msg_tbl_nm + ' Total Run Time: ' + ISNULL(@run_time, ''),
				3
			);

        end try

        begin catch
            if @@trancount > 0
                rollback;

            select @err_msg = error_message()
                 , @err_severity = error_severity()
                 , @err_num = error_number();

            insert into etl.mu_processing_log (messagetype, logmessage, severity)
            values('ERROR', cast(@err_num as varchar) + ' ' + @err_msg, @err_severity);

            insert into etl.mu_processing_log(messagetype, logmessage, logginglevel)
            values('ERROR', 'UDEFACCT_DETAILS Rolled Back.', 0);

            raiserror(@err_msg, @err_severity, 16);
        end catch;

        /***********************************************************
            Dump the logging to a permanent table.  
        ***********************************************************/

--        insert into etl.mu_processing_log(messagetype, logmessage, severity, logginglevel, logtime)
--        select messagetype, logmessage, severity, logginglevel, logtime
--        from @logsystem;
		
    end;
GO

