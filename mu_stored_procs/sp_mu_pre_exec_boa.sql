use crs5_oltp;
go

-- create the processing stored procedure
if object_id('ETL.sp_mu_pre_exec_boa', 'P') is not null
    drop procedure etl.sp_mu_pre_exec_boa;
go

/* ****************************************************************************************************************** */
/*   Name: Migration Utility - Pre MU Execution Process                                                               */
/*   Staging Table: N/A                                                                                               */
/*   Processing Stored Procedure: ETL.sp_mu_pre_exec_boa                                                              */
/*   Date:  04/14/2021                                                                                                */
/*   Purpose:  Custom index maintanence prior to MU load.                                                             */
/*   Change Log:                                                                                                      */
/*       Version     Description                         Developer               Date                                 */
/*       ------------------------------------------------------------------------------------------------------------ */
/*       11.2        Initial                             Chris Hausman           12/13/2021                           */
/*       11.2        Fixed bug with error logging.       Chris Hausman           04/29/2022                           */
/*       11.2        Direct to table logging.            Andrew Sheldon          04/12/2023                           */
/*       11.3        Added index on account legacy.      Chris Hausman           04/29/2024                           */
/*       ------------------------------------------------------------------------------------------------------------ */
/*  TO DO LIST                                                                                                        */
/*  ----------                                                                                                        */
/* ****************************************************************************************************************** */

create   procedure etl.sp_mu_pre_exec_boa
as
    begin
        declare @err_msg nvarchar(4000)= N'';
        declare @err_severity int;
        declare @err_num int;
	
        declare @current_datetime as datetime = dbo.getcurrdt();
		
        /**************************************************************************************
            Create permanent logging table.
        **************************************************************************************/

        --This table will keep track of ETL progress.
        if not exists (select 1 from information_schema.tables where table_name = 'mu_Processing_Log')
        begin
            create table etl.mu_processing_log
            (
				pklog        numeric(18, 0) identity(1, 1) not null
			  , messagetype  nvarchar(50) not null
              , logmessage   nvarchar(1000) null
              , severity     int null
              , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
              , logtime      datetime not null default getdate()
            );
        end;

        /************************************************************************************
            Create local logging table.
        ************************************************************************************/

        declare @logsystem table
        (
			pklog        numeric(18, 0) not null identity(1, 1)
		  , messagetype  nvarchar(50) not null
		  , logmessage   nvarchar(1000) not null
		  , severity     int null
		  , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
		  , logtime      datetime not null default getdate()
        );
		
		begin try
		
			/* ****************************************************************************************************** */
			/*  Index creation for cnsmr_idntfr_drvr_lcns_txt and agncy_entty_crrltn_id on all DL/MU staging tables.  */
			/* ****************************************************************************************************** */
			
			--Index for agncy_entty_crrltn_id & cnsmr_idntfr_drvr_lcns_txt on cnsmr table.
			IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'cnsmr_dl_num')
				CREATE NONCLUSTERED INDEX cnsmr_dl_num ON dbo.cnsmr (agncy_entty_crrltn_id, cnsmr_idntfr_drvr_lcns_txt)
			ELSE
				ALTER INDEX cnsmr_dl_num ON dbo.cnsmr REBUILD WITH (ONLINE = OFF)
			
			INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
			VALUES ('sp_mu_pre_exec_boa', 'Finished creation of cnsmr_dl_num index.', 3)
			
			--Index for cnsmr_accnt_idntfr_lgcy_txt on cnsmr_accnt table.
			IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'cnsmr_accnt_lgcy_txt')
				CREATE NONCLUSTERED INDEX cnsmr_accnt_lgcy_txt ON dbo.cnsmr_accnt (cnsmr_accnt_idntfr_lgcy_txt)
			ELSE
				ALTER INDEX cnsmr_accnt_lgcy_txt ON dbo.cnsmr_accnt REBUILD WITH (ONLINE = OFF)
			
			INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
			VALUES ('sp_mu_pre_exec_boa', 'Finished creation of cnsmr_accnt_lgcy_txt index.', 3)
		
		end try

        begin catch
            if @@trancount > 0
                rollback;

            select @err_msg = error_message()
                 , @err_severity = error_severity()
                 , @err_num = error_number();

            insert into etl.mu_processing_log (messagetype, logmessage, severity)
            values('ERROR', cast(@err_num as varchar) + ' ' + @err_msg, @err_severity);

            insert into etl.mu_processing_log(messagetype, logmessage, logginglevel)
            values('ERROR', 'Pre MU Exec BOA Rolled Back.', 0);

            raiserror(@err_msg, @err_severity, 16);
        end catch;

        /***********************************************************
            Dump the logging to a permanent table.  
        ***********************************************************/

--        insert into etl.mu_processing_log(messagetype, logmessage, severity, logginglevel, logtime)
--        select messagetype, logmessage, severity, logginglevel, logtime
--        from @logsystem;
		
    end;
GO

