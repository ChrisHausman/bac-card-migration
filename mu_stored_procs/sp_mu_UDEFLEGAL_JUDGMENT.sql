use crs5_oltp;
go

-- drop existing staging table if present

if object_id('ETL.mu_UDEFLEGAL_JUDGMENT', 'U') is not null
    drop table etl.mu_UDEFLEGAL_JUDGMENT;

-- mu_UDEFLEGAL_JUDGMENT staging table
CREATE TABLE [ETL].[mu_UDEFLEGAL_JUDGMENT](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cnsmr_accnt_idntfr_lgcy_txt] [varchar](20) NULL,
	[UDEFJUDGMNT_RENEW_DT] [datetime] NULL,
	[UDEFATTRNY_FIRM] [varchar](80) NULL,
	[UDEFSTAT_LIM_DT] [datetime] NULL,
	[UDEFJUDGMNT_VAC_DT] [datetime] NULL,
	[UDEFWOP_DT] [datetime] NULL,
	[UDEFANSW_FLD_DT] [datetime] NULL,
	[UDEFDOCKET_NUM] [varchar](50) NULL,
	[UDEFNON_RECOV_COST] [decimal](12, 3) NULL,
	[UDEFJUDG_ON] [varchar](50) NULL,
	[UDEFJUDGE_OBT_DT] [datetime] NULL,
	[UDEFCOMP_FLD_DT] [datetime] NULL,
	[UDEFST_FILED] [varchar](2) NULL,
	[UDEFSUIT_INT] [decimal](12, 3) NULL,
	[UDEFJUDG_BOOK] [varchar](50) NULL,
	[UDEFAFFID_FL_DT] [datetime] NULL,
	[UDEFJUDGE_EXPR_DT] [datetime] NULL,
	[UDEFJUDGE_INT] [decimal](12, 3) NULL,
	[UDEFDISMISS_DT] [datetime] NULL,
	[UDEFSERVED_DT] [datetime] NULL,
	[UDEFCOURT_VENUE] [varchar](80) NULL,
	[UDEFSUIT_TOTAL] [decimal](12, 3) NULL,
	[UDEFJUDG_PAGE] [varchar](50) NULL,
	[UDEFJUDG_TYP_VAL] [varchar](256) NULL,
	[UDEFSUIT_FILE_DATE] [datetime] NULL,
	[UDEFJUDGE_COST] [decimal](12, 3) NULL,
	[UDEFJUDGE_PRINC] [decimal](12, 3) NULL,
	[UDEFNM_ST_ATTRY] [varchar](50) NULL,
	[UDEFPLNTFF_NM] [varchar](120) NULL,
	[UDEFBOND_FF_VAL] [varchar](256) NULL,
	[UDEFSUIT_DISMISS_DT] [datetime] NULL,
	[UDEFCASE_NUM] [varchar](50) NULL,
	[UDEFAFFID_RQ_DT] [datetime] NULL,
	[UDEFBOND_RQ_VAL] [varchar](256) NULL,
	[UDEFSAT_JUDGE_DT] [datetime] NULL,
	[UDEFATTRNY_FEE_AMT] [decimal](12, 3) NULL,
	[UDEFSUIT_COST] [decimal](12, 3) NULL,
	[UDEFATTRNY_ASSGN_DT] [datetime] NULL,
	[UDEFJUDG_REC_DT] [datetime] NULL,
	[UDEFSUIT_PRINC] [decimal](12, 3) NULL,
	[UDEFRECOV_COST] [decimal](12, 3) NULL,
	[UDEFATTRNY_PHONE] [varchar](20) NULL,
	[UDEFSCRA_ATTRY_CK_DT] [datetime] NULL,
	[UDEFJUDGE_STATUS_VAL] [varchar](256) NULL,
	[UDEFJUDG_REDCTN_AMT] [decimal](12, 3) NULL,
	[UDEFFRM_ID] [varchar](20) NULL,
	[UDEFRCVR] [varchar](32) NULL
) ON [PRIMARY]

-- create the processing stored procedure

if object_id('ETL.sp_mu_UDEFLEGAL_JUDGMENT', 'P') is not null
    drop procedure etl.sp_mu_UDEFLEGAL_JUDGMENT;
go

/* ****************************************************************************************************************** */
/*   Name: Direct Load LEGAL_JUDGMENT UDP                                                                             */
/*   Staging Table: ETL.mu_UDEFLEGAL_JUDGMENT                                                                         */
/*   Processing Stored Procedure: ETL.sp_mu_UDEFLEGAL_JUDGMENT                                                        */
/*   Date:  11/02/2021                                                                                                */
/*   Purpose:  Custom DL script for loading data into LEGAL_JUDGMENT UDP                                              */
/*   Change Log:                                                                                                      */
/*       Version     Description                         Developer               Date                                 */
/*       ------------------------------------------------------------------------------------------------------------ */
/*       1.0         Initial - for BAC TSYS migration    Chris Hausman           11/17/2021                           */
/*       1.1         Added FRM_ID & RCVR fields          Chris Hausman           03/17/2022                           */
/*       1.2         Fixed bug with error logging.       Chris Hausman           04/29/2022                           */
/*       1.3         Direct to table logging.            Andrew Sheldon          04/12/2023                           */
/*       ------------------------------------------------------------------------------------------------------------ */
/*  TO DO LIST                                                                                                        */
/*  ----------                                                                                                        */
/*   (1) N/A                                                                                                          */
/* ****************************************************************************************************************** */

create   procedure etl.sp_mu_UDEFLEGAL_JUDGMENT @chunk_size bigint = 10000
as
    begin
        declare @err_msg nvarchar(4000)= N'';
        declare @err_severity int;
        declare @err_num int;

        declare @msg_tbl_nm nvarchar(50)= N'UDEFLEGAL_JUDGMENT';

        declare @start_time datetime;
        declare @run_time varchar(20);
        declare @loop_count int;
        declare @total_count bigint;
        declare @total_loops int;
        declare @start_key bigint;

        declare @default_upsrt_soft_comp_id bigint;
        declare @default_upsrt_usr_id bigint;
		
        declare @current_datetime as datetime = dbo.GETDATE();

		declare @udef_adt_id as bigint;
		declare @judg_typ_adc_id as bigint;
		declare @bond_ff_adc_id as bigint;
		declare @bond_rq_adc_id as bigint;
		declare @judge_status_adc_id as bigint;
		
        /**************************************************************************************
            Create permanent logging table.  
        **************************************************************************************/

        --This table will keep track of ETL progress.
        if not exists (select 1 from information_schema.tables where table_name = 'mu_Processing_Log')
        begin
            create table etl.mu_processing_log
            (
				pklog        numeric(18, 0) identity(1, 1) not null
			  , messagetype  nvarchar(50) not null
              , logmessage   nvarchar(1000) null
              , severity     int null
              , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
              , logtime      datetime not null default getdate()
            );
        end;

        /************************************************************************************
            Create local logging table.
        ************************************************************************************/

        declare @logsystem table
        (
			pklog        numeric(18, 0) not null identity(1, 1)
		  , messagetype  nvarchar(50) not null
		  , logmessage   nvarchar(1000) not null
		  , severity     int null
		  , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
		  , logtime      datetime not null default getdate()
        );
		
        begin try
            select @start_time = @current_datetime;

            -- set some default values
            select @default_upsrt_soft_comp_id = upsrt_soft_comp_id
            from dbo.lkup_soft_comp
            where upsrt_soft_comp_nm = 'load';

            select @default_upsrt_usr_id = usr_id
            from dbo.usr
            where usr_usrnm = 'CNVRSN';

			--set the variables for the list of value fields
			select @udef_adt_id = agncy_dfnd_tbl_id
			from   agncy_Dfnd_Tbl
			where  agncy_dfnd_tbl_nm = 'UDEFLEGAL_JUDGMENT'
			
			select @judg_typ_adc_id = agncy_dfnd_clmn_id
			from   agncy_Dfnd_Clmn
			where  agncy_dfnd_clmn_nm = 'UDEFJUDG_TYP'
			       and agncy_dfnd_tbl_id = @udef_adt_id

			select @bond_ff_adc_id = agncy_dfnd_clmn_id
			from   agncy_Dfnd_Clmn
			where  agncy_dfnd_clmn_nm = 'UDEFBOND_FF'
			       and agncy_dfnd_tbl_id = @udef_adt_id

			select @bond_rq_adc_id = agncy_dfnd_clmn_id
			from   agncy_Dfnd_Clmn
			where  agncy_dfnd_clmn_nm = 'UDEFBOND_RQ'
			       and agncy_dfnd_tbl_id = @udef_adt_id

			select @judge_status_adc_id = agncy_dfnd_clmn_id
			from   agncy_Dfnd_Clmn
			where  agncy_dfnd_clmn_nm = 'UDEFJUDGE_STATUS'
			       and agncy_dfnd_tbl_id = @udef_adt_id

            -- setup the looping variables to process in chucks
            set @loop_count = 1;

            select @total_count = count(*)
            from etl.mu_UDEFLEGAL_JUDGMENT;

            select @start_key = min(id)
            from etl.mu_UDEFLEGAL_JUDGMENT;

            set @total_loops = @total_count / @chunk_size + 1;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - START',
				'Starting ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

            while @loop_count > 0
            begin
				
				INSERT INTO [dbo].[UDEFLEGAL_JUDGMENT]
				(
					[cnsmr_accnt_id],
					[UDEFJUDGMNT_RENEW_DT],
					--[UDEFATTRNY_FIRM],
					[UDEFSTAT_LIM_DT],
					[UDEFJUDGMNT_VAC_DT],
					[UDEFWOP_DT],
					[UDEFANSW_FLD_DT],
					[UDEFDOCKET_NUM],
					[UDEFNON_RECOV_COST],
					[UDEFJUDG_ON],
					[UDEFJUDGE_OBT_DT],
					[UDEFCOMP_FLD_DT],
					[UDEFST_FILED],
					[UDEFSUIT_INT],
					[UDEFJUDG_BOOK],
					[UDEFAFFID_FL_DT],
					[UDEFJUDGE_EXPR_DT],
					[UDEFJUDGE_INT],
					[UDEFDISMISS_DT],
					[UDEFSERVED_DT],
					[UDEFCOURT_VENUE],
					[UDEFSUIT_TOTAL],
					[UDEFJUDG_PAGE],
					[UDEFJUDG_TYP],
					[UDEFSUIT_FILE_DATE],
					[UDEFJUDGE_COST],
					[UDEFJUDGE_PRINC],
					[UDEFNM_ST_ATTRY],
					[UDEFPLNTFF_NM],
					[UDEFBOND_FF],
					[UDEFSUIT_DISMISS_DT],
					[UDEFCASE_NUM],
					[UDEFAFFID_RQ_DT],
					[UDEFBOND_RQ],
					[UDEFSAT_JUDGE_DT],
					[UDEFATTRNY_FEE_AMT],
					[UDEFSUIT_COST],
					[UDEFATTRNY_ASSGN_DT],
					[UDEFJUDG_REC_DT],
					[UDEFSUIT_PRINC],
					[UDEFRECOV_COST],
					--[UDEFATTRNY_PHONE],
					[UDEFSCRA_ATTRY_CK_DT],
					[UDEFJUDGE_STATUS],
					[UDEFJUDG_REDCTN_AMT],
					[upsrt_dttm],
					[upsrt_soft_comp_id],
					[upsrt_trnsctn_nmbr],
					[upsrt_usr_id],
					[UDEFFRM_ID],
					[UDEFRCVR]
				)
				SELECT ca.cnsmr_accnt_id AS cnsmr_accnt_id,
					   stg.[UDEFJUDGMNT_RENEW_DT],
					   --stg.[UDEFATTRNY_FIRM],
					   stg.[UDEFSTAT_LIM_DT],
					   stg.[UDEFJUDGMNT_VAC_DT],
					   stg.[UDEFWOP_DT],
					   stg.[UDEFANSW_FLD_DT],
					   stg.[UDEFDOCKET_NUM],
					   stg.[UDEFNON_RECOV_COST],
					   stg.[UDEFJUDG_ON],
					   stg.[UDEFJUDGE_OBT_DT],
					   stg.[UDEFCOMP_FLD_DT],
					   stg.[UDEFST_FILED],
					   stg.[UDEFSUIT_INT],
					   stg.[UDEFJUDG_BOOK],
					   stg.[UDEFAFFID_FL_DT],
					   stg.[UDEFJUDGE_EXPR_DT],
					   stg.[UDEFJUDGE_INT],
					   stg.[UDEFDISMISS_DT],
					   stg.[UDEFSERVED_DT],
					   stg.[UDEFCOURT_VENUE],
					   stg.[UDEFSUIT_TOTAL],
					   stg.[UDEFJUDG_PAGE],
					   uvl_judg_typ.udf_val_list_id,
					   stg.[UDEFSUIT_FILE_DATE],
					   stg.[UDEFJUDGE_COST],
					   stg.[UDEFJUDGE_PRINC],
					   stg.[UDEFNM_ST_ATTRY],
					   stg.[UDEFPLNTFF_NM],
					   uvl_bond_ff.udf_val_list_id,
					   stg.[UDEFSUIT_DISMISS_DT],
					   stg.[UDEFCASE_NUM],
					   stg.[UDEFAFFID_RQ_DT],
					   uvl_bond_rq.udf_val_list_id,
					   stg.[UDEFSAT_JUDGE_DT],
					   stg.[UDEFATTRNY_FEE_AMT],
					   stg.[UDEFSUIT_COST],
					   stg.[UDEFATTRNY_ASSGN_DT],
					   stg.[UDEFJUDG_REC_DT],
					   stg.[UDEFSUIT_PRINC],
					   stg.[UDEFRECOV_COST],
					   --stg.[UDEFATTRNY_PHONE],
					   stg.[UDEFSCRA_ATTRY_CK_DT],
					   uvl_judge_status.udf_val_list_id,
					   stg.[UDEFJUDG_REDCTN_AMT],
					   GETDATE() AS upsrt_dttm,
					   @Default_upsrt_soft_comp_id AS upsrt_soft_comp_id,
					   1 AS upsrt_trnsctn_nmbr,
					   @Default_upsrt_usr_id AS upsrt_usr_id,
					   stg.[UDEFFRM_ID],
					   stg.[UDEFRCVR]
				FROM   ETL.mu_UDEFLEGAL_JUDGMENT stg
					   INNER JOIN [dbo].[cnsmr_accnt] ca
						 ON ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
					   LEFT JOIN udf_val_lst uvl_judg_typ
					     ON stg.UDEFJUDG_TYP_VAL = uvl_judg_typ.udf_val_txt
						    AND uvl_judg_typ.agncy_dfnd_clmn_id = @judg_typ_adc_id
					   LEFT JOIN udf_val_lst uvl_bond_ff
					     ON stg.UDEFBOND_FF_VAL = uvl_bond_ff.udf_val_txt
						    AND uvl_bond_ff.agncy_dfnd_clmn_id = @bond_ff_adc_id
					   LEFT JOIN udf_val_lst uvl_bond_rq
					     ON stg.UDEFBOND_RQ_VAL = uvl_bond_rq.udf_val_txt
						    AND uvl_bond_rq.agncy_dfnd_clmn_id = @bond_rq_adc_id
					   LEFT JOIN udf_val_lst uvl_judge_status
					     ON stg.UDEFJUDGE_STATUS_VAL = uvl_judge_status.udf_val_txt
						    AND uvl_judge_status.agncy_dfnd_clmn_id = @judge_status_adc_id
				WHERE  stg.id >= @start_key
					   AND stg.id < @start_key + @chunk_size;
									
				INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
				VALUES
				(
					@msg_tbl_nm,
					'Completed insert into ' + @msg_tbl_nm + ', ' + convert(nvarchar, @@rowcount) + ' rows inserted.  Loop: ' + convert(nvarchar, @loop_count),
					3
				)

                -- advance the start_key for the next set of records in next loop
                set @start_key = @start_key + @chunk_size;

                -- advance the loop
                if @loop_count < @total_loops
                    set @loop_count = @loop_count + 1;
                else
                    set @loop_count = 0;

            end;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - FINISHED',
				'Completed ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

			set @run_time = convert(nvarchar, dateadd(ms, datediff(second, @start_time, dbo.GETDATE()) * 1000, 0), 114);

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - RUN TIME',
				@msg_tbl_nm + ' Total Run Time: ' + ISNULL(@run_time, ''),
				3
			);

        end try

        begin catch
            if @@trancount > 0
                rollback;

            select @err_msg = error_message()
                 , @err_severity = error_severity()
                 , @err_num = error_number();

            insert into etl.mu_processing_log (messagetype, logmessage, severity)
            values('ERROR', cast(@err_num as varchar) + ' ' + @err_msg, @err_severity);

            insert into etl.mu_processing_log(messagetype, logmessage, logginglevel)
            values('ERROR', 'UDEFLEGAL_JUDGMENT Rolled Back.', 0);

            raiserror(@err_msg, @err_severity, 16);
        end catch;

        /***********************************************************
            Dump the logging to a permanent table.  
        ***********************************************************/

--        insert into etl.mu_processing_log(messagetype, logmessage, severity, logginglevel, logtime)
--        select messagetype, logmessage, severity, logginglevel, logtime
--        from @logsystem;
		
    end;
GO

