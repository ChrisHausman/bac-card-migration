USE [crs5_oltp]
GO

-- drop existing staging table if present
IF OBJECT_ID('ETL.mu_fnncl_trnsctn', 'U') IS NOT NULL
    DROP TABLE ETL.mu_fnncl_trnsctn;
GO

CREATE TABLE [ETL].[mu_fnncl_trnsctn](
	[id] BIGINT IDENTITY(1,1) NOT NULL,
    [CUSTOMER_ID] [varchar](50) NULL,
    [CUSTOMER_ACCOUNT_ID] [varchar](50) NULL,
    [PAYMENT_ID] [varchar](50) NULL,
    [PAYMENT_POSTED_DATE] [datetime] NULL,
    [PAYMENT_ENTERED_DATE] [datetime] NULL,
    [TOT_PAYMENT_AMT] [numeric](12, 4) NULL,
    [PAYMENT_TYP_CD] [varchar](64) NULL,
    [PAYMENT_LOC_CD] [varchar](64) NULL,
    [PAYMENT_INSTRUMENT_TYP_CD] [varchar](20) NULL,
    [MEMO_CD_SHRT_NM] [varchar](32) NULL,
    [FINANCIAL_BUCKET_NM] [varchar](50) NULL,
    [TRANSACTION_AMT] [numeric](12, 4) NULL,
    [CREATED_BY] [varchar](30) NULL,
    [CREATED_DTTM] [date] NULL,
    [PAYMENT_TAG_SHORT_NAME] [varchar](64) NULL,
    [WORKGROUP_SHORT_NAME] [varchar](64) NULL,
    [COMMENT] [varchar](MAX) NULL,
	[COMMISSION_AMOUNT] [numeric](11,3) NULL,
	[COMMISSION_PERCENT] [numeric](7,4) NULL,
	[RECEIVER_SHORT_NAME] [varchar](32) NULL,
	[RECEIVER_COMMISSION_AMOUNT] [numeric](11,3) NULL,
	[RECEIVER_COMMISSION_PERCENT] [numeric](7,4) NULL,
	[CURRENCY] [varchar](32) NULL,
    [cnsmr_id] [bigint] NULL,
    [cnsmr_accnt_id] [bigint] NULL,
    [cnsmr_pymnt_jrnl_id] [bigint] NULL,
    [cnsmr_accnt_pymnt_jrnl_id] [bigint] NULL,
    [bckt_id] [bigint] NULL,
    [INVALID] [tinyint] NULL,
    [crdtr_id] [bigint] NULL,
    [bckt_trnsctn_typ_cd] [bigint] NULL,
    [pymnt_lctn_cd] [bigint] NULL,
    [wrkgrp_id] [bigint] NULL,
	[pymnt_memo_id] [smallint] NULL,
	[crrncy_cd] [smallint] NULL,
	[rcvr_id] [bigint] NULL,
	[usr_id] [bigint] NULL
);
GO

IF OBJECT_ID('ETL.sp_mu_fnncl_trnsctn', 'P') IS NOT NULL
    DROP PROCEDURE ETL.sp_mu_fnncl_trnsctn;
GO

CREATE PROCEDURE [ETL].[sp_mu_fnncl_trnsctn] @chunk_size BIGINT = 10000, @cpi_chunk BIGINT = 20000, @cat_chunk BIGINT = 50000
AS 
BEGIN

	DECLARE @StatusMsg as varchar(2500)
	DECLARE @ErrMsg NVARCHAR(4000) = ''
	DECLARE @ErrSeverity INT
    
    declare @start_time datetime;
    declare @run_time varchar(20);
    declare @current_datetime datetime = dbo.getcurrdt();

	/* ********************************************************** */
	/*  Create permanent logging table just for financials step.  */
	/* ********************************************************** */

	--This table will keep track of ETL progress.
	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'mu_fnncl_trnsctn_log')
	BEGIN
		CREATE TABLE [ETL].[mu_fnncl_trnsctn_log]
		(
			[pkLog]        [numeric](18, 0) IDENTITY(1,1) NOT NULL,
			[MessageType]  [nvarchar](20) NOT NULL,
			[LogMessage]   [nvarchar](1000) NULL,
			[EmailAddress] [nvarchar](50) NULL,
			[Severity]     [int] NULL,
			[LoggingLevel] [int] NOT NULL,
			[LogTime]      [datetime] NOT NULL
		)
	END

	TRUNCATE TABLE ETL.mu_fnncl_trnsctn_log
    
    SELECT @start_time = @current_datetime;
    
    INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
    VALUES ('Fin Trnx - START', 'Starting financial transaction loading...', 3, GETDATE());

	/* **************************************************************************************************** */
	/*  Create temp table to keep track of distinct list of cnsmr_id for which we're importing financials.  */
	/* **************************************************************************************************** */
	
	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'mu_fnncl_trnsctn_cnsmr_list')
		DROP TABLE ETL.mu_fnncl_trnsctn_cnsmr_list
	
	CREATE TABLE ETL.mu_fnncl_trnsctn_cnsmr_list
	(
		id BIGINT IDENTITY(1,1) NOT NULL,
		cnsmr_id BIGINT NOT NULL
		PRIMARY KEY CLUSTERED (id ASC)
	)
	
	/* ****************************************************************************************************** */
	/*  Create temp table to keep track of distinct list of PAYMENT_ID for which we're importing financials.  */
	/* ****************************************************************************************************** */
	
	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'mu_fnncl_trnsctn_payment_id_list')
		DROP TABLE ETL.mu_fnncl_trnsctn_payment_id_list
	
	CREATE TABLE ETL.mu_fnncl_trnsctn_payment_id_list
	(
		id            BIGINT IDENTITY(1,1) NOT NULL,
		PAYMENT_ID    NVARCHAR(50) NOT NULL
		PRIMARY KEY CLUSTERED (id ASC)
	)

	DECLARE @curdate AS DATETIME
	DECLARE @upsrt_soft_comp_id AS INTEGER
	DECLARE @upsrt_usr_id AS INTEGER	
	DECLARE @status as INTEGER
	DECLARE @clerk_id AS INTEGER
	DECLARE @INT_CONTEXT AS BIGINT
	
	SELECT @curdate = GETDATE()

	SELECT @INT_CONTEXT = trnsctn_cntxt_cd
	FROM   ref_trnsctn_cntxt_cd
	WHERE  trnsctn_cntxt_val_txt = 'INTEREST_ACCRUAL'

	select @status = pymnt_btch_stts_cd from Ref_pymnt_btch_stts_cd where pymnt_btch_stts_val_txt = 'POSTED'
		
	SELECT @upsrt_soft_comp_id = upsrt_soft_comp_id
	FROM   lkup_soft_comp
	WHERE  upsrt_soft_comp_nm = 'admin'

	SELECT @upsrt_usr_id = usr_id
	FROM   usr
	WHERE  usr_usrnm = 'CNVRSN'

	SELECT @clerk_id = clrk_id
	FROM   clrk
	WHERE  usr_id = @upsrt_usr_id
	
	--11/17/2017 AS:  Default receiver for records with commission but no receiver
	DECLARE @rcvr_id AS INTEGER
	SELECT	@rcvr_id = rcvr_id
	FROM	rcvr
	WHERE	rcvr_shrt_nm = 'CONV'

	SET @StatusMsg = 'Completed variable declaration & setting.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg , 3, GETDATE())

	/* ********* */
	/*  Indices  */
	/* ********* */

	--cnsmr_id
	IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'mu_fnncl_trnsctn_CNSMR_ID')
		CREATE NONCLUSTERED INDEX mu_fnncl_trnsctn_CNSMR_ID ON ETL.mu_fnncl_trnsctn (cnsmr_id)
	ELSE
		ALTER INDEX mu_fnncl_trnsctn_CNSMR_ID ON ETL.mu_fnncl_trnsctn REBUILD WITH (ONLINE = OFF)

	SET @StatusMsg = 'Completed mu_fnncl_trnsctn_CNSMR_ID index.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	--RECEIVER_SHORT_NAME
	IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'mu_fnncl_trnsctn_RECEIVER')
		CREATE NONCLUSTERED INDEX mu_fnncl_trnsctn_RECEIVER ON ETL.mu_fnncl_trnsctn (RECEIVER_SHORT_NAME)
	ELSE
		ALTER INDEX mu_fnncl_trnsctn_RECEIVER ON ETL.mu_fnncl_trnsctn REBUILD WITH (ONLINE = OFF)

	SET @StatusMsg = 'Completed mu_fnncl_trnsctn_RECEIVER index.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())


	/* ******************************************* */
	/*  Lookup id values from various DM9 tables.  */
	/* ******************************************* */

	--Match to accounts and buckets
	UPDATE TX
	SET
	TX.cnsmr_id = ca.cnsmr_id,
	TX.cnsmr_accnt_id = ca.cnsmr_accnt_id,
	TX.crdtr_id = CA.crdtr_id,
	TX.bckt_id = B.bckt_id,
	TX.INVALID = 0 -- if we found it lets start with thinking it's ok
	FROM
	ETL.mu_fnncl_trnsctn TX
	join cnsmr_accnt CA on CA.cnsmr_accnt_idntfr_lgcy_txt = TX.CUSTOMER_ACCOUNT_ID
	join bckt B on B.bckt_shrt_nm = TX.FINANCIAL_BUCKET_NM

	SET @StatusMsg = 'Completed update for cnsmr_id & cnsmr_accnt_id, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

    --update transaction type code
    UPDATE TX
    SET    TX.bckt_trnsctn_typ_cd = ref.bckt_trnsctn_typ_cd
    FROM   ETL.mu_fnncl_trnsctn TX
           INNER JOIN Ref_bckt_trnsctn_typ_cd ref
             ON TX.PAYMENT_TYP_CD = ref.bckt_trnsctn_val_txt

	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', 'Completed update for bckt_trnsctn_typ_cd, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.', 3, GETDATE())

    --update location code
    UPDATE TX
    SET    TX.pymnt_lctn_cd = ref.pymnt_lctn_cd
    FROM   ETL.mu_fnncl_trnsctn TX
           INNER JOIN Ref_pymnt_lctn_cd ref
             ON TX.PAYMENT_LOC_CD = ref.pymnt_lctn_val_txt

	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', 'Completed update for pymnt_lctn_cd, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.', 3, GETDATE())

    --workgroup short name
    UPDATE TX
    SET    TX.wrkgrp_id = c.wrkgrp_id
    FROM   ETL.mu_fnncl_trnsctn TX
		   INNER JOIN cnsmr c
		     ON TX.cnsmr_id = c.cnsmr_id

	SET @StatusMsg = 'Completed update for wrkgrp_id, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	--pymnt_memo_id
	UPDATE TX
	SET    TX.pymnt_memo_id = pm.pymnt_memo_id
	FROM   ETL.mu_fnncl_trnsctn TX
	       INNER JOIN pymnt_memo pm
		     ON TX.MEMO_CD_SHRT_NM = pm.pymnt_memo_nm

	SET @StatusMsg = 'Completed update for pymnt_memo_id, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	--crrncy_cd
	UPDATE TX
	SET    TX.crrncy_cd = ref.crrncy_cd
	FROM   ETL.mu_fnncl_trnsctn TX
	       INNER JOIN ref_crrncy_cd ref
		     ON TX.CURRENCY = ref.crrncy_val_txt

	SET @StatusMsg = 'Completed update for crrncy_cd, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	--usr_id
	UPDATE TX
	SET    TX.usr_id = usr.usr_id
	FROM   ETL.mu_fnncl_trnsctn TX
	       INNER JOIN usr
		     ON TX.CREATED_BY = usr.usr_usrnm

	SET @StatusMsg = 'Completed update for usr_id, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	--rcvr_id
	UPDATE TX
	SET    TX.rcvr_id = r.rcvr_id
	FROM   ETL.mu_fnncl_trnsctn TX
	       INNER JOIN rcvr r
	       ON TX.RECEIVER_SHORT_NAME = r.rcvr_shrt_nm

	--New Logic: 11/17/2017 AS
	--First attempt to get the receiver id for the receiver short names provided in the staging table.
	--If we have the receiver defined, we want to use it.  For transacftions that have commission but we have not already found
	--the receiver, use the default specified above
	UPDATE ETL.mu_fnncl_trnsctn
	SET    rcvr_id = @rcvr_id
	WHERE  rcvr_id is null
	       AND RECEIVER_COMMISSION_AMOUNT > 0.00

	SET @StatusMsg = 'Completed update for rcvr_id, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	/* ********************** */
	/*  Update INVALID flag.  */
	/* ********************** */

	--If the account isn't found or the amounts are 0 it's not valid
	UPDATE ETL.mu_fnncl_trnsctn SET INVALID = 1 
	WHERE 
	bckt_id is null OR
	crdtr_id is null OR
	cnsmr_id is null OR
	cnsmr_accnt_id is null OR
	TRANSACTION_AMT = 0 or 
	TOT_PAYMENT_AMT = 0 or 
	TRANSACTION_AMT is null or
	TOT_PAYMENT_AMT is null or
	bckt_trnsctn_typ_cd is null

	SET @StatusMsg = 'Completed update for INVALID flag, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	/* ***************************************************************************************************** */
	/*  Verify all TOT_PAYMENT_AMTs match the summation of individual TRANSACTION_AMTs for each PAYMENT_ID.  */
	/* ***************************************************************************************************** */

	SELECT PAYMENT_ID,
	       SUM(TRANSACTION_AMT) TOT_PAYMENT_AMT
	INTO   #pid_amounts
	FROM   ETL.mu_fnncl_trnsctn
	GROUP  BY PAYMENT_ID

	SET @StatusMsg = 'Completed insert for #pid_amounts, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows inserted.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	CREATE NONCLUSTERED INDEX #pid_amounts_idx_01 ON #pid_amounts (PAYMENT_ID) INCLUDE (TOT_PAYMENT_AMT)

	UPDATE stg
	SET    stg.INVALID = 1
	FROM   #pid_amounts tmp
	       INNER JOIN ETL.mu_fnncl_trnsctn stg
		     ON tmp.PAYMENT_ID = stg.PAYMENT_ID
	WHERE  stg.TOT_PAYMENT_AMT <> tmp.TOT_PAYMENT_AMT

	SET @StatusMsg = 'Completed update for transaction amount mismatch, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	/* ********************************************************************************************************* */
	/*  If one transaction in a PAYMENT_ID is INVALID, mark all other transactions for that PAYMENT_ID INVALID.  */
	/* ********************************************************************************************************* */

	SELECT DISTINCT PAYMENT_ID
	INTO   #invalid_pid
	FROM   ETL.mu_fnncl_trnsctn
	WHERE  INVALID = 1

	SET @StatusMsg = 'Completed insert into #invalid_pid, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows inserted.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	CREATE NONCLUSTERED INDEX #invalid_pid_idx_01 ON #invalid_pid (PAYMENT_ID)

	UPDATE stg
	SET    stg.INVALID = 1
	FROM   #invalid_pid tmp
	       INNER JOIN ETL.mu_fnncl_trnsctn stg
		     ON tmp.PAYMENT_ID = stg.PAYMENT_ID
			    AND stg.INVALID = 0
	
	SET @StatusMsg = 'Completed update for marking all pieces INVALID, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	/* ************************************************************************* */
	/*  Populate our mu_fnncl_trnsctn_cnsmr_list temp table with all consumers.  */
	/* ************************************************************************* */

	INSERT INTO ETL.mu_fnncl_trnsctn_cnsmr_list
	(
		cnsmr_id
	)
	SELECT DISTINCT
	       cnsmr_id
	FROM   ETL.mu_fnncl_trnsctn
	WHERE  cnsmr_id IS NOT NULL

	SET @StatusMsg = 'Completed insert into ETL.mu_fnncl_trnsctn_cnsmr_list, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows inserted.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	CREATE NONCLUSTERED INDEX #mu_fnncl_trnsctn_cnsmr_list_idx_01 ON ETL.mu_fnncl_trnsctn_cnsmr_list (cnsmr_id) INCLUDE (id)

	SET @StatusMsg = 'Completed creation of index on ETL.mu_fnncl_trnsctn_cnsmr_list.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())
	
	/* ***************************************************************************************** */
	/*  Populate our mu_fnncl_trnsctn_payment_id_list temp table with all distinct payment IDs.  */
	/* ***************************************************************************************** */

	INSERT INTO ETL.mu_fnncl_trnsctn_payment_id_list
	(
		PAYMENT_ID
	)
	SELECT DISTINCT
	       PAYMENT_ID
	FROM   ETL.mu_fnncl_trnsctn
	WHERE  cnsmr_id IS NOT NULL
	       AND ISNULL(PAYMENT_ID, '') <> ''

	SET @StatusMsg = 'Completed insert into ETL.mu_fnncl_trnsctn_payment_id_list, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows inserted.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	CREATE NONCLUSTERED INDEX #mu_fnncl_trnsctn_payment_id_list_idx_01 ON ETL.mu_fnncl_trnsctn_payment_id_list (PAYMENT_ID) INCLUDE (id)

	SET @StatusMsg = 'Completed creation of index on ETL.mu_fnncl_trnsctn_payment_id_list.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())
	
	--Declare & initialize variables     
	DECLARE @LastCPJ BIGINT 
	DECLARE @LastCAPJ BIGINT 
	DECLARE @AdjustBatch BIGINT 

	DECLARE @LO_VAL BIGINT
	DECLARE @HI_VAL BIGINT
	DECLARE @MAX_ID BIGINT

	/* ************* */
	/*  CPB Section  */
	/* ************* */

	INSERT INTO [dbo].[cnsmr_pymnt_btch] 
				([cnsmr_pymnt_btch_assgnd_usrid], 
				 [cnsmr_pymnt_btch_file_registry_id], 
				 [cnsmr_pymnt_btch_crt_usrid], 
				 [cnsmr_pymnt_btch_typ_cd], 
				 [cnsmr_pymnt_btch_instrmnt_typ_cd], 
				 [cnsmr_pymnt_btch_crt_dttm], 
				 [cnsmr_pymnt_btch_elctrnc_dt], 
				 [cnsmr_pymnt_btch_extrnl_nm], 
				 [cnsmr_pymnt_btch_extrnl_btch_cd_txt], 
				 [cnsmr_pymnt_btch_extrnl_crdtr_cd_txt], 
				 [cnsmr_pymnt_btch_fnncl_instrmnt_cnt], 
				 [cnsmr_pymnt_btch_fnncl_instrmnt_val_amnt], 
				 [cnsmr_pymnt_btch_msg_txt], 
				 [cnsmr_pymnt_btch_nm], 
				 [cnsmr_pymnt_btch_prcssd_dttm], 
				 [cnsmr_pymnt_btch_pymnt_cnt_nmbr], 
				 [cnsmr_pymnt_btch_pymnt_ttl_amnt], 
				 [cnsmr_pymnt_btch_rlsd_usrid], 
				 [cnsmr_pymnt_btch_rlsd_dttm], 
				 [cnsmr_pymnt_btch_stts_cd], 
				 [cnsmr_pymnt_epp_athrz_flg], 
				 [cnsmr_pymnt_btch_lck_flg], 
				 [cnsmr_pymnt_btch_imprtd_rec_cnt], 
				 [cnsmr_pymnt_btch_pstng_retry_nmbr], 
				 [cnsmr_pymnt_btch_auto_post_flg], 
				 [upsrt_dttm], 
				 [upsrt_soft_comp_id], 
				 [upsrt_trnsctn_nmbr], 
				 [upsrt_usr_id], 
				 [cnsmr_archvd_flg], 
				 [cnsmr_pymnt_btch_orgnl_pymnt_cnt_nmbr], 
				 [cnsmr_pymnt_btch_orgnl_pymnt_ttl_amnt]) 
	VALUES      (@clerk_id, 
				 NULL, 
				 @clerk_id, 
				 3, 
				 NULL, 
				 Getdate(), 
				 NULL, 
				 NULL, 
				 NULL, 
				 NULL, 
				 NULL, 
				 NULL, 
				 NULL, 
				 'Pmnt' 
				 + Replace(Replace(Replace(Replace(CONVERT(VARCHAR(30), Getdate(), 
				 126), '-', '') 
				 , 'T', ''), ':', ''), '.', ''), 
				 NULL, 
				 NULL, 
				 NULL, 
				 NULL, 
				 NULL, 
				 1, 
				 'N', 
				 'N', 
				 NULL, 
				 0, 
				 'N', 
				 Getdate(), 
				 19, 
				 0, 
				 @upsrt_usr_id, 
				 'N', 
				 NULL, 
				 NULL)

	SET @StatusMsg = 'Completed insert into cpb, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows inserted.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	SELECT @LastCPJ = isnull(Max(cnsmr_pymnt_jrnl_id) ,0)
	FROM   cnsmr_pymnt_jrnl 

	SELECT @LastCAPJ = isnull(Max(cnsmr_accnt_pymnt_jrnl_id) ,0)
	FROM   cnsmr_accnt_pymnt_jrnl 

	SELECT @AdjustBatch = isnull(Max(cnsmr_pymnt_btch_id) ,0)
	FROM   cnsmr_pymnt_btch
	
	/* ************* */
	/*  CPI Section  */
	/* ************* */

    --ChrisH: 6/17/21 - bug fix, CPI needs to iterate through the payment ID list table, NOT the cnsmr list
	SELECT @MAX_ID = MAX(id) FROM ETL.mu_fnncl_trnsctn_payment_id_list
	SELECT @LO_VAL = MIN(id) FROM ETL.mu_fnncl_trnsctn_payment_id_list
	SET    @HI_VAL = @LO_VAL + @CPI_CHUNK
	
	WHILE (@LO_VAL <= @MAX_ID)
	BEGIN
	
		--This is new for DM11. In order to reverse a payment, it MUST have a CPI entry. So insert a stub with the method for each payment in staging.
		INSERT INTO cnsmr_pymnt_instrmnt
		(
			cnsmr_id,
			cnsmr_pymnt_instrmnt_typ_cd,
			upsrt_dttm,
			upsrt_soft_comp_id,
			upsrt_trnsctn_nmbr,
			upsrt_usr_id,
			bdl_stgng_id
		)
		SELECT DISTINCT stg.cnsmr_id,
			   pm.pymnt_instrmnt_typ_cd,
			   @curdate,
			   99,
			   0,
			   @upsrt_usr_id,
			   tmp.id
		FROM   ETL.mu_fnncl_trnsctn_payment_id_list tmp
			   INNER JOIN ETL.mu_fnncl_trnsctn stg
				 ON tmp.PAYMENT_ID = stg.PAYMENT_ID
					AND stg.pymnt_memo_id IS NOT NULL
			   INNER JOIN pymnt_memo pm
				 ON stg.pymnt_memo_id = pm.pymnt_memo_id
		WHERE  tmp.id >= @LO_VAL
		       AND tmp.id < @HI_VAL

		SET @StatusMsg = 'Completed insert into cpi for chunk ' + CAST(@LO_VAL AS VARCHAR(32)) + ' to ' + CAST(@HI_VAL AS VARCHAR(32)) + ', ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows inserted.'
		INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
		VALUES ('FIN', @StatusMsg, 3, GETDATE())

		SET @LO_VAL = @LO_VAL + @CPI_CHUNK
		SET @HI_VAL = @HI_VAL + @CPI_CHUNK

	END

	/* ************* */
	/*  CPJ Section  */
	/* ************* */

	SELECT @MAX_ID = MAX(id) FROM ETL.mu_fnncl_trnsctn_cnsmr_list
	SELECT @LO_VAL = MIN(id) FROM ETL.mu_fnncl_trnsctn_cnsmr_list
	SET    @HI_VAL = @LO_VAL + @CHUNK_SIZE
	
	WHILE (@LO_VAL <= @MAX_ID)
	BEGIN

		INSERT INTO cnsmr_pymnt_jrnl 
		(
			bckt_trnsctn_typ_cd, 
			cnsmr_id,
			cnsmr_pymnt_instrmnt_id,
			cnsmr_pymnt_btch_id, 
			cnsmr_pymnt_amnt, 
			cnsmr_pymnt_cmmnt_txt, 
			cnsmr_pymnt_entrd_dt, 
			cnsmr_pymnt_is_cnvrsn_flg, 
			cnsmr_pymnt_is_rglr_flg, 
			cnsmr_pymnt_lctn_cd, 
			cnsmr_pymnt_stts_cd, 
			cnsmr_pymnt_tndrd_dt, 
			cnsmr_pymnt_rfnd_flg, 
			cnsmr_pymnt_is_nsf_flg, 
			cnsmr_pymnt_memo_cd_txt,
			pymnt_memo_id,
			crrncy_cd,
			cnsmr_pymnt_extrnl_ref_idntfr_txt,
			cnsmr_pymnt_crt_usrid,
			pymnt_orgntd_frm_sspns_flg, 
			sspns_pymnt_rvrs_on_pymnt_rvrsl_flg, 
			upsrt_dttm, 
			upsrt_soft_comp_id, 
			upsrt_trnsctn_nmbr, 
			upsrt_usr_id, 
			crtd_dttm, 
			crtd_usr_id
		) 
		SELECT DISTINCT 
			   ref.bckt_trnsctn_typ_cd,
			   stg.cnsmr_id,
			   cpi.cnsmr_pymnt_instrmnt_id,
			   @AdjustBatch, 
			   CASE WHEN PAYMENT_TYP_CD = 'NSF' THEN (TOT_PAYMENT_AMT * -1) ELSE TOT_PAYMENT_AMT END, 
			   COMMENT, 
			   Max(PAYMENT_ENTERED_DATE), 
			   'N', 
			   'Y', 
			   plc.pymnt_lctn_cd, 
			   5, 
			   max(PAYMENT_ENTERED_DATE), 
			   'N', 
			   'N', 
			   MEMO_CD_SHRT_NM,
			   pymnt_memo_id,
			   crrncy_cd,
			   stg.PAYMENT_ID,
			   ISNULL(usr_id, @upsrt_usr_id),
			   'N', 
			   'N', 
			   Getdate(), 
			   99, 
			   1, 
			   @upsrt_usr_id, 
			   Getdate(), 
			   @upsrt_usr_id
		 FROM  ETL.mu_fnncl_trnsctn_cnsmr_list tmp
		       INNER JOIN ETL.mu_fnncl_trnsctn stg
			     ON tmp.cnsmr_id = stg.cnsmr_id
				    AND stg.INVALID = 0
				    AND stg.cnsmr_accnt_id IS NOT NULL
				    AND stg.PAYMENT_TYP_CD NOT IN ('CHARGE') --We don't put CHARGE type into cpj
			   INNER JOIN Ref_bckt_trnsctn_typ_cd ref
                 ON stg.PAYMENT_TYP_CD = ref.bckt_trnsctn_val_txt
               INNER JOIN Ref_pymnt_lctn_cd plc
                 ON stg.PAYMENT_LOC_CD = plc.pymnt_lctn_val_txt
		       INNER JOIN ETL.mu_fnncl_trnsctn_payment_id_list pid
			     ON stg.PAYMENT_ID = pid.PAYMENT_ID
			   INNER JOIN cnsmr_pymnt_instrmnt cpi
			     ON pid.id = cpi.bdl_stgng_id
				    AND stg.cnsmr_id = cpi.cnsmr_id
					AND cpi.upsrt_soft_comp_id = 99
					AND cpi.upsrt_dttm = @curdate
		 WHERE tmp.id >= @LO_VAL
		       AND tmp.id < @HI_VAL
		 GROUP BY stg.PAYMENT_ID, stg.cnsmr_id, TOT_PAYMENT_AMT,
		          ref.bckt_trnsctn_typ_cd, COMMENT, plc.pymnt_lctn_cd,
				  PAYMENT_TYP_CD, MEMO_CD_SHRT_NM, pymnt_memo_id, crrncy_cd, usr_id, cpi.cnsmr_pymnt_instrmnt_id

		SET @StatusMsg = 'Completed insert into cpj for chunk ' + CAST(@LO_VAL AS VARCHAR(32)) + ' to ' + CAST(@HI_VAL AS VARCHAR(32)) + ', ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows inserted.'
		INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
		VALUES ('FIN', @StatusMsg, 3, GETDATE())

		SET @LO_VAL = @LO_VAL + @CHUNK_SIZE
		SET @HI_VAL = @HI_VAL + @CHUNK_SIZE

	END
	 
	 /*  ADD CONSUMER PAY ID TO STAGING TABLE  */ 
	UPDATE FTD 
	SET    FTD.cnsmr_pymnt_jrnl_id = cpj.cnsmr_pymnt_jrnl_id 
	FROM   ETL.mu_fnncl_trnsctn FTD
		   INNER JOIN cnsmr_pymnt_jrnl cpj 
				   ON cpj.cnsmr_id = FTD.cnsmr_id 
					  AND cpj.cnsmr_pymnt_jrnl_id > @LastCPJ 
					  AND cpj.cnsmr_pymnt_extrnl_ref_idntfr_txt = FTD.PAYMENT_ID

	SET @StatusMsg = 'Completed update of cnsmr_pymnt_jrnl_id on stg, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	/* ************** */
	/*  CAPJ Section  */
	/* ************** */

    --ChrisH: 6/17 - MAX ID was not being reset, so the CAPJ portion was looping too much. This didn't case any issue, just inefficient looping.
    SELECT @MAX_ID = MAX(id) FROM ETL.mu_fnncl_trnsctn_cnsmr_list
	SELECT @LO_VAL = MIN(id) FROM ETL.mu_fnncl_trnsctn_cnsmr_list
	SET    @HI_VAL = @LO_VAL + @CHUNK_SIZE
	
	WHILE (@LO_VAL <= @MAX_ID)
	BEGIN

		INSERT INTO cnsmr_accnt_pymnt_jrnl 
		(
			bckt_trnsctn_typ_cd, 
			cnsmr_accnt_id, 
			cnsmr_pymnt_jrnl_id, 
			crdtr_id, 
			wrkgrp_id, 
			cnsmr_accnt_pymnt_amnt, 
			cnsmr_accnt_pymnt_cmmnt_txt, 
			cnsmr_accnt_pymnt_cmssn_amnt, 
			cnsmr_accnt_pymnt_ovvrd_allctn_flg, 
			cnsmr_accnt_pymnt_is_nsf_flg, 
			cnsmr_accnt_pymnt_stts_cd, 
			cnsmr_accnt_pymnt_pstd_dt, 
			pymnt_wash_flg, 
			rcvr_cmssn_amnt, 
			cnsmr_pymnt_entrd_dt, 
			cnsmr_accnt_pymnt_systm_dstrbtn_flg, 
			upsrt_dttm, 
			upsrt_soft_comp_id, 
			upsrt_trnsctn_nmbr, 
			upsrt_usr_id, 
			crtd_dttm, 
			crtd_usr_id
		)
		SELECT DISTINCT 
		       ref.bckt_trnsctn_typ_cd, 
			   FTD.cnsmr_accnt_id, 
			   FTD.cnsmr_pymnt_jrnl_id, 
			   MAX(FTD.crdtr_id), 
			   MAX(FTD.wrkgrp_id), 
			   CASE WHEN PAYMENT_TYP_CD = 'NSF' THEN (TOT_PAYMENT_AMT * -1) ELSE TOT_PAYMENT_AMT END, 
			   FTD.COMMENT, 
			   ISNULL(FTD.COMMISSION_AMOUNT, 0.00), 
			   'N', 
			   'N', 
			   5, 
			   MAX(FTD.PAYMENT_POSTED_DATE),--Getdate(), 
			   'N', 
			   0, 
			   MAX(FTD.PAYMENT_ENTERED_DATE),--GetDate(), 
			   'N', 
			   Getdate(), 
			   99, 
			   1, 
			   @upsrt_usr_id, 
			   Getdate(), 
			   @upsrt_usr_id
		 FROM  ETL.mu_fnncl_trnsctn_cnsmr_list tmp
		       INNER JOIN ETL.mu_fnncl_trnsctn FTD
			     ON tmp.cnsmr_id = FTD.cnsmr_id
				    AND FTD.INVALID = 0
					AND FTD.cnsmr_accnt_id IS NOT NULL
					AND FTD.cnsmr_pymnt_jrnl_id IS NOT NULL
					AND FTD.PAYMENT_TYP_CD NOT IN ('CHARGE') --We don't put CHARGE type into capj
			  INNER JOIN Ref_bckt_trnsctn_typ_cd ref
                ON FTD.PAYMENT_TYP_CD = ref.bckt_trnsctn_val_txt
		 WHERE tmp.id >= @LO_VAL
		       AND tmp.id < @HI_VAL
		 GROUP BY PAYMENT_ID, cnsmr_accnt_id, cnsmr_pymnt_jrnl_id,
		          TOT_PAYMENT_AMT, ref.bckt_trnsctn_typ_cd, COMMENT,
				  PAYMENT_TYP_CD, COMMISSION_AMOUNT

		SET @StatusMsg = 'Completed insert into capj for chunk ' + CAST(@LO_VAL AS VARCHAR(32)) + ' to ' + CAST(@HI_VAL AS VARCHAR(32)) + ', ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows inserted.'
		INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
		VALUES ('FIN', @StatusMsg, 3, GETDATE())

		SET @LO_VAL = @LO_VAL + @CHUNK_SIZE
		SET @HI_VAL = @HI_VAL + @CHUNK_SIZE

	END
	 
	 /*  ADD CONSUMER ACCOUNT PAY ID TO STAGING TABLE  */ 
	UPDATE FTD 
	SET    FTD.cnsmr_accnt_pymnt_jrnl_id = capj.cnsmr_accnt_pymnt_jrnl_id 
	FROM   ETL.mu_fnncl_trnsctn FTD 
		   INNER JOIN cnsmr_accnt_pymnt_jrnl capj 
				   ON capj.cnsmr_accnt_id = FTD.cnsmr_accnt_id 
					  and CAPJ.cnsmr_pymnt_jrnl_id = ftd.cnsmr_pymnt_jrnl_id
					  AND capj.cnsmr_accnt_pymnt_jrnl_id > @LastCAPJ

	SET @StatusMsg = 'Completed update of cnsmr_accnt_pymnt_jrnl_id on stg, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	/* ************* */
	/*  CAT Section  */
	/* ************* */

	SELECT @MAX_ID = MAX(id) FROM ETL.mu_fnncl_trnsctn
	SELECT @LO_VAL = MIN(id) FROM ETL.mu_fnncl_trnsctn
	SET    @HI_VAL = @LO_VAL + @CAT_CHUNK
	
	WHILE (@LO_VAL <= @MAX_ID)
	BEGIN
	
		INSERT INTO cnsmr_accnt_trnsctn 
		(
			bckt_id, 
			bckt_trnsctn_typ_cd, 
			cnsmr_accnt_id, 
			cnsmr_accnt_pymnt_jrnl_id, 
			cnsmr_accnt_trnsctn_stts_cd, 
			wrkgrp_id, 
			cnsmr_accnt_trnsctn_amnt, 
			cnsmr_accnt_trnsctn_cmssn_isdflt_flg, 
			cnsmr_accnt_trnsctn_cmssn_isfxd_flg,
			cnsmr_accnt_trnsctn_cmssn_amnt,
			cnsmr_accnt_trnsctn_cmssn_prcntg,
			cnsmr_accnt_trnsctn_pst_dt, 
			cnsmr_accnt_trnsctn_rcvr_cmssn_prcntg, 
			cnsmr_accnt_trnsctn_rcvr_cmssn_amnt, 
			cnsmr_accnt_trnsctn_rcvr_cmssn_isfxd_flg, 
			cnsmr_accnt_trnsctn_rcvr_cmssn_isdflt_flg, 
			cnsmr_accnt_trnsctn_lctn_cd, 
			cnsmr_accnt_trnsctn_invcd_flg, 
			cnsmr_accnt_trsnctn_entrd_dttm, 
			cnsmr_accnt_lst_dly_intrst_accmltn_trnsctn_flg, 
			crdtr_id,
			rcvr_id,
			crrncy_cd,
			upsrt_dttm, 
			upsrt_soft_comp_id, 
			upsrt_trnsctn_nmbr, 
			upsrt_usr_id, 
			crtd_dttm, 
			crtd_usr_id, 
			cnsmr_accnt_trnsctn_is_systm_adjstd_flg, 
			cnsmr_accnt_trnsctn_rcvr_is_systm_adjstd_flg,
			cnsmr_accnt_trnsctn_cntxt_cd
		)
		SELECT DISTINCT
		       FTD.bckt_id, 
			   ref.bckt_trnsctn_typ_cd, 
			   cnsmr_accnt_id, 
			   cnsmr_accnt_pymnt_jrnl_id, 
			   1, 
			   wrkgrp_id, 
			   CASE WHEN PAYMENT_TYP_CD = 'NSF' 
			        THEN TRANSACTION_AMT 
					ELSE (TRANSACTION_AMT * -1)
			   END,
			   'N',
			   'N',
			   ISNULL(FTD.COMMISSION_AMOUNT, 0.00),
			   ISNULL(FTD.COMMISSION_PERCENT, 0.00),
			   FTD.PAYMENT_POSTED_DATE,--Getdate(),
			   ISNULL(FTD.RECEIVER_COMMISSION_PERCENT, 0.00),
			   ISNULL(FTD.RECEIVER_COMMISSION_AMOUNT, 0.00),
			   'N',
			   'N',
			   plc.pymnt_lctn_cd,
			   'N',
			   FTD.PAYMENT_ENTERED_DATE,--Getdate(),
			   'N',
			   FTD.crdtr_id,
			   FTD.rcvr_id,
			   FTD.crrncy_cd,
			   Getdate(), 
			   99, 
			   1, 
			   @upsrt_usr_id, 
			   Getdate(), 
			   @upsrt_usr_id, 
			   'N', 
			   'N',
			   CASE WHEN PAYMENT_TYP_CD IN ('CHARGE')
			        THEN @INT_CONTEXT
					ELSE NULL
			   END
		 FROM  ETL.mu_fnncl_trnsctn FTD
		       INNER JOIN Ref_bckt_trnsctn_typ_cd ref
                 ON FTD.PAYMENT_TYP_CD = ref.bckt_trnsctn_val_txt
               INNER JOIN Ref_pymnt_lctn_cd plc
                 ON FTD.PAYMENT_LOC_CD = plc.pymnt_lctn_val_txt
		 WHERE cnsmr_accnt_id IS NOT NULL
			   AND INVALID = 0
			   AND FTD.id >= @LO_VAL
			   AND FTD.id < @HI_VAL

		SET @StatusMsg = 'Completed insert into cat for chunk ' + CAST(@LO_VAL AS VARCHAR(32)) + ' to ' + CAST(@HI_VAL AS VARCHAR(32)) + ', ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows inserted.'
		INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
		VALUES ('FIN', @StatusMsg, 3, GETDATE())

		SET @LO_VAL = @LO_VAL + @CAT_CHUNK
		SET @HI_VAL = @HI_VAL + @CAT_CHUNK

	END

	UPDATE cnsmr_accnt_wrk_actn 
	SET    cnsmr_accnt_bal_config_chng_sync_flg = 'Y' 
	FROM   cnsmr_accnt_wrk_actn caw 
		   INNER JOIN ETL.mu_fnncl_trnsctn FTD 
				   ON FTD.cnsmr_accnt_id = caw.cnsmr_accnt_id
	WHERE FTD.INVALID = 0

	SET @StatusMsg = 'Completed update of sync flag, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	update [cnsmr_pymnt_btch]
	set [cnsmr_pymnt_btch_stts_cd] =@status, cnsmr_pymnt_btch_prcssd_dttm = GETDATE() --ChrisH: 6/28 - added processed dttm
	where cnsmr_pymnt_btch_id = @AdjustBatch

	SET @StatusMsg = 'Completed update of cpb status, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

--This script from FICO PTO updates cnsmr_accnt_lst_pymnt_dttm and amt
-- with the most recent cnsmr_pymnt_jrnl entry
--Script added by David Taylor,FICO - 7/23/14

/* Commenting out 6/24/24 - BAC updates Last Pay Info from TSYS as part of change capture process.			
	DECLARE @last_payment TABLE (
  cnsmr_accnt_id            BIGINT NOT NULL,
  cnsmr_accnt_pymnt_jrnl_id BIGINT NOT NULL,
  cnsmr_pymnt_entrd_dt      DATETIME,
  cnsmr_accnt_pymnt_amnt    DECIMAL (11, 3),
  PRIMARY KEY CLUSTERED (cnsmr_accnt_id))


	INSERT INTO @last_payment
	(
		cnsmr_accnt_id,
		cnsmr_accnt_pymnt_jrnl_id,
		cnsmr_pymnt_entrd_dt,
		cnsmr_accnt_pymnt_amnt
	)
	SELECT cnsmr_accnt_id,
		   cnsmr_accnt_pymnt_jrnl_id,
		   cnsmr_pymnt_entrd_dt,
		   cnsmr_accnt_pymnt_amnt
	FROM  (SELECT capj.cnsmr_accnt_id,
				  capj.cnsmr_accnt_pymnt_jrnl_id,
				  cpj.cnsmr_pymnt_entrd_dt,
				  capj.cnsmr_accnt_pymnt_amnt,
				  ROW_NUMBER() OVER(PARTITION BY capj.cnsmr_accnt_id ORDER BY
						cpj.cnsmr_pymnt_entrd_dt
				  DESC, capj.cnsmr_accnt_pymnt_jrnl_id DESC) AS rank
		   FROM   cnsmr_pymnt_jrnl cpj
				  INNER JOIN cnsmr_accnt_pymnt_jrnl capj
					ON cpj.cnsmr_pymnt_jrnl_id = capj.cnsmr_pymnt_jrnl_id
				  INNER JOIN ETL.mu_fnncl_trnsctn txns
					on txns.cnsmr_pymnt_jrnl_id=cpj.cnsmr_pymnt_jrnl_id and txns.INVALID=0
				  INNER JOIN ref_bckt_trnsctn_typ_cd tr_typ
					ON cpj.bckt_trnsctn_typ_cd = tr_typ.bckt_trnsctn_typ_cd
					   AND tr_typ.bckt_trnsctn_val_txt = 'PAYMENT') t1
	WHERE  rank = 1

	SET @StatusMsg = 'Completed insert into @last_payment, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows inserted.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

	UPDATE cawa
	SET    upsrt_dttm = Getdate(),
		   cnsmr_accnt_lst_pymnt_amnt = lp.cnsmr_accnt_pymnt_amnt,
		   cnsmr_accnt_lst_pymnt_dttm = lp.cnsmr_pymnt_entrd_dt
	FROM   cnsmr_accnt_wrk_actn cawa
		   INNER JOIN @last_payment lp
			 ON cawa.cnsmr_accnt_id = lp.cnsmr_accnt_id

	SET @StatusMsg = 'Completed update of cawa last pay, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())
*/
	/* ***************************************************************** */
	/*  ChrisH - 8/5/20 - Let's get cwa updated with last pay info too.  */
	/* ***************************************************************** */

	DECLARE @cnsmr_last_payment TABLE
	(
		cnsmr_id             BIGINT NOT NULL,
		cnsmr_pymnt_entrd_dt DATETIME,
		cnsmr_pymnt_amnt     DECIMAL (11, 3),
		PRIMARY KEY CLUSTERED (cnsmr_id)
	)

	--Find most recent payment at the consumer level, order by entered date desc.
	INSERT INTO @cnsmr_last_payment
	(
		cnsmr_id,
		cnsmr_pymnt_entrd_dt,
		cnsmr_pymnt_amnt
	)
	SELECT cnsmr_id,
		   cnsmr_pymnt_entrd_dt,
		   cnsmr_pymnt_amnt
	FROM  (SELECT cpj.cnsmr_id,
				  cpj.cnsmr_pymnt_entrd_dt,
				  cpj.cnsmr_pymnt_amnt,
				  ROW_NUMBER() OVER(PARTITION BY cpj.cnsmr_id
				  ORDER BY cpj.cnsmr_pymnt_entrd_dt DESC, cpj.cnsmr_pymnt_jrnl_id DESC) AS rank
		   FROM   cnsmr_pymnt_jrnl cpj
				  INNER JOIN ETL.mu_fnncl_trnsctn txns
					on txns.cnsmr_pymnt_jrnl_id = cpj.cnsmr_pymnt_jrnl_id
					   and txns.INVALID=0
				  INNER JOIN ref_bckt_trnsctn_typ_cd tr_typ
					ON cpj.bckt_trnsctn_typ_cd = tr_typ.bckt_trnsctn_typ_cd
					   AND tr_typ.bckt_trnsctn_val_txt = 'PAYMENT') t1
	WHERE  rank = 1

	--Update cwa with the info
	UPDATE cwa
	SET    cnsmr_lst_pymnt_amnt = lp.cnsmr_pymnt_amnt,
		   cnsmr_lst_pymnt_dttm = lp.cnsmr_pymnt_entrd_dt,
		   upsrt_dttm = GETDATE()
	FROM   cnsmr_wrk_actn cwa
		   INNER JOIN @cnsmr_last_payment lp
			 ON cwa.cnsmr_id = lp.cnsmr_id
             
    SET @StatusMsg = 'Completed update of cwa last pay, ' + CONVERT(NVARCHAR, @@ROWCOUNT) + ' rows updated.'
	INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
	VALUES ('FIN', @StatusMsg, 3, GETDATE())

    set @run_time = convert(nvarchar, dateadd(ms, datediff(second, @start_time, dbo.getcurrdt()) * 1000, 0), 114);

    INSERT INTO ETL.mu_fnncl_trnsctn_log (MessageType, LogMessage, LoggingLevel, LogTime)
    VALUES ('Fin Trnx - RUN TIME', 'Total Run Time: ' + @run_time, 3, GETDATE());
END

GO
