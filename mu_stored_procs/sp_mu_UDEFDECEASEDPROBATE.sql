use crs5_oltp;
go

-- drop existing staging table if present

if object_id('ETL.mu_UDEFDECEASEDPROBATE', 'U') is not null
    drop table etl.mu_UDEFDECEASEDPROBATE;

-- mu_UDEFDECEASEDPROBATE staging table
CREATE TABLE [ETL].[mu_UDEFDECEASEDPROBATE](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[agncy_entty_crrltn_id] [varchar](30) NULL,
  [cnsmr_idntfr_drvr_lcns_txt] [varchar](20) NULL,
	[UDEFESTT_NM] [varchar](120) NULL,
	[UDEFAMND_APPRVD_DT] [datetime] NULL,
	[UDEFWITHDRAWAL_DATE] [datetime] NULL,
	[UDEFBARDATE] [datetime] NULL,
	[UDEFCRT_ADDR_CITY_TXT] [varchar](64) NULL,
	[UDEFCRT_CLM_NM] [varchar](80) NULL,
	[UDEFCRT_ADDR_CNTRY_TXT] [varchar](128) NULL,
	[UDEFCRT_ADDR_CNTY_TXT] [varchar](128) NULL,
	[UDEFIS_NOTERIZED] [char](1) NULL,
	[UDEFURL] [varchar](1024) NULL,
	[UDEFFILE_DATE] [datetime] NULL,
	[UDEFPHN] [varchar](10) NULL,
	[UDEFCLAIM_NO] [varchar](50) NULL,
	[UDEFDEATH_DATE] [datetime] NULL,
	[UDEFCRT_ADDR_PSTL_CD] [varchar](32) NULL,
	[UDEFSATISFACTION_DATE] [datetime] NULL,
	[UDEFPHN_EXT] [varchar](5) NULL,
	[UDEFAMND_RQSTED_DT] [datetime] NULL,
	[UDEFREJECTED] [varchar](80) NULL,
	[UDEFCRT_ADDR_ADDRSS_LN_1_TXT] [varchar](128) NULL,
	[UDEFEML] [varchar](1024) NULL,
	[UDEFCRT_ADDR_ST_TXT] [varchar](20) NULL,
	[UDEFSND_TO_REP] [char](1) NULL,
	[UDEFIS_AMNDMNT] [char](1) NULL,
	[UDEFCRT_ADDR_ADDRSS_LN_2_TXT] [varchar](128) NULL,
	[UDEFCASE_NO] [varchar](50) NULL,
	[UDEFVNDR_CLM_NO] [varchar](50) NULL,
	--[UDEFDEATH_DATE_NON_CNFRM] [datetime] NULL,			8/22/2023 Field Removed from UDP
	[UDEFSND_TO_ATTY] [char](1) NULL,
	[UDEFSND_TO_CRT] [char](1) NULL,
	[UDEFCRT_ADDR_ADDRSS_LN_3_TXT] [varchar](128) NULL,
	[UDEFCLAIM_BAL] [decimal](12, 3) NULL,
	[UDEFFILED_BY] [varchar](80) NULL,
	[UDEFFAX] [varchar](10) NULL,
	[UDEFDOD_STATUS] [varchar](128) NULL
) ON [PRIMARY]

-- create the processing stored procedure

if object_id('ETL.sp_mu_UDEFDECEASEDPROBATE', 'P') is not null
    drop procedure etl.sp_mu_UDEFDECEASEDPROBATE;
go

/* ****************************************************************************************************************** */
/*   Name: Direct Load DECEASEDPROBATE UDP                                                                            */
/*   Staging Table: ETL.mu_UDEFDECEASEDPROBATE                                                                        */
/*   Processing Stored Procedure: ETL.sp_mu_UDEFDECEASEDPROBATE                                                       */
/*   Date:  11/16/2021                                                                                                */
/*   Purpose:  Custom DL script for loading data into DECEASEDPROBATE UDP                                             */
/*   Change Log:                                                                                                      */
/*       Version     Description                                            Developer               Date              */
/*       ------------------------------------------------------------------------------------------------------------ */
/*       11.1        Initial - for BAC TSYS migration                      Chris Hausman           11/16/2021         */
/*       11.1        Non-Confirmed DOD field removed, DOD Status added     Andrew Sheldon           08/22/2023        */
/*       ------------------------------------------------------------------------------------------------------------ */
/*  TO DO LIST                                                                                                        */
/*  ----------                                                                                                        */
/*   (1) What values will come for country?                                                                           */
/* ****************************************************************************************************************** */

create procedure etl.sp_mu_UDEFDECEASEDPROBATE @chunk_size bigint = 10000
as
    begin
        declare @err_msg nvarchar(4000)= N'';
        declare @err_severity int;
        declare @err_num int;

        declare @msg_tbl_nm nvarchar(50)= N'UDEFDECEASEDPROBATE';

        declare @start_time datetime;
        declare @run_time varchar(20);
        declare @loop_count int;
        declare @total_count bigint;
        declare @total_loops int;
        declare @start_key bigint;

        declare @default_upsrt_soft_comp_id bigint;
        declare @default_upsrt_usr_id bigint;
		
        declare @current_datetime as datetime = dbo.GETDATE();
		
    		declare @udef_adt_id as bigint;
    		declare @dod_status_adc_id as bigint;
		
        /**************************************************************************************
            Create permanent logging table.  
        **************************************************************************************/

        --This table will keep track of ETL progress.
        if not exists (select 1 from information_schema.tables where table_name = 'mu_Processing_Log')
        begin
            create table etl.mu_processing_log
            (
				pklog        numeric(18, 0) identity(1, 1) not null
			  , messagetype  nvarchar(50) not null
              , logmessage   nvarchar(1000) null
              , severity     int null
              , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
              , logtime      datetime not null default getdate()
            );
        end;

        /************************************************************************************
            Create local logging table.
        ************************************************************************************/

        declare @logsystem table
        (
			pklog        numeric(18, 0) not null identity(1, 1)
		  , messagetype  nvarchar(50) not null
		  , logmessage   nvarchar(1000) not null
		  , severity     int null
		  , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
		  , logtime      datetime not null default getdate()
        );
		
        begin try
            select @start_time = @current_datetime;

            -- set some default values
            select @default_upsrt_soft_comp_id = upsrt_soft_comp_id
            from dbo.lkup_soft_comp
            where upsrt_soft_comp_nm = 'load';

            select @default_upsrt_usr_id = usr_id
            from dbo.usr
            where usr_usrnm = 'CNVRSN';
			
      			--set the variables for the list of value fields
      			select @udef_adt_id = agncy_dfnd_tbl_id
      			from   agncy_Dfnd_Tbl
      			where  agncy_dfnd_tbl_nm = 'UDEFDECEASEDPROBATE'
      			
      			select @dod_status_adc_id = agncy_dfnd_clmn_id
      			from   agncy_Dfnd_Clmn
      			where  agncy_dfnd_clmn_nm = 'UDEFDOD_STATUS'
      			       and agncy_dfnd_tbl_id = @udef_adt_id			

            -- setup the looping variables to process in chucks
            set @loop_count = 1;

            select @total_count = count(*)
            from etl.mu_UDEFDECEASEDPROBATE;

            select @start_key = min(id)
            from etl.mu_UDEFDECEASEDPROBATE;

            set @total_loops = @total_count / @chunk_size + 1;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - START',
				'Starting ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

            while @loop_count > 0
            begin
				
				INSERT INTO [dbo].[UDEFDECEASEDPROBATE]
				(
					[cnsmr_id],
					[UDEFESTT_NM],
					[UDEFAMND_APPRVD_DT],
					[UDEFWITHDRAWAL_DATE],
					[UDEFBARDATE],
					[UDEFCRT_ADDR_CITY_TXT],
					[UDEFCRT_CLM_NM],
					[UDEFCRT_ADDR_CNTRY_TXT],
					[UDEFCRT_ADDR_CNTY_TXT],
					[UDEFIS_NOTERIZED],
					[UDEFURL],
					[UDEFFILE_DATE],
					[UDEFPHN],
					[UDEFCLAIM_NO],
					[UDEFDEATH_DATE],
					[UDEFCRT_ADDR_PSTL_CD],
					[UDEFSATISFACTION_DATE],
					[UDEFPHN_EXT],
					[UDEFAMND_RQSTED_DT],
					[UDEFREJECTED],
					[UDEFCRT_ADDR_ADDRSS_LN_1_TXT],
					[UDEFEML],
					[UDEFCRT_ADDR_ST_TXT],
					[UDEFSND_TO_REP],
					[UDEFIS_AMNDMNT],
					[UDEFCRT_ADDR_ADDRSS_LN_2_TXT],
					[UDEFCASE_NO],
					[UDEFVNDR_CLM_NO],
					--[UDEFDEATH_DATE_NON_CNFRM],  8/22/2023 Field removed from UDP
					[UDEFSND_TO_ATTY],
					[UDEFSND_TO_CRT],
					[UDEFCRT_ADDR_ADDRSS_LN_3_TXT],
					[UDEFCLAIM_BAL],
					[UDEFFILED_BY],
					[UDEFFAX],
					[upsrt_dttm],
					[upsrt_soft_comp_id],
					[upsrt_trnsctn_nmbr],
					[upsrt_usr_id],
					[UDEFDOD_STATUS]			--8/22/2023 New field added to UDP
				)
				SELECT c.cnsmr_id AS cnsmr_id,
					   stg.[UDEFESTT_NM],
					   stg.[UDEFAMND_APPRVD_DT],
					   stg.[UDEFWITHDRAWAL_DATE],
					   stg.[UDEFBARDATE],
					   stg.[UDEFCRT_ADDR_CITY_TXT],
					   stg.[UDEFCRT_CLM_NM],
					   stg.[UDEFCRT_ADDR_CNTRY_TXT],
					   stg.[UDEFCRT_ADDR_CNTY_TXT],
					   stg.[UDEFIS_NOTERIZED],
					   stg.[UDEFURL],
					   stg.[UDEFFILE_DATE],
					   stg.[UDEFPHN],
					   stg.[UDEFCLAIM_NO],
					   stg.[UDEFDEATH_DATE],
					   stg.[UDEFCRT_ADDR_PSTL_CD],
					   stg.[UDEFSATISFACTION_DATE],
					   stg.[UDEFPHN_EXT],
					   stg.[UDEFAMND_RQSTED_DT],
					   stg.[UDEFREJECTED],
					   stg.[UDEFCRT_ADDR_ADDRSS_LN_1_TXT],
					   stg.[UDEFEML],
					   stg.[UDEFCRT_ADDR_ST_TXT],
					   stg.[UDEFSND_TO_REP],
					   stg.[UDEFIS_AMNDMNT],
					   stg.[UDEFCRT_ADDR_ADDRSS_LN_2_TXT],
					   stg.[UDEFCASE_NO],
					   stg.[UDEFVNDR_CLM_NO],
					   --stg.[UDEFDEATH_DATE_NON_CNFRM],
					   stg.[UDEFSND_TO_ATTY],
					   stg.[UDEFSND_TO_CRT],
					   stg.[UDEFCRT_ADDR_ADDRSS_LN_3_TXT],
					   stg.[UDEFCLAIM_BAL],
					   stg.[UDEFFILED_BY],
					   stg.[UDEFFAX]
					   ,GETDATE() AS upsrt_dttm
					   ,@Default_upsrt_soft_comp_id AS upsrt_soft_comp_id
					   ,1 AS upsrt_trnsctn_nmbr
					   ,@Default_upsrt_usr_id AS upsrt_usr_id
             ,uvl_dod_status.udf_val_list_id
				FROM   ETL.mu_UDEFDECEASEDPROBATE stg
					   INNER JOIN [dbo].[cnsmr] c
						 ON c.agncy_entty_crrltn_id = stg.agncy_entty_crrltn_id
                AND c.cnsmr_idntfr_drvr_lcns_txt = stg.cnsmr_idntfr_drvr_lcns_txt --Surrogate Key
					   LEFT JOIN udf_val_lst uvl_dod_status
					     ON stg.UDEFDOD_STATUS = uvl_dod_status.udf_val_txt
						    AND uvl_dod_status.agncy_dfnd_clmn_id = @dod_status_adc_id
				WHERE  stg.id >= @start_key
					   AND stg.id < @start_key + @chunk_size;
									
				INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
				VALUES
				(
					@msg_tbl_nm,
					'Completed insert into ' + @msg_tbl_nm + ', ' + convert(nvarchar, @@rowcount) + ' rows inserted.  Loop: ' + convert(nvarchar, @loop_count),
					3
				)

                -- advance the start_key for the next set of records in next loop
                set @start_key = @start_key + @chunk_size;

                -- advance the loop
                if @loop_count < @total_loops
                    set @loop_count = @loop_count + 1;
                else
                    set @loop_count = 0;

            end;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - FINISHED',
				'Completed ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

			set @run_time = convert(nvarchar, dateadd(ms, datediff(second, @start_time, dbo.GETDATE()) * 1000, 0), 114);

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - RUN TIME',
				@msg_tbl_nm + ' Total Run Time: ' + ISNULL(@run_time, ''),
				3
			);

        end try

        begin catch
            if @@trancount > 0
                rollback;

            select @err_msg = error_message()
                 , @err_severity = error_severity()
                 , @err_num = error_number();

            insert into etl.mu_processing_log (messagetype, logmessage, severity)
            values('ERROR', cast(@err_num as varchar)+ ' ' + @err_msg, @err_severity);

            insert into etl.mu_processing_log(messagetype, logmessage, logginglevel)
            values('ERROR', 'UDEFDECEASEDPROBATE Rolled Back.', 0);

            raiserror(@err_msg, @err_severity, 16);
        end catch;

        /***********************************************************
            Dump the logging to a permanent table.  
        ***********************************************************/

--        insert into etl.mu_processing_log(messagetype, logmessage, severity, logginglevel, logtime)
--        select messagetype, logmessage, severity, logginglevel, logtime
--        from @logsystem;
		
    end;
go
                                