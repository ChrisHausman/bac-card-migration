USE [crs5_oltp]
GO

/****** Object:  StoredProcedure [etl].[sp_dl_pre_exec_boa]    Script Date: 9/1/2023 10:03:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/* ****************************************************************************************************************** */
/*   Name: Migration Utility - Pre Execution Process                                                                  */
/*   Staging Table: N/A                                                                                               */
/*   Processing Stored Procedure: ETL.sp_dl_pre_exec_boa                                                              */
/*   Date:  04/14/2021                                                                                                */
/*   Purpose:  Custom index maintanence prior to initial DL load.                                                     */
/*   Change Log:                                                                                                      */
/*       Version     Description                         Developer               Date                                 */
/*       ------------------------------------------------------------------------------------------------------------ */
/*       11.2        Initial                             Chris Hausman           12/13/2021                           */
/*       11.2        Fixed bug with error logging.       Chris Hausman           04/29/2022                           */
/*       11.2        Added index to phone Staging Table  Andrew Sheldon          09/01/2023                           */
/*       11.3        Add index on acct lgcy txt.         Chris Hausman           04/29/2024                           */
/*       ------------------------------------------------------------------------------------------------------------ */
/*  TO DO LIST                                                                                                        */
/*  ----------                                                                                                        */
/* ****************************************************************************************************************** */

create or alter procedure [etl].[sp_dl_pre_exec_boa]
as
    begin
        declare @err_msg nvarchar(4000)= N'';
        declare @err_severity int;
        declare @err_num int;
	
        declare @current_datetime as datetime = dbo.getcurrdt();
		
        /**************************************************************************************
            Create permanent logging table.
        **************************************************************************************/

        --This table will keep track of ETL progress.
        if not exists (select 1 from information_schema.tables where table_name = 'mu_Processing_Log')
        begin
            create table etl.mu_processing_log
            (
				pklog        numeric(18, 0) identity(1, 1) not null
			  , messagetype  nvarchar(50) not null
              , logmessage   nvarchar(1000) null
              , severity     int null
              , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
              , logtime      datetime not null default getdate()
            );
        end;

        /************************************************************************************
            Create local logging table.
        ************************************************************************************/

        declare @logsystem table
        (
			pklog        numeric(18, 0) not null identity(1, 1)
		  , messagetype  nvarchar(50) not null
		  , logmessage   nvarchar(1000) not null
		  , severity     int null
		  , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
		  , logtime      datetime not null default getdate()
        );
		
		begin try
		
			/* ****************************************************************************************************** */
			/*  Index creation for cnsmr_idntfr_drvr_lcns_txt and agncy_entty_crrltn_id on all DL/MU staging tables.  */
			/* ****************************************************************************************************** */
			
			--Index for agncy_entty_crrltn_id & cnsmr_idntfr_drvr_lcns_txt on dl_cnsmr_stgng
			IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'dl_cnsmr_dl_num')
				CREATE NONCLUSTERED INDEX dl_cnsmr_dl_num ON dbo.dl_cnsmr_stgng (agncy_entty_crrltn_id, cnsmr_idntfr_drvr_lcns_txt)
			ELSE
				ALTER INDEX dl_cnsmr_dl_num ON dbo.dl_cnsmr_stgng REBUILD WITH (ONLINE = OFF)
			
			INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
			VALUES ('sp_dl_pre_exec_boa', 'Finished creation of dl_cnsmr_dl_num index.', 3)

			--Index for agncy_entty_crrltn_id & cnsmr_idntfr_drvr_lcns_txt on dl_cnsmr_addrss_stgng
			IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'dl_cnsmr_addrss_dl_num')
				CREATE NONCLUSTERED INDEX dl_cnsmr_addrss_dl_num ON dbo.dl_cnsmr_addrss_stgng (agncy_entty_crrltn_id, cnsmr_idntfr_drvr_lcns_txt)
			ELSE
				ALTER INDEX dl_cnsmr_addrss_dl_num ON dbo.dl_cnsmr_addrss_stgng REBUILD WITH (ONLINE = OFF)
			
			INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
			VALUES ('sp_dl_pre_exec_boa', 'Finished creation of dl_cnsmr_addrss_dl_num index.', 3)

			--Index for agncy_entty_crrltn_id & cnsmr_idntfr_drvr_lcns_txt on dl_cnsmr_phn_stgng
			IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'dl_cnsmr_phn_dl_num')
				CREATE NONCLUSTERED INDEX dl_cnsmr_phn_dl_num ON dbo.dl_cnsmr_phn_stgng (agncy_entty_crrltn_id, cnsmr_idntfr_drvr_lcns_txt)
			ELSE
				ALTER INDEX dl_cnsmr_phn_dl_num ON dbo.dl_cnsmr_phn_stgng REBUILD WITH (ONLINE = OFF)
			
			INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
			VALUES ('sp_dl_pre_exec_boa', 'Finished creation of dl_cnsmr_phn_dl_num index.', 3)

			--Index for agncy_entty_crrltn_id & cnsmr_idntfr_drvr_lcns_txt on mu_bnkrptcy
			IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'mu_bnkrptcy_dl_num')
				CREATE NONCLUSTERED INDEX mu_bnkrptcy_dl_num ON etl.mu_bnkrptcy (agncy_entty_crrltn_id, cnsmr_idntfr_drvr_lcns_txt)
			ELSE
				ALTER INDEX mu_bnkrptcy_dl_num ON etl.mu_bnkrptcy REBUILD WITH (ONLINE = OFF)
			
			INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
			VALUES ('sp_dl_pre_exec_boa', 'Finished creation of mu_bnkrptcy_dl_num index.', 3)

			--Index for agncy_entty_crrltn_id & cnsmr_idntfr_drvr_lcns_txt on mu_cnsmr_tag
			IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'mu_cnsmr_tag_dl_num')
				CREATE NONCLUSTERED INDEX mu_cnsmr_tag_dl_num ON etl.mu_cnsmr_tag (agncy_entty_crrltn_id, cnsmr_idntfr_drvr_lcns_txt)
			ELSE
				ALTER INDEX mu_cnsmr_tag_dl_num ON etl.mu_cnsmr_tag REBUILD WITH (ONLINE = OFF)
			
			INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
			VALUES ('sp_dl_pre_exec_boa', 'Finished creation of mu_cnsmr_tag_dl_num index.', 3)

			--Index for cnsmr_phn_nmbr_txt & cnsmr_idntfr_drvr_lcns_txt on dl_cnsmr_phn_stgng
			IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'dl_cnsmr_phn_stgng_dl_num_phn_num')
				CREATE NONCLUSTERED INDEX dl_cnsmr_phn_stgng_dl_num_phn_num ON dbo.dl_cnsmr_phn_stgng (cnsmr_idntfr_drvr_lcns_txt, cnsmr_phn_nmbr_txt)
			ELSE
				ALTER INDEX dl_cnsmr_phn_stgng_dl_num_phn_num ON dbo.dl_cnsmr_phn_stgng REBUILD WITH (ONLINE = OFF)
			
			INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
			VALUES ('sp_dl_pre_exec_boa', 'Finished creation of dl_cnsmr_phn_stgng index.', 3)

			/* ************************************************************************* */
			/*  Index creation for cnsmr_accnt_idntfr_lgcy_txt on DL/MU staging tables.  */
			/* ************************************************************************* */

			--Index for cnsmr_accnt_idntfr_lgcy_txt on dl_cnsmr_accnt_stgng
			IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'dl_ca_lgcy_txt')
				CREATE NONCLUSTERED INDEX dl_ca_lgcy_txt ON dbo.dl_cnsmr_accnt_stgng (cnsmr_accnt_idntfr_lgcy_txt)
			ELSE
				ALTER INDEX dl_ca_lgcy_txt ON dbo.dl_cnsmr_accnt_stgng REBUILD WITH (ONLINE = OFF)
			
			INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
			VALUES ('sp_dl_pre_exec_boa', 'Finished creation of dl_ca_lgcy_txt index.', 3)

			--Index for cnsmr_accnt_idntfr_lgcy_txt on dl_cnsmr_accnt_wrk_actn_stgng
			IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'dl_cawa_lgcy_txt')
				CREATE NONCLUSTERED INDEX dl_cawa_lgcy_txt ON dbo.dl_cnsmr_accnt_wrk_actn_stgng (cnsmr_accnt_idntfr_lgcy_txt)
			ELSE
				ALTER INDEX dl_cawa_lgcy_txt ON dbo.dl_cnsmr_accnt_wrk_actn_stgng REBUILD WITH (ONLINE = OFF)
			
			INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
			VALUES ('sp_dl_pre_exec_boa', 'Finished creation of dl_cawa_lgcy_txt index.', 3)

			--Index for cnsmr_accnt_idntfr_lgcy_txt on dl_cnsmr_accnt_ownrs_stgng
			IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'dl_cao_lgcy_txt')
				CREATE NONCLUSTERED INDEX dl_cao_lgcy_txt ON dbo.dl_cnsmr_accnt_ownrs_stgng (cnsmr_accnt_idntfr_lgcy_txt)
			ELSE
				ALTER INDEX dl_cao_lgcy_txt ON dbo.dl_cnsmr_accnt_ownrs_stgng REBUILD WITH (ONLINE = OFF)
			
			INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
			VALUES ('sp_dl_pre_exec_boa', 'Finished creation of dl_cao_lgcy_txt index.', 3)

			--Index for cnsmr_accnt_idntfr_lgcy_txt on dl_cnsmr_accnt_trnsctn_stgng
			IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'dl_cat_lgcy_txt')
				CREATE NONCLUSTERED INDEX dl_cat_lgcy_txt ON dbo.dl_cnsmr_accnt_trnsctn_stgng (cnsmr_accnt_idntfr_lgcy_txt)
			ELSE
				ALTER INDEX dl_cat_lgcy_txt ON dbo.dl_cnsmr_accnt_trnsctn_stgng REBUILD WITH (ONLINE = OFF)
			
			INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
			VALUES ('sp_dl_pre_exec_boa', 'Finished creation of dl_cat_lgcy_txt index.', 3)

			--Index for cnsmr_accnt_idntfr_lgcy_txt on mu_cnsmr_accnt_tag
			IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'mu_ca_tag_lgcy_txt')
				CREATE NONCLUSTERED INDEX mu_ca_tag_lgcy_txt ON etl.mu_cnsmr_accnt_tag (cnsmr_accnt_idntfr_lgcy_txt)
			ELSE
				ALTER INDEX mu_ca_tag_lgcy_txt ON etl.mu_cnsmr_accnt_tag REBUILD WITH (ONLINE = OFF)
			
			INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
			VALUES ('sp_dl_pre_exec_boa', 'Finished creation of mu_ca_tag_lgcy_txt index.', 3)

		end try

        begin catch
            if @@trancount > 0
                rollback;

            select @err_msg = error_message()
                 , @err_severity = error_severity()
                 , @err_num = error_number();

            insert into etl.mu_processing_log (messagetype, logmessage, severity)
            values('ERROR', cast(@err_num as varchar) + ' ' + @err_msg, @err_severity);

            insert into etl.mu_processing_log(messagetype, logmessage, logginglevel)
            values('ERROR', 'Pre Exec BOA Rolled Back.', 0);

            raiserror(@err_msg, @err_severity, 16);
        end catch;

        /***********************************************************
            Dump the logging to a permanent table.  
        ***********************************************************/

 --       insert into etl.mu_processing_log(messagetype, logmessage, severity, logginglevel, logtime)
 --       select messagetype, logmessage, severity, logginglevel, logtime
 --       from @logsystem;
		
    end;

GO


