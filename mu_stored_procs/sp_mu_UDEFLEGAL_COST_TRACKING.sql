use crs5_oltp;
go

-- drop existing staging table if present
if object_id('ETL.mu_UDEFLEGAL_COST_TRACKING', 'U') is not null
    drop table etl.mu_UDEFLEGAL_COST_TRACKING;

-- mu_UDEFLEGAL_COST_TRACKING staging table
CREATE TABLE [ETL].[mu_UDEFLEGAL_COST_TRACKING](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cnsmr_accnt_idntfr_lgcy_txt] [varchar](20) NULL,
	[UDEFTXN_DT] [datetime] NULL,
	[UDEFFIRM_ID] [varchar](20) NULL,
	[UDEFRCVR_SHRT_NM] [varchar](32) NULL,
	[UDEFPOST_DT] [datetime] NULL,
	[UDEFRECOV_COST_VAL] [varchar](256) NULL,
	[UDEFCOST_TYP_CD] [varchar](10) NULL,
	[UDEFINVOICE_DT] [datetime] NULL,
	[UDEFCOST_AMT] [decimal](12, 3) NULL,
	[UDEFUNQ_IMP_KY] [varchar](30) NULL
) ON [PRIMARY]

-- create the processing stored procedure
if object_id('ETL.sp_mu_UDEFLEGAL_COST_TRACKING', 'P') is not null
    drop procedure etl.sp_mu_UDEFLEGAL_COST_TRACKING;
go

/* ****************************************************************************************************************** */
/*   Name: Direct Load LEGAL_COST_TRACKING UDP                                                                        */
/*   Staging Table: ETL.mu_UDEFLEGAL_COST_TRACKING                                                                    */
/*   Processing Stored Procedure: ETL.sp_mu_UDEFLEGAL_COST_TRACKING                                                   */
/*   Date:  02/09/2022                                                                                                */
/*   Purpose:  Custom DL script for loading data into LEGAL_COST_TRACKING UDP                                         */
/*   Change Log:                                                                                                      */
/*       Version     Description                         Developer               Date                                 */
/*       ------------------------------------------------------------------------------------------------------------ */
/*       1.0         Initial - for BAC TSYS migration    Chris Hausman           02/09/2022                           */
/*       1.1         Removed CREATE_DT                   Chris Hausman           03/17/2022                           */
/*                   Removed RCVR_GRP_SHRT_NM                                                                         */
/*                   Changed RCVR_SHRT_NM to varchar(32)                                                              */
/*       1.2         Fixed bug with error logging.       Chris Hausman           04/29/2022                           */
/*       1.3         Added UDEFUNQ_IMP_KY                Chris Hausman           07/08/2022                           */
/*                   Changed RECOV_COST to LOV type                                                                   */
/*       1.4         Direct to table logging             Andrew Sheldon          04/12/2023                                                                    */
/*       ------------------------------------------------------------------------------------------------------------ */
/*  TO DO LIST                                                                                                        */
/*  ----------                                                                                                        */
/*   (1) N/A                                                                                                          */
/* ****************************************************************************************************************** */

create   procedure etl.sp_mu_UDEFLEGAL_COST_TRACKING @chunk_size bigint = 10000
as
    begin
        declare @err_msg nvarchar(4000)= N'';
        declare @err_severity int;
        declare @err_num int;

        declare @msg_tbl_nm nvarchar(50)= N'UDEFLEGAL_COST_TRACKING';

        declare @start_time datetime;
        declare @run_time varchar(20);
        declare @loop_count int;
        declare @total_count bigint;
        declare @total_loops int;
        declare @start_key bigint;

        declare @default_upsrt_soft_comp_id bigint;
        declare @default_upsrt_usr_id bigint;
		
        declare @current_datetime as datetime = dbo.GETDATE();
		
		declare @udef_adt_id as bigint;
		declare @recov_cost_typ_adc_id as bigint;

        /**************************************************************************************
            Create permanent logging table.  
        **************************************************************************************/

        --This table will keep track of ETL progress.
        if not exists (select 1 from information_schema.tables where table_name = 'mu_Processing_Log')
        begin
            create table etl.mu_processing_log
            (
				pklog        numeric(18, 0) identity(1, 1) not null
			  , messagetype  nvarchar(50) not null
              , logmessage   nvarchar(1000) null
              , severity     int null
              , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
              , logtime      datetime not null default getdate()
            );
        end;

        /************************************************************************************
            Create local logging table.
        ************************************************************************************/

        declare @logsystem table
        (
			pklog        numeric(18, 0) not null identity(1, 1)
		  , messagetype  nvarchar(50) not null
		  , logmessage   nvarchar(1000) not null
		  , severity     int null
		  , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
		  , logtime      datetime not null default getdate()
        );
		
        begin try
            select @start_time = @current_datetime;

            -- set some default values
            select @default_upsrt_soft_comp_id = upsrt_soft_comp_id
            from dbo.lkup_soft_comp
            where upsrt_soft_comp_nm = 'load';

            select @default_upsrt_usr_id = usr_id
            from dbo.usr
            where usr_usrnm = 'CNVRSN';
			
			--set the variables for the list of value fields
			select @udef_adt_id = agncy_dfnd_tbl_id
			from   agncy_Dfnd_Tbl
			where  agncy_dfnd_tbl_nm = 'UDEFLEGAL_COST_TRACKING'
			
			select @recov_cost_typ_adc_id = agncy_dfnd_clmn_id
			from   agncy_Dfnd_Clmn
			where  agncy_dfnd_clmn_nm = 'UDEFRECOV_COST'
			       and agncy_dfnd_tbl_id = @udef_adt_id

            -- setup the looping variables to process in chucks
            set @loop_count = 1;

            select @total_count = count(*)
            from etl.mu_UDEFLEGAL_COST_TRACKING;

            select @start_key = min(id)
            from etl.mu_UDEFLEGAL_COST_TRACKING;

            set @total_loops = @total_count / @chunk_size + 1;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - START',
				'Starting ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

            while @loop_count > 0
            begin
				
				INSERT INTO [dbo].[UDEFLEGAL_COST_TRACKING]
				(
					[cnsmr_accnt_id],
	                [UDEFTXN_DT],
	                [UDEFFIRM_ID],
	                [UDEFRCVR_SHRT_NM],
	                [UDEFPOST_DT],
	                [UDEFRECOV_COST],
	                [UDEFCOST_TYP_CD],
	                [UDEFINVOICE_DT],
	                [UDEFCOST_AMT],
					[UDEFUNQ_IMP_KY],
					[upsrt_dttm],
					[upsrt_soft_comp_id],
					[upsrt_trnsctn_nmbr],
					[upsrt_usr_id]
				)
				SELECT ca.cnsmr_accnt_id AS cnsmr_accnt_id,
	                   stg.UDEFTXN_DT,
	                   stg.UDEFFIRM_ID,
	                   stg.UDEFRCVR_SHRT_NM,
	                   stg.UDEFPOST_DT,
	                   uvl_recov_cost.udf_val_list_id,
	                   stg.UDEFCOST_TYP_CD,
	                   stg.UDEFINVOICE_DT,
	                   stg.UDEFCOST_AMT,
					   stg.UDEFUNQ_IMP_KY,
					   GETDATE() AS upsrt_dttm,
					   @Default_upsrt_soft_comp_id AS upsrt_soft_comp_id,
					   1 AS upsrt_trnsctn_nmbr,
					   @Default_upsrt_usr_id AS upsrt_usr_id
				FROM   ETL.mu_UDEFLEGAL_COST_TRACKING stg
					   INNER JOIN [dbo].[cnsmr_accnt] ca
						 ON ca.cnsmr_accnt_idntfr_lgcy_txt = stg.cnsmr_accnt_idntfr_lgcy_txt
					   LEFT JOIN udf_val_lst uvl_recov_cost
					     ON stg.UDEFRECOV_COST_VAL = uvl_recov_cost.udf_val_txt
						    AND uvl_recov_cost.agncy_dfnd_clmn_id = @recov_cost_typ_adc_id
				WHERE  stg.id >= @start_key
					   AND stg.id < @start_key + @chunk_size;
									
				INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
				VALUES
				(
					@msg_tbl_nm,
					'Completed insert into ' + @msg_tbl_nm + ', ' + convert(nvarchar, @@rowcount) + ' rows inserted.  Loop: ' + convert(nvarchar, @loop_count),
					3
				)

                -- advance the start_key for the next set of records in next loop
                set @start_key = @start_key + @chunk_size;

                -- advance the loop
                if @loop_count < @total_loops
                    set @loop_count = @loop_count + 1;
                else
                    set @loop_count = 0;

            end;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - FINISHED',
				'Completed ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

			set @run_time = convert(nvarchar, dateadd(ms, datediff(second, @start_time, dbo.GETDATE()) * 1000, 0), 114);

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - RUN TIME',
				@msg_tbl_nm + ' Total Run Time: ' + ISNULL(@run_time, ''),
				3
			);

        end try

        begin catch
            if @@trancount > 0
                rollback;

            select @err_msg = error_message()
                 , @err_severity = error_severity()
                 , @err_num = error_number();

            insert into etl.mu_processing_log (messagetype, logmessage, severity)
            values('ERROR', cast(@err_num as varchar) + ' ' + @err_msg, @err_severity);

            insert into etl.mu_processing_log(messagetype, logmessage, logginglevel)
            values('ERROR', 'UDEFLEGAL_COST_TRACKING Rolled Back.', 0);

            raiserror(@err_msg, @err_severity, 16);
        end catch;

        /***********************************************************
            Dump the logging to a permanent table.  
        ***********************************************************/

--        insert into etl.mu_processing_log(messagetype, logmessage, severity, logginglevel, logtime)
--        select messagetype, logmessage, severity, logginglevel, logtime
--        from @logsystem;
		
    end;
GO

