use crs5_oltp;
go

-- drop existing staging table if present

if object_id('ETL.mu_UDEFESTT_CNTCT_INF', 'U') is not null
    drop table etl.mu_UDEFESTT_CNTCT_INF;

-- mu_UDEFESTT_CNTCT_INF staging table
CREATE TABLE [ETL].[mu_UDEFESTT_CNTCT_INF](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[agncy_entty_crrltn_id] [varchar](30) NULL,
	[cnsmr_idntfr_drvr_lcns_txt] [varchar](20) NULL,
	[UDEFADDRESS_ADDRSS_LN_3_TXT] [varchar](128) NULL,
	[UDEFADDRESS_CNTY_TXT] [varchar](128) NULL,
	[UDEFADDRESS_ADDRSS_LN_1_TXT] [varchar](128) NULL,
	[UDEFEML] [varchar](1024) NULL,
	[UDEFADDRESS_CNTRY_TXT] [varchar](128) NULL,
	[UDEFCNTCT_NM] [varchar](80) NULL,
	[UDEFADDRESS_CITY_TXT] [varchar](64) NULL,
	[UDEFADDRESS_ADDRSS_LN_2_TXT] [varchar](128) NULL,
	[UDEFPHN] [varchar](10) NULL,
	[UDEFADDRESS_ST_TXT] [varchar](20) NULL,
	[UDEFREL_VAL] [varchar](256) NULL,
	[UDEFADDRESS_PSTL_CD] [varchar](32) NULL,
	[UDEFFAX] [varchar](10) NULL,
	[UDEFPHN_EXT] [varchar](10) NULL
) ON [PRIMARY]

-- create the processing stored procedure

if object_id('ETL.sp_mu_UDEFESTT_CNTCT_INF', 'P') is not null
    drop procedure etl.sp_mu_UDEFESTT_CNTCT_INF;
go

/* ****************************************************************************************************************** */
/*   Name: Direct Load ESTT_CNTCT_INF UDP                                                                             */
/*   Staging Table: ETL.mu_UDEFESTT_CNTCT_INF                                                                         */
/*   Processing Stored Procedure: ETL.sp_mu_UDEFESTT_CNTCT_INF                                                        */
/*   Date:  11/16/2021                                                                                                */
/*   Purpose:  Custom DL script for loading data into ESTT_CNTCT_INF UDP                                              */
/*   Change Log:                                                                                                      */
/*       Version     Description                         Developer               Date                                 */
/*       ------------------------------------------------------------------------------------------------------------ */
/*       11.1        Initial - for BAC TSYS migration    Chris Hausman           11/16/2021                           */
/*       11.2.BAC    Added join to cnsmr using DL#.      Chris Hausman           12/13/2021                           */
/*       11.2.BAC    Fixed bug with error logging.       Chris Hausman           04/29/2022                           */
/*       11.2.BAC    Direct to table logging.            Andrew Sheldon          04/12/2023                           */
/*       ------------------------------------------------------------------------------------------------------------ */
/*  TO DO LIST                                                                                                        */
/*  ----------                                                                                                        */
/*   (1) N/A                                                                                                          */
/* ****************************************************************************************************************** */

create   procedure etl.sp_mu_UDEFESTT_CNTCT_INF @chunk_size bigint = 10000
as
    begin
        declare @err_msg nvarchar(4000)= N'';
        declare @err_severity int;
        declare @err_num int;

        declare @msg_tbl_nm nvarchar(50)= N'UDEFESTT_CNTCT_INF';

        declare @start_time datetime;
        declare @run_time varchar(20);
        declare @loop_count int;
        declare @total_count bigint;
        declare @total_loops int;
        declare @start_key bigint;

        declare @default_upsrt_soft_comp_id bigint;
        declare @default_upsrt_usr_id bigint;
		
        declare @current_datetime as datetime = dbo.GETDATE();
		
		declare @udef_adt_id as bigint;
		declare @rel_adc_id as bigint;
		
        /**************************************************************************************
            Create permanent logging table.  
        **************************************************************************************/

        --This table will keep track of ETL progress.
        if not exists (select 1 from information_schema.tables where table_name = 'mu_Processing_Log')
        begin
            create table etl.mu_processing_log
            (
				pklog        numeric(18, 0) identity(1, 1) not null
			  , messagetype  nvarchar(50) not null
              , logmessage   nvarchar(1000) null
              , severity     int null
              , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
              , logtime      datetime not null default getdate()
            );
        end;

        /************************************************************************************
            Create local logging table.
        ************************************************************************************/

        declare @logsystem table
        (
			pklog        numeric(18, 0) not null identity(1, 1)
		  , messagetype  nvarchar(50) not null
		  , logmessage   nvarchar(1000) not null
		  , severity     int null
		  , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
		  , logtime      datetime not null default getdate()
        );
		
        begin try
            select @start_time = @current_datetime;

            -- set some default values
            select @default_upsrt_soft_comp_id = upsrt_soft_comp_id
            from dbo.lkup_soft_comp
            where upsrt_soft_comp_nm = 'load';

            select @default_upsrt_usr_id = usr_id
            from dbo.usr
            where usr_usrnm = 'CNVRSN';
			
			--set the variables for the list of value fields
			select @udef_adt_id = agncy_dfnd_tbl_id
			from   agncy_Dfnd_Tbl
			where  agncy_dfnd_tbl_nm = 'UDEFESTT_CNTCT_INF'
			
			select @rel_adc_id = agncy_dfnd_clmn_id
			from   agncy_Dfnd_Clmn
			where  agncy_dfnd_clmn_nm = 'UDEFREL'
			       and agncy_dfnd_tbl_id = @udef_adt_id

            -- setup the looping variables to process in chucks
            set @loop_count = 1;

            select @total_count = count(*)
            from etl.mu_UDEFESTT_CNTCT_INF;

            select @start_key = min(id)
            from etl.mu_UDEFESTT_CNTCT_INF;

            set @total_loops = @total_count / @chunk_size + 1;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - START',
				'Starting ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

            while @loop_count > 0
            begin
				
				INSERT INTO [dbo].[UDEFESTT_CNTCT_INF]
				(
					[cnsmr_id],
					[UDEFADDRESS_ADDRSS_LN_3_TXT],
					[UDEFADDRESS_CNTY_TXT],
					[UDEFADDRESS_ADDRSS_LN_1_TXT],
					[UDEFEML],
					[UDEFADDRESS_CNTRY_TXT],
					[UDEFCNTCT_NM],
					[UDEFADDRESS_CITY_TXT],
					[UDEFADDRESS_ADDRSS_LN_2_TXT],
					[UDEFPHN],
					[UDEFADDRESS_ST_TXT],
					[UDEFREL],
					[UDEFADDRESS_PSTL_CD],
					[UDEFFAX],
					[UDEFPHN_EXT],
					[upsrt_dttm],
					[upsrt_soft_comp_id],
					[upsrt_trnsctn_nmbr],
					[upsrt_usr_id]
				)
				SELECT c.cnsmr_id AS cnsmr_id,
					   stg.[UDEFADDRESS_ADDRSS_LN_3_TXT],
					   stg.[UDEFADDRESS_CNTY_TXT],
					   stg.[UDEFADDRESS_ADDRSS_LN_1_TXT],
					   stg.[UDEFEML],
					   stg.[UDEFADDRESS_CNTRY_TXT],
					   stg.[UDEFCNTCT_NM],
					   stg.[UDEFADDRESS_CITY_TXT],
					   stg.[UDEFADDRESS_ADDRSS_LN_2_TXT],
					   stg.[UDEFPHN],
					   stg.[UDEFADDRESS_ST_TXT],
					   uvl.udf_val_list_id, --double check if this should default to null if empty
					   stg.[UDEFADDRESS_PSTL_CD],
					   stg.[UDEFFAX],
					   stg.[UDEFPHN_EXT],
					   GETDATE() AS upsrt_dttm,
					   @Default_upsrt_soft_comp_id AS upsrt_soft_comp_id,
					   1 AS upsrt_trnsctn_nmbr,
					   @Default_upsrt_usr_id AS upsrt_usr_id
				FROM   ETL.mu_UDEFESTT_CNTCT_INF stg
					   INNER JOIN [dbo].[cnsmr] c
						 ON c.agncy_entty_crrltn_id = stg.agncy_entty_crrltn_id
						    AND c.cnsmr_idntfr_drvr_lcns_txt = stg.cnsmr_idntfr_drvr_lcns_txt --Surrogate Key
					   LEFT JOIN udf_val_lst uvl
					     ON stg.UDEFREL_VAL = uvl.udf_val_txt
						    AND uvl.agncy_dfnd_clmn_id = @rel_adc_id
				WHERE  stg.id >= @start_key
					   AND stg.id < @start_key + @chunk_size;
									
				INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
				VALUES
				(
					@msg_tbl_nm,
					'Completed insert into ' + @msg_tbl_nm + ', ' + convert(nvarchar, @@rowcount) + ' rows inserted.  Loop: ' + convert(nvarchar, @loop_count),
					3
				)

                -- advance the start_key for the next set of records in next loop
                set @start_key = @start_key + @chunk_size;

                -- advance the loop
                if @loop_count < @total_loops
                    set @loop_count = @loop_count + 1;
                else
                    set @loop_count = 0;

            end;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - FINISHED',
				'Completed ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

			set @run_time = convert(nvarchar, dateadd(ms, datediff(second, @start_time, dbo.GETDATE()) * 1000, 0), 114);

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - RUN TIME',
				@msg_tbl_nm + ' Total Run Time: ' + ISNULL(@run_time, ''),
				3
			);

        end try

        begin catch
            if @@trancount > 0
                rollback;

            select @err_msg = error_message()
                 , @err_severity = error_severity()
                 , @err_num = error_number();

            insert into etl.mu_processing_log (messagetype, logmessage, severity)
            values('ERROR', cast(@err_num as varchar) + ' ' + @err_msg, @err_severity);

            insert into etl.mu_processing_log(messagetype, logmessage, logginglevel)
            values('ERROR', 'UDEFESTT_CNTCT_INF Rolled Back.', 0);

            raiserror(@err_msg, @err_severity, 16);
        end catch;

        /***********************************************************
            Dump the logging to a permanent table.  
        ***********************************************************/

--        insert into etl.mu_processing_log(messagetype, logmessage, severity, logginglevel, logtime)
--        select messagetype, logmessage, severity, logginglevel, logtime
--        from @logsystem;
		
    end;
GO

