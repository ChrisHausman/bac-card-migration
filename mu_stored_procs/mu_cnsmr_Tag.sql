use crs5_oltp;
go

-- drop existing staging table if present
if object_id('ETL.mu_cnsmr_Tag', 'U') is not null
    drop table ETL.mu_cnsmr_tag;

-- create the staging table
create table ETL.mu_cnsmr_tag
(
	cnsmr_tag_id              bigint identity(1, 1) not null
    ,cnsmr_idntfr_lgcy_txt    varchar(40) not null
	,agncy_entty_crrltn_id    varchar(30) Null
	,cnsmr_idntfr_drvr_lcns_txt varchar(20) NULL
    ,tag_shrt_nm              varchar(8) not null
    ,cnsmr_tag_assgn_dt       datetime null
    ,cnsmr_tag_ordr_nmbr      int null
    ,cnsmr_tag_sft_delete_flg char(1) not null
    ,upsrt_btch_nmbr          bigint null
);

alter table ETL.mu_cnsmr_tag
add constraint df_mu_cnsmr_tag_sft_delete_flg default 'N' for cnsmr_tag_sft_delete_flg;
go

-- create the processing stored procedure
use dm_oltp;
go

if object_id('ETL.sp_mu_cnsmr_Tag', 'P') is not null
    drop procedure ETL.sp_mu_cnsmr_tag;
go

/* ****************************************************************************************************************** */
/*   Name: Migration Utility - cnsmr_Tag - Processing                                                                 */
/*   Staging Table: ETL.mu_cnsmr_Tag                                                                                  */
/*   Processing Stored Procedure: ETL.sp_mu_cnsmr_Tag                                                                 */
/*   Date:  07/07/2020                                                                                                */
/*   Purpose:  This script is designed to create the staging table and processing stored procedure                    */
/*             for the migration utility (MU) to migrate the cnsmr_Tag entity.                                        */
/*   Change Log:                                                                                                      */
/*       Version     Description                         Developer               Date                                 */
/*       ------------------------------------------------------------------------------------------------------------ */
/*       11.1        Initial - for DM 11.1               Doug Barlett            07/07/2020                           */
/*       11.1.BAC    Updated for BAC's TSYS Migration    Chris Hausman           12/29/2020                           */
/*       11.2.BAC    Added join to cnsmr using DL#.      Chris Hausman           12/13/2021                           */
/*       11.2.BAC    Fixed bug with error logging.       Chris Hausman           04/29/2022                           */
/*       11.3.BAC    Direct to table logging.            Andrew Sheldon          04/11/2023                           */
/*       ------------------------------------------------------------------------------------------------------------ */
/*  TO DO LIST                                                                                                        */
/*  ----------                                                                                                        */
/*   (1) Are any consumer level system codes being used? If not the second half of this script can be removed.        */
/*   (2) Change @cnsmr_list to use Correlation rather than Legacy if we need to keep the sys code section.            */
/* ****************************************************************************************************************** */

create   procedure [etl].[sp_mu_cnsmr_tag] @chunk_size bigint = 10000, @process_system_cd bit = 1
as
    begin
        declare @err_msg nvarchar(4000)= N'';
        declare @err_severity int;
        declare @err_num int;

        declare @msg_tbl_nm nvarchar(50) = N'cnsmr_tag';

        declare @start_time datetime;
        declare @run_time varchar(20);
        declare @loop_count int;
        declare @total_count bigint;
        declare @total_loops int;
        declare @start_key bigint;

        declare @default_upsrt_soft_comp_id bigint;
        declare @default_upsrt_usr_id bigint;

        declare @current_datetime datetime = dbo.getcurrdt();

        /**********************************************************************************
            Create permanent logging table just for secondary ownership step.  
        **********************************************************************************/

        --This table will keep track of ETL progress.
        if not exists (select 1 from information_schema.tables where table_name = 'mu_Processing_Log')
        begin
            create table ETL.mu_processing_log
            (
				pklog         numeric(18, 0) identity(1, 1) not null
                ,messagetype  nvarchar(50) not null
                ,logmessage   nvarchar(1000) null
                ,severity     int null
                ,logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
                ,logtime      datetime not null default getdate()
            );
        end;

        /******************************************************************
            Create local logging table.
        ******************************************************************/

        -- The following table variable lives outside the overall transaction and allows us to keep log messages through roll back.
        declare @logsystem table
        (
			pklog        numeric(18, 0) not null identity(1, 1)
           ,messagetype  nvarchar(50) not null
           ,logmessage   nvarchar(1000) not null
           ,severity     int null
           ,logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
           ,logtime      datetime not null default getdate()
        );

        -- The following table variable is used to determine system code status on tags being assigned and apply that status to the cnsmr_wrk_actn record.
        declare @cnsmr_list table
        (
			cnsmr_id bigint not null
            ,cnsmr_idntfr_lgcy_txt varchar(40) not null
            ,primary key(cnsmr_id)
        );

        insert into @cnsmr_list(cnsmr_id, cnsmr_idntfr_lgcy_txt)
        select distinct c.cnsmr_id, stg.cnsmr_idntfr_lgcy_txt
        from ETL.mu_cnsmr_tag stg
             inner join dbo.cnsmr c
			   on c.agncy_entty_crrltn_id = stg.agncy_entty_crrltn_id --CRK
			      AND c.cnsmr_idntfr_drvr_lcns_txt = stg.cnsmr_idntfr_drvr_lcns_txt --Surrogate Key

        begin try
            select @start_time = @current_datetime;

            select @default_upsrt_soft_comp_id = upsrt_soft_comp_id
            from lkup_soft_comp
            where upsrt_soft_comp_nm = 'load';

            select @default_upsrt_usr_id = usr_id
            from usr
            where usr_usrnm = 'system';

            -- setup the looping variables to process in chucks
            set @loop_count = 1;

            select @total_count = count(*)
            from ETL.mu_cnsmr_tag;

            select @start_key = min(cnsmr_tag_id)
            from ETL.mu_cnsmr_tag;
            set @total_loops = @total_count / @chunk_size + 1;

            insert into ETL.mu_processing_log(messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - START',
			    'Starting ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

            while @loop_count > 0
            begin
                begin transaction;

                -- create the cnsmr_Tag record
                insert into dbo.cnsmr_tag
				(
					cnsmr_id
                    ,tag_id
                    ,cnsmr_tag_assgn_dt
                    ,cnsmr_tag_ordr_nmbr
                    ,cnsmr_tag_sft_delete_flg
                    ,upsrt_btch_nmbr
                    ,upsrt_dttm
                    ,upsrt_soft_comp_id
                    ,upsrt_trnsctn_nmbr
                    ,upsrt_usr_id
				)
                select c.cnsmr_id as cnsmr_id
                       ,t.tag_id as tag_id
					   ,isnull(stg.cnsmr_tag_assgn_dt, GETDATE()) as cnsmr_tag_assgn_dt
                       ,stg.cnsmr_tag_ordr_nmbr as cnsmr_tag_ordr_nmbr
                       ,stg.cnsmr_tag_sft_delete_flg as cnsmr_tag_sft_delete_flg
                       ,stg.upsrt_btch_nmbr as upsrt_btch_nmbr
                       ,@current_datetime as upsrt_dttm
                       ,@default_upsrt_soft_comp_id as upsrt_soft_comp_id
                       ,1 as upsrt_trnsctn_nmbr
                       ,@default_upsrt_usr_id as upsrt_usr_id
                from   etl.mu_cnsmr_tag stg
                       inner join dbo.cnsmr c
					     on c.agncy_entty_crrltn_id = stg.agncy_entty_crrltn_id --CRK
						    AND c.cnsmr_idntfr_drvr_lcns_txt = stg.cnsmr_idntfr_drvr_lcns_txt --Surrogate Key
                       inner join dbo.tag t
					     on t.tag_shrt_nm = stg.tag_shrt_nm
                where  stg.cnsmr_tag_id >= @start_key
                       and stg.cnsmr_tag_id < @start_key + @chunk_size;

                insert into ETL.mu_processing_log(messagetype, logmessage, logginglevel)
                values
				(
					@msg_tbl_nm,
					'Completed insert into ' + @msg_tbl_nm
					 + ', ' + convert(nvarchar, @@rowcount)
					 + ' rows inserted.  Loop: ' + convert(nvarchar, @loop_count),
					3
				);

                -- Based on the passed in "@process_system_cd" (defaults to 1 (true))
                -- update the cnsmr_wrk_actn records for those consumers assigned tags with system codes attached
                if @process_system_cd = 1
                begin
                    -- callable flag
                    update cwa
                        set 
                          cwa.cnsmr_is_cllbl_flg = 'Y'
                        , cwa.upsrt_usr_id = @default_upsrt_usr_id
                        , cwa.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , cwa.upsrt_trnsctn_nmbr = cwa.upsrt_trnsctn_nmbr + 1
                        , cwa.upsrt_dttm = getdate()
                    from dbo.cnsmr_wrk_actn cwa
                            inner join @cnsmr_list cl on cwa.cnsmr_id = cl.cnsmr_id
                            inner join ETL.mu_cnsmr_tag stg on stg.cnsmr_idntfr_lgcy_txt = cl.cnsmr_idntfr_lgcy_txt
                    where exists
                    (
                        select 1
                        from cnsmr_tag ct
                                inner join tag t on ct.tag_id = t.tag_id
                                                    and ct.cnsmr_id = cl.cnsmr_id
                                                    and ct.cnsmr_tag_sft_delete_flg = 'N'
                                inner join systm_cd sc on t.systm_cd = sc.systm_cd
                                                        and sc.systm_cd_is_cllbl_flg = 'Y'
                                                        and sc.systm_cd_is_actv_flg = 'Y'
                    )
                        and cwa.cnsmr_is_cllbl_flg <> 'Y'
                        and stg.cnsmr_tag_id >= @start_key
                        and stg.cnsmr_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_wrk_actn callable flag = Y, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    update cwa
                        set 
                          cwa.cnsmr_is_cllbl_flg = 'N'
                        , cwa.upsrt_usr_id = @default_upsrt_usr_id
                        , cwa.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , cwa.upsrt_trnsctn_nmbr = cwa.upsrt_trnsctn_nmbr + 1
                        , cwa.upsrt_dttm = getdate()
                    from dbo.cnsmr_wrk_actn cwa
                            inner join @cnsmr_list cl on cwa.cnsmr_id = cl.cnsmr_id
                            inner join ETL.mu_cnsmr_tag stg on stg.cnsmr_idntfr_lgcy_txt = cl.cnsmr_idntfr_lgcy_txt
                    where exists
                    (
                        select 1
                        from cnsmr_tag ct
                                inner join tag t on ct.tag_id = t.tag_id
                                                    and ct.cnsmr_id = cl.cnsmr_id
                                                    and ct.cnsmr_tag_sft_delete_flg = 'N'
                                inner join systm_cd sc on t.systm_cd = sc.systm_cd
                                                        and sc.systm_cd_is_cllbl_flg = 'N'
                                                        and sc.systm_cd_is_actv_flg = 'Y'
                    )
                        and cwa.cnsmr_is_cllbl_flg <> 'N'
                        and stg.cnsmr_tag_id >= @start_key
                        and stg.cnsmr_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_wrk_actn callable flag = N, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    -- payable flag
                    update cwa
                        set 
                          cwa.cnsmr_is_pyble_flg = 'Y'
                        , cwa.upsrt_usr_id = @default_upsrt_usr_id
                        , cwa.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , cwa.upsrt_trnsctn_nmbr = cwa.upsrt_trnsctn_nmbr + 1
                        , cwa.upsrt_dttm = getdate()
                    from dbo.cnsmr_wrk_actn cwa
                            inner join @cnsmr_list cl on cwa.cnsmr_id = cl.cnsmr_id
                            inner join ETL.mu_cnsmr_tag stg on stg.cnsmr_idntfr_lgcy_txt = cl.cnsmr_idntfr_lgcy_txt
                    where exists
                    (
                        select 1
                        from cnsmr_tag ct
                                inner join tag t on ct.tag_id = t.tag_id
                                                    and ct.cnsmr_id = cl.cnsmr_id
                                                    and ct.cnsmr_tag_sft_delete_flg = 'N'
                                inner join systm_cd sc on t.systm_cd = sc.systm_cd
                                                        and sc.systm_cd_is_pybl_flg = 'Y'
                                                        and sc.systm_cd_is_actv_flg = 'Y'
                    )
                        and cwa.cnsmr_is_pyble_flg <> 'Y'
                        and stg.cnsmr_tag_id >= @start_key
                        and stg.cnsmr_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_wrk_actn payable flag = Y, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                    update cwa
                        set 
                          cwa.cnsmr_is_pyble_flg = 'N'
                        , cwa.upsrt_usr_id = @default_upsrt_usr_id
                        , cwa.upsrt_soft_comp_id = @default_upsrt_soft_comp_id
                        , cwa.upsrt_trnsctn_nmbr = cwa.upsrt_trnsctn_nmbr + 1
                        , cwa.upsrt_dttm = getdate()
                    from dbo.cnsmr_wrk_actn cwa
                            inner join @cnsmr_list cl on cwa.cnsmr_id = cl.cnsmr_id
                            inner join ETL.mu_cnsmr_tag stg on stg.cnsmr_idntfr_lgcy_txt = cl.cnsmr_idntfr_lgcy_txt
                    where exists
                    (
                        select 1
                        from cnsmr_tag ct
                                inner join tag t on ct.tag_id = t.tag_id
                                                    and ct.cnsmr_id = cl.cnsmr_id
                                                    and ct.cnsmr_tag_sft_delete_flg = 'N'
                                inner join systm_cd sc on t.systm_cd = sc.systm_cd
                                                        and sc.systm_cd_is_pybl_flg = 'N'
                                                        and sc.systm_cd_is_actv_flg = 'Y'
                    )
                        and cwa.cnsmr_is_pyble_flg <> 'N'
                        and stg.cnsmr_tag_id >= @start_key
                        and stg.cnsmr_tag_id < @start_key + @chunk_size;

                    insert into ETL.mu_processing_log(messagetype
                                            , logmessage
                                            , logginglevel)
                    values(@msg_tbl_nm, 'Loop: ' + convert(nvarchar, @loop_count) + ' Updated cnsmr_wrk_actn payable flag = N, ' + convert(nvarchar, @@rowcount) + ' rows updated.', 3);

                end;

                -- advance the start_key for the next set of records in next loop
                set @start_key = @start_key + @chunk_size;

                -- advance the loop
                if @loop_count < @total_loops
                    set @loop_count = @loop_count + 1;
                else
                    set @loop_count = 0;

                if @@trancount > 0
                    commit transaction;
            end;

            insert into ETL.mu_processing_log(messagetype
                                 , logmessage
                                 , logginglevel)
            values(@msg_tbl_nm + ' - FINISHED', 'Completed ' + @msg_tbl_nm + ' Loading of ' + convert(nvarchar, @total_count) + ' records, using ' + convert(nvarchar, @total_loops) + ' loops.', 3);

            set @run_time = convert(nvarchar, dateadd(ms, datediff(second, @start_time, dbo.getcurrdt()) * 1000, 0), 114);

            insert into ETL.mu_processing_log(messagetype
                                 , logmessage
                                 , logginglevel)
            values(@msg_tbl_nm + ' - RUN TIME', @msg_tbl_nm + ' Total Run Time: ' + @run_time, 3);

        end try

        begin catch
            if @@trancount > 0
                rollback;

            select @err_msg = error_message()
                 , @err_severity = error_severity()
                 , @err_num = error_number();

            insert into ETL.mu_processing_log(messagetype
                                 , logmessage
                                 , severity)
            values('ERROR', cast(@err_num as varchar) + ' ' + @err_msg, @err_severity);

            insert into ETL.mu_processing_log(messagetype
                                 , logmessage
                                 , logginglevel)
            values('ERROR', 'Status Dispo Rolled Back.', 0);

            raiserror(@err_msg, @err_severity, 16);
        end catch;

        /*******************************************************
            Dump the logging to a permanent table.  
        *******************************************************/

--        insert into ETL.mu_processing_log(messagetype
--                                        , logmessage
--                                        , severity
--                                        , logginglevel
--                                        , logtime)
--        select messagetype
--             , logmessage
--             , severity
--             , logginglevel
--             , logtime
--        from @logsystem;
    end;
GO

