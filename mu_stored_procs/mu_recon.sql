USE crs5_oltp
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* ****************************************************************************************************************** */
/*   Name: Migration Utility - Bucket Reconciliation                                                                  */
/*   Staging Table: ETL.mu_recon                                                                                      */
/*   Processing Stored Procedure: ETL.sp_mu_recon                                                                     */
/*   Date:  07/07/2020                                                                                                */
/*   Purpose:  This script is designed to create the staging table and processing stored procedure                    */
/*             for the migration utility (MU) to perform a bucket level reconciliation with legacy system             */
/*             after all historical financial transactions have been migrated to DM.                                  */
/*   Change Log:                                                                                                      */
/*       Version     Description                         Developer               Date                                 */
/*       ------------------------------------------------------------------------------------------------------------ */
/*       11.1        Initial - for DM 11.1               Chris Hausman           07/07/2020                           */
/*       11.1.BAC    Updated for BAC's TSYS Migration    Chris Hausman           12/29/2020                           */
/*       11.1.BAC    Updates based on 1/19/21 BAC call.  Chris Hausman           02/13/2021                           */
/*       ------------------------------------------------------------------------------------------------------------ */
/*  TO DO LIST                                                                                                        */
/*  ----------                                                                                                        */
/*   (1) Figure out where the TSYS current bucket values will be stored. We just need them in any staging table.      */
/*       1/19/21 => ChrisH to determine what is most effective for the SP.                                            */
/*       2/13/21 => Decided to stick with Charge DL table - BAC WILL NEED TO POPULATE WITH CURRENT TSYS BCKT BALS     */
/*   (2) Determine full list of buckets from TSYS and write query for each.                                           */
/*       1/19/21 => Pri, Int, Fees                                                                                    */
/*   (3) Ask BAC - is it okay to have 1 adjustment per bucket, or should we combine?                                  */
/*       1/19/21 => Should be comined. One consumer/account level entry.                                              */
/*   (4) Ask BAC what to use for Memo Code (defaulted to NA for now)                                                  */
/*       1/19/21 => Use "CONV"                                                                                        */
/*   (5) Ask BAC what to use for location (typically AGENCY)                                                          */
/*       1/19/21 => AGENCY/ORGANIZATION is fine.                                                                      */
/*   (6) Also, do we want to use any payment tag?                                                                     */
/*       1/19/21 => Gail says "I hope not."                                                                           */
/*   (7) What to use for comment? Right now it is "TSYS Migration Reconciliation Adjustment"                          */
/*       1/19/21 => No comment should be entered.  																	*/    
/*     (8)  Modiffied the datatype length of   mu_recon.TRANSACTION_AMT field to numeric(16,6) from 				  */
/*  numeric(12,4) 										                                                            */
/* ****************************************************************************************************************** */

CREATE   PROCEDURE [etl].[sp_mu_recon]
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @pymnt_lctn_cd AS BIGINT
	DECLARE @posting_date  AS DATETIME
	DECLARE @upsrt_usr_id  AS BIGINT
	DECLARE @pymnt_memo_nm AS VARCHAR(32)
	DECLARE @pymnt_memo_id AS BIGINT
	DECLARE @crrncy_cd     AS BIGINT

	--Use 'AGENCY' for payment location code on all recon adjustments
	SELECT @pymnt_lctn_cd = pymnt_lctn_cd
	FROM   Ref_pymnt_lctn_cd
	WHERE  pymnt_lctn_val_txt = 'AGENCY'
	
	--Use this value for all dates across all tables - this'll make it easy to tell what posted from this process
	SELECT @posting_date = GETDATE()
	
	--Use 'system' for all user id columns - upsrt, crt
	SELECT @upsrt_usr_id = usr_id
	FROM   usr
	WHERE  usr_usrnm = 'system'
	
	--Payment Memo Code to be utilized for Recon transactions. Default to NA.
	SET    @pymnt_memo_nm = 'CONV'
	
	SELECT @pymnt_memo_id = pymnt_memo_id
	FROM   pymnt_memo
	WHERE  pymnt_memo_nm = @pymnt_memo_nm
	
	--Currency - default to USD
	SELECT @crrncy_cd = crrncy_cd
	FROM   Ref_crrncy_cd
	WHERE  crrncy_val_txt = 'USD'

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'mu_recon')
	BEGIN
		CREATE TABLE [ETL].[mu_recon]
		(
			-- Go thru all these, see what is needed.
			[id] [bigint] IDENTITY(1,1) NOT NULL,
			[FINANCIAL_BUCKET_NM] [nvarchar](50) NULL,
			[TRANSACTION_AMT] [numeric](16, 6) NULL, --modified the datype type from [numeric](12, 4)
			[PAYMENT_TAG_SHORT_NAME] [nvarchar](64) NULL, --Not used, but keep in case we want to use a tag for these recon transactions.
			[cnsmr_id] [bigint] NULL,
			[cnsmr_accnt_id] [bigint] NULL,
			[cnsmr_pymnt_jrnl_id] [bigint] NULL,
			[cnsmr_accnt_pymnt_jrnl_id] [bigint] NULL,
			[bckt_id] [bigint] NULL,
			[INVALID] [tinyint] NULL DEFAULT 0,
			[crdtr_id] [bigint] NULL,
			[wrkgrp_id] [bigint] NULL
		)
	END

	TRUNCATE TABLE ETL.mu_recon

	/* **************************************************************************** */
	/*  First, find balance discrepancies between TSYS and DM at the bucket level.  */
	/*  Store those to a temp table for now.                                        */
	/* **************************************************************************** */
	
	INSERT INTO ETL.mu_recon
	(
		bckt_id,
		transaction_amt,
		cnsmr_id,
		cnsmr_accnt_id,
		crdtr_id,
		wrkgrp_id
	)
	SELECT b.bckt_id,
		   ISNULL(cat.amt, 0.00) - ISNULL(stg.cnsmr_accnt_trnsctn_amnt, 0.00),
		   cao.cnsmr_id,
		   ca.cnsmr_accnt_id,
		   ca.crdtr_id,
		   c.wrkgrp_id
	FROM   dbo.dl_cnsmr_accnt_trnsctn_stgng stg
		   INNER JOIN cnsmr_accnt ca
			 ON stg.cnsmr_accnt_idntfr_lgcy_txt = ca.cnsmr_accnt_idntfr_lgcy_txt
		   INNER JOIN cnsmr_accnt_ownrs cao
		     ON ca.cnsmr_accnt_id = cao.cnsmr_accnt_id
			    AND cao.cnsmr_accnt_ownrshp_sft_dlt_flg = 'N'
				AND cao.cnsmr_accnt_ownrshp_typ_cd = 1 --Primary
		   INNER JOIN cnsmr c
		     ON cao.cnsmr_id = c.cnsmr_id
		   INNER JOIN bckt b
		     ON stg.bckt_shrt_nm = b.bckt_shrt_nm
		   LEFT JOIN
		   (
				SELECT cat.cnsmr_accnt_id,
				       cat.bckt_id,
					   SUM(cat.cnsmr_accnt_trnsctn_amnt) amt
				FROM   cnsmr_accnt_trnsctn cat
				GROUP  BY cat.cnsmr_accnt_id, bckt_id
		   ) cat ON ca.cnsmr_accnt_id = cat.cnsmr_accnt_id AND b.bckt_id = cat.bckt_id
	WHERE  stg.cnsmr_accnt_trnsctn_amnt <> ISNULL(cat.amt, 0)
  	 
	--If the account isn't found or the amounts are 0 it's not valid
	UPDATE ETL.mu_recon
	SET    INVALID = 1 
	WHERE  bckt_id is null
	       OR crdtr_id is null
		   OR cnsmr_id is null
		   OR cnsmr_accnt_id is null
		   OR ISNULL(TRANSACTION_AMT, 0) = 0

	BEGIN TRY
	BEGIN TRANSACTION
	
	--Declare & initialize variables     
	DECLARE @LastCPJ BIGINT 
	DECLARE @LastCAPJ BIGINT 
	DECLARE @AdjustBatch BIGINT 

	--Create a payment batch for the adjustments    
	INSERT INTO [dbo].[cnsmr_pymnt_btch] 
	(
		[cnsmr_pymnt_btch_assgnd_usrid], 
		[cnsmr_pymnt_btch_file_registry_id], 
		[cnsmr_pymnt_btch_crt_usrid], 
		[cnsmr_pymnt_btch_typ_cd], 
		[cnsmr_pymnt_btch_instrmnt_typ_cd], 
		[cnsmr_pymnt_btch_crt_dttm],
		[cnsmr_pymnt_btch_elctrnc_dt], 
		[cnsmr_pymnt_btch_extrnl_nm], 
		[cnsmr_pymnt_btch_extrnl_btch_cd_txt], 
		[cnsmr_pymnt_btch_extrnl_crdtr_cd_txt], 
		[cnsmr_pymnt_btch_fnncl_instrmnt_cnt], 
		[cnsmr_pymnt_btch_fnncl_instrmnt_val_amnt], 
		[cnsmr_pymnt_btch_msg_txt], 
		[cnsmr_pymnt_btch_nm], 
		[cnsmr_pymnt_btch_prcssd_dttm], 
		[cnsmr_pymnt_btch_pymnt_cnt_nmbr], 
		[cnsmr_pymnt_btch_pymnt_ttl_amnt], 
		[cnsmr_pymnt_btch_rlsd_usrid], 
		[cnsmr_pymnt_btch_rlsd_dttm], 
		[cnsmr_pymnt_btch_stts_cd], 
		[cnsmr_pymnt_epp_athrz_flg], 
		[cnsmr_pymnt_btch_lck_flg], 
		[cnsmr_pymnt_btch_imprtd_rec_cnt], 
		[cnsmr_pymnt_btch_pstng_retry_nmbr], 
		[cnsmr_pymnt_btch_auto_post_flg], 
		[upsrt_dttm], 
		[upsrt_soft_comp_id], 
		[upsrt_trnsctn_nmbr], 
		[upsrt_usr_id], 
		[cnsmr_archvd_flg], 
		[cnsmr_pymnt_btch_orgnl_pymnt_cnt_nmbr], 
		[cnsmr_pymnt_btch_orgnl_pymnt_ttl_amnt]
	) 
	VALUES
	(
		(SELECT clrk_id FROM clrk WHERE usr_id = @upsrt_usr_id),
		NULL, 
		(SELECT clrk_id FROM clrk WHERE usr_id = @upsrt_usr_id),
		3, 
		NULL, 
		@posting_date,
		NULL, 
		NULL, 
		NULL, 
		NULL, 
		NULL, 
		NULL, 
		NULL, 
		'TSYS_MgrtnRecon_' + Replace(Replace(Replace(Replace(CONVERT(VARCHAR(30), @posting_date, 126), '-', ''), 'T', ''), ':', ''), '.', ''), 
		NULL, 
		NULL, 
		NULL, 
		NULL, 
		NULL, 
		1, 
		'N', 
		'N', 
		NULL, 
		0, 
		'N', 
		@posting_date,
		19, 
		0, 
		@upsrt_usr_id,
		'N', 
		NULL, 
		NULL
	) 

	SELECT @LastCPJ = isnull(Max(cnsmr_pymnt_jrnl_id) ,0)
	FROM   cnsmr_pymnt_jrnl 

	SELECT @LastCAPJ = isnull(Max(cnsmr_accnt_pymnt_jrnl_id) ,0)
	FROM   cnsmr_accnt_pymnt_jrnl 

	SELECT @AdjustBatch = isnull(Max(cnsmr_pymnt_btch_id) ,0)
	FROM   cnsmr_pymnt_btch 

	/*  INSERT FINANCIAL RECORDS   */ 
	--  Consumer Payment Journal 
	INSERT INTO cnsmr_pymnt_jrnl 
	(
		bckt_trnsctn_typ_cd, 
		cnsmr_id, 
		cnsmr_pymnt_btch_id, 
		cnsmr_pymnt_amnt, 
		cnsmr_pymnt_cmmnt_txt, 
		cnsmr_pymnt_entrd_dt, 
		cnsmr_pymnt_is_cnvrsn_flg, 
		cnsmr_pymnt_is_rglr_flg, 
		cnsmr_pymnt_lctn_cd, 
		cnsmr_pymnt_stts_cd, 
		cnsmr_pymnt_tndrd_dt, 
		cnsmr_pymnt_rfnd_flg, 
		cnsmr_pymnt_is_nsf_flg, 
		cnsmr_pymnt_memo_cd_txt,
		pymnt_memo_id,
		crrncy_cd,
		cnsmr_pymnt_extrnl_ref_idntfr_txt,
		cnsmr_pymnt_crt_usrid,
		pymnt_orgntd_frm_sspns_flg, 
		sspns_pymnt_rvrs_on_pymnt_rvrsl_flg, 
		upsrt_dttm, 
		upsrt_soft_comp_id, 
		upsrt_trnsctn_nmbr, 
		upsrt_usr_id, 
		crtd_dttm, 
		crtd_usr_id
	) 
	SELECT DISTINCT 3, --Hard code to ADJUSTMENT
					cnsmr_id, 
					@AdjustBatch, 
					SUM(TRANSACTION_AMT),
					NULL, --Comment
					@posting_date,
					'N', 
					'Y', 
					@pymnt_lctn_cd, 
					5, 
					@posting_date,
					'N', 
					'N', 
					@pymnt_memo_nm,
					@pymnt_memo_id,
					@crrncy_cd,
					NULL, --cnsmr_pymnt_extrnl_ref_idntfr_txt
					@upsrt_usr_id,
					'N', 
					'N', 
					@posting_date,
					99, 
					0,
					@upsrt_usr_id, 
					@posting_date,
					@upsrt_usr_id
	 FROM   ETL.mu_recon
	 WHERE  cnsmr_accnt_id IS NOT NULL
	        AND INVALID = 0
	 GROUP  BY cnsmr_id
	 
	 /*  ADD CONSUMER PAY ID TO STAGING TABLE  */ 
	UPDATE stg
	SET    stg.cnsmr_pymnt_jrnl_id = cpj.cnsmr_pymnt_jrnl_id
	FROM   ETL.mu_recon stg
		   INNER JOIN cnsmr_pymnt_jrnl cpj
	         ON cpj.cnsmr_id = stg.cnsmr_id
			    AND cpj.cnsmr_pymnt_jrnl_id > @LastCPJ
					  
	--  Consumer Account Payment Journal   
	INSERT INTO cnsmr_accnt_pymnt_jrnl 
	(
		bckt_trnsctn_typ_cd, 
		cnsmr_accnt_id, 
		cnsmr_pymnt_jrnl_id, 
		crdtr_id, 
		wrkgrp_id, 
		cnsmr_accnt_pymnt_amnt, 
		cnsmr_accnt_pymnt_cmmnt_txt, 
		cnsmr_accnt_pymnt_cmssn_amnt, 
		cnsmr_accnt_pymnt_ovvrd_allctn_flg, 
		cnsmr_accnt_pymnt_is_nsf_flg, 
		cnsmr_accnt_pymnt_stts_cd, 
		cnsmr_accnt_pymnt_pstd_dt, 
		pymnt_wash_flg, 
		rcvr_cmssn_amnt, 
		cnsmr_pymnt_entrd_dt, 
		cnsmr_accnt_pymnt_systm_dstrbtn_flg, 
		upsrt_dttm, 
		upsrt_soft_comp_id, 
		upsrt_trnsctn_nmbr, 
		upsrt_usr_id, 
		crtd_dttm, 
		crtd_usr_id
	) 
	SELECT DISTINCT 3, --Hard code to ADJUSTMENT
					stg.cnsmr_accnt_id, 
					stg.cnsmr_pymnt_jrnl_id, 
					MAX(stg.crdtr_id), 
					MAX(stg.wrkgrp_id), 
					SUM(TRANSACTION_AMT),
					NULL, --Comment
					0.00, 
					'N', 
					'N', 
					5, 
					@posting_date,
					'N', 
					0, 
					@posting_date,
					'N', 
					@posting_date,
					99, 
					0,
					@upsrt_usr_id,
					@posting_date,
					@upsrt_usr_id
	 FROM   ETL.mu_recon stg
	 WHERE  cnsmr_accnt_id IS NOT NULL
 	        AND cnsmr_pymnt_jrnl_id IS NOT NULL
			AND INVALID = 0
	 GROUP  BY cnsmr_accnt_id, cnsmr_pymnt_jrnl_id
	 
	 /*  ADD CONSUMER ACCOUNT PAY ID TO STAGING TABLE  */ 
	UPDATE stg 
	SET    stg.cnsmr_accnt_pymnt_jrnl_id = capj.cnsmr_accnt_pymnt_jrnl_id 
	FROM   ETL.mu_recon stg 
		   INNER JOIN cnsmr_accnt_pymnt_jrnl capj 
				   ON capj.cnsmr_accnt_id = stg.cnsmr_accnt_id 
					  and CAPJ.cnsmr_pymnt_jrnl_id = stg.cnsmr_pymnt_jrnl_id
					  AND capj.cnsmr_accnt_pymnt_jrnl_id > @LastCAPJ 

	--  Consumer Account Transaction Table 
	INSERT INTO cnsmr_accnt_trnsctn 
	(
		bckt_id, 
		bckt_trnsctn_typ_cd, 
		cnsmr_accnt_id, 
		cnsmr_accnt_pymnt_jrnl_id, 
		cnsmr_accnt_trnsctn_stts_cd, 
		wrkgrp_id, 
		cnsmr_accnt_trnsctn_amnt, 
		cnsmr_accnt_trnsctn_cmssn_isdflt_flg, 
		cnsmr_accnt_trnsctn_cmssn_isfxd_flg,
		cnsmr_accnt_trnsctn_cmssn_amnt,
		cnsmr_accnt_trnsctn_cmssn_prcntg,
		cnsmr_accnt_trnsctn_pst_dt, 
		cnsmr_accnt_trnsctn_rcvr_cmssn_prcntg, 
		cnsmr_accnt_trnsctn_rcvr_cmssn_amnt, 
		cnsmr_accnt_trnsctn_rcvr_cmssn_isfxd_flg, 
		cnsmr_accnt_trnsctn_rcvr_cmssn_isdflt_flg, 
		cnsmr_accnt_trnsctn_lctn_cd, 
		cnsmr_accnt_trnsctn_invcd_flg, 
		cnsmr_accnt_trsnctn_entrd_dttm, 
		cnsmr_accnt_lst_dly_intrst_accmltn_trnsctn_flg, 
		crdtr_id,
		rcvr_id,
		crrncy_cd,
		upsrt_dttm, 
		upsrt_soft_comp_id, 
		upsrt_trnsctn_nmbr, 
		upsrt_usr_id, 
		crtd_dttm, 
		crtd_usr_id, 
		cnsmr_accnt_trnsctn_is_systm_adjstd_flg, 
		cnsmr_accnt_trnsctn_rcvr_is_systm_adjstd_flg,
		cnsmr_accnt_trnsctn_cntxt_cd
	) 
	SELECT  DISTINCT stg.bckt_id, 
					 3, --Hard code to ADJUSTMENT
					 cnsmr_accnt_id, 
					 cnsmr_accnt_pymnt_jrnl_id, 
					 1, 
					 wrkgrp_id, 
					 TRANSACTION_AMT * -1,
					 'N', 
					 'N',
					 0.00,
					 0.00,
					 @posting_date,
					 0.00,
					 0.00,
					 'N',
					 'N',
					 @pymnt_lctn_cd,
					 'N',
					 @posting_date,
					 'N',
					 stg.crdtr_id,
					 NULL, --rcvr_id
					 @crrncy_cd,
					 @posting_date,
					 99, 
					 0,
					 @upsrt_usr_id,
					 @posting_date,
					 @upsrt_usr_id, 
					 'N', 
					 'N',
					 NULL
	 FROM   ETL.mu_recon stg
	 WHERE  cnsmr_accnt_id IS NOT NULL
			AND INVALID = 0

	UPDATE cnsmr_accnt_wrk_actn 
	SET    cnsmr_accnt_bal_config_chng_sync_flg = 'Y' 
	FROM   cnsmr_accnt_wrk_actn caw 
		   INNER JOIN ETL.mu_recon stg
		     ON stg.cnsmr_accnt_id = caw.cnsmr_accnt_id
	WHERE stg.INVALID = 0
	
	update [cnsmr_pymnt_btch]
	set    [cnsmr_pymnt_btch_stts_cd] = (SELECT pymnt_btch_stts_cd from Ref_pymnt_btch_stts_cd where pymnt_btch_stts_val_txt = 'POSTED')
	where  cnsmr_pymnt_btch_id = @AdjustBatch
	
	COMMIT
	END TRY
	
	BEGIN CATCH
		IF  @@TRANCOUNT > 0	ROLLBACK
		
		Declare @Msg nvarchar(500) = ERROR_MESSAGE()
		Declare @Serv int = ERROR_SEVERITY()
				
		RAISERROR (@Msg, @Serv, 1)
	END CATCH	

END
------------------------------------------------------------------------------------------------------------------------
GO

