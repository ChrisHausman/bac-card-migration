use crs5_oltp;
go

-- drop existing staging table if present

if object_id('ETL.mu_bnkrptcy', 'U') is not null
    drop table etl.mu_bnkrptcy;

-- mu_bnkrptcy staging table
CREATE TABLE [ETL].[mu_bnkrptcy](
	[bnkrptcy_id] [bigint] IDENTITY(1,1) NOT NULL,
	[cnsmr_idntfr_lgcy_txt] [varchar](40) NULL,
	[agncy_entty_crrltn_id] [varchar](30) NULL,
	[cnsmr_idntfr_drvr_lcns_txt] [varchar](20) NULL,
	[bnkrptcy_attrny_addrss_city_txt] [varchar](64) NULL,
	[bnkrptcy_attrny_addrss_cnty_txt] [varchar](128) NULL,
	[cntry_val_txt] [varchar](32) NULL,
	[bnkrptcy_attrny_addrss_ln_1_txt] [varchar](128) NULL,
	[bnkrptcy_attrny_addrss_ln_2_txt] [varchar](128) NULL,
	[bnkrptcy_attrny_addrss_ln_3_txt] [varchar](128) NULL,
	[bnkrptcy_attrny_addrss_st_txt] [varchar](20) NULL,
	[bnkrptcy_attrny_addrss_pstl_cd_txt] [varchar](32) NULL,
	[bnkrptcy_attrny_nm_full_txt] [varchar](128) NULL,
	[bnkrptcy_attrny_phn_nmbr_txt] [varchar](20) NULL,
	[bnkrptcy_chptr_txt] [varchar](128) NULL,
	[bnkrptcy_court_nm] [varchar](128) NULL,
	[bnkrptcy_dschrg_dt] [datetime] NULL,
	[bnkrptcy_dsmssd_dt] [datetime] NULL,
	[bnkrptcy_dckt_nmbr_txt] [varchar](64) NULL,
	[bnkrptcy_flng_dt] [datetime] NULL,
	[bnkrptcy_jrsdctn_nm] [varchar](256) NULL,
	[bnkrptcy_prf_dt] [datetime] NULL,
	[bnkrptcy_val_txt] [varchar](32) NOT NULL,
	[bnkrptcy_intl_clm_fld_dttm] [datetime] NULL,
	[bnkrptcy_notice_rcvd_dttm] [datetime] NULL,
	[bnkrptcy_cnvrsn_dttm] [datetime] NULL,
	[bnkrptcy_cnfrmtn_dttm] [datetime] NULL,
	[bnkrptcy_crt_addrss_ln_1_txt] [varchar](128) NULL,
	[bnkrptcy_crt_addrss_ln_2_txt] [varchar](128) NULL,
	[bnkrptcy_crt_addrss_ln_3_txt] [varchar](128) NULL,
	[bnkrptcy_crt_addrss_pstl_cd_txt] [varchar](32) NULL,
	[bnkrptcy_crt_addrss_city_txt] [varchar](64) NULL,
	[bnkrptcy_crt_addrss_cnty_txt] [varchar](128) NULL,
	[bnkrptcy_crt_addrss_st_txt] [varchar](20) NULL,
	[bnkrptcy_crt_cntry_val_txt] [varchar](32) NULL,
	[bnkrptcy_crt_phn_nmbr_txt] [varchar](20) NULL,
	[bnkrptcy_crt_bar_dttm] [datetime] NULL,
	[bnkrptcy_clsd_dttm] [datetime] NULL,
	[bnkrptcy_rpn_dttm] [datetime] NULL,
	[bnkrptcy_341_mtgn_dttm] [datetime] NULL,
	[bnkrptcy_341_mtgn_lctn_txt] [varchar](50) NULL,
	[bnkrptcy_clms_ddln_dttm] [datetime] NULL,
	[bnkrptcy_cmplnt_ddln_dttm] [datetime] NULL,
	[bnkrptcy_ecoa_cd_txt] [varchar](20) NULL,
	[bnkrptcy_jdg_intls_txt] [varchar](20) NULL,
	[bnkrptcy_fnds_avlbl_fr_unscr_dbts_flg] [char](1) NOT NULL,
	[bnkrptcy_addtnl_info_apprvl_dt_fr_motion_to_lift_stay_dt_dnd_dttm] [datetime] NULL, --bai
	[upsrt_dttm] [datetime] NULL,
	[upsrt_soft_comp_id] [int] NULL,
	[upsrt_trnsctn_nmbr] [int] NULL,
	[upsrt_usr_id] [bigint] NULL
) ON [PRIMARY]

-- create the processing stored procedure

if object_id('ETL.sp_mu_bnkrptcy', 'P') is not null
    drop procedure etl.sp_mu_bnkrptcy;
go

/* ****************************************************************************************************************** */
/*   Name: Migration Utility - Bankruptcy - Processing                                                                */
/*   Staging Table: ETL.mu_bnkrptcy                                                                                   */
/*   Processing Stored Procedure: ETL.sp_mu_bnkrptcy                                                                  */
/*   Date:  04/14/2021                                                                                                */
/*   Purpose:  This script is designed to create the staging table and processing stored procedure                    */
/*             for the migration utility (MU) to migrate bankruptcy information.                                      */
/*   Change Log:                                                                                                      */
/*       Version     Description                         Developer               Date                                 */
/*       ------------------------------------------------------------------------------------------------------------ */
/*       11.1        Initial - for DM 11.1               Doug Barlett            07/07/2020                           */
/*       11.1.BAC    Updated for BAC's TSYS Migration    Chris Hausman           04/14/2021                           */
/*       11.2.BAC    Added join to cnsmr using DL#.      Chris Hausman           12/13/2021                           */
/*       11.2.BAC    Fixed bug with error logging.       Chris Hausman           04/29/2022                           */
/*       11.3.BAC    Direct to table logging.            Andrew Sheldon          04/12/2023                           */
/*       ------------------------------------------------------------------------------------------------------------ */
/*  TO DO LIST                                                                                                        */
/*  ----------                                                                                                        */
/*   (1) We need to insert a blank trustee to mimic app logic, but not petitioner - DONE                              */
/*   (2) Question for Gail - can we ever have more than 1 bankruptcy per consumer? - NO, but the code will handle     */
/* ****************************************************************************************************************** */

create   procedure etl.sp_mu_bnkrptcy @chunk_size bigint = 10000
as
    begin
        declare @err_msg nvarchar(4000)= N'';
        declare @err_severity int;
        declare @err_num int;

        declare @msg_tbl_nm nvarchar(50)= N'bnkrptcy';

        declare @start_time datetime;
        declare @run_time varchar(20);
        declare @loop_count int;
        declare @total_count bigint;
        declare @total_loops int;
        declare @start_key bigint;

        declare @default_upsrt_soft_comp_id bigint;
        declare @default_upsrt_usr_id bigint;
		
        declare @current_datetime as datetime = dbo.getcurrdt();
		
        /**************************************************************************************
            Create permanent logging table just for bankruptcy step.  
        **************************************************************************************/

        --This table will keep track of ETL progress.
        if not exists (select 1 from information_schema.tables where table_name = 'mu_Processing_Log')
        begin
            create table etl.mu_processing_log
            (
				pklog        numeric(18, 0) identity(1, 1) not null
			  , messagetype  nvarchar(50) not null
              , logmessage   nvarchar(1000) null
              , severity     int null
              , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
              , logtime      datetime not null default getdate()
            );
        end;

        /************************************************************************************
            Create local logging table.
        ************************************************************************************/

        declare @logsystem table
        (
			pklog        numeric(18, 0) not null identity(1, 1)
		  , messagetype  nvarchar(50) not null
		  , logmessage   nvarchar(1000) not null
		  , severity     int null
		  , logginglevel int not null default 0 --0 is the most severe climbing to 3 as informational messages
		  , logtime      datetime not null default getdate()
        );
		
        begin try
            select @start_time = @current_datetime;

            -- set some default values
            select @default_upsrt_soft_comp_id = upsrt_soft_comp_id
            from dbo.lkup_soft_comp
            where upsrt_soft_comp_nm = 'load';

            select @default_upsrt_usr_id = usr_id
            from dbo.usr
            where usr_usrnm = 'system';

            -- setup the looping variables to process in chucks
            set @loop_count = 1;

            select @total_count = count(*)
            from etl.mu_bnkrptcy;

            select @start_key = min(bnkrptcy_id)
            from etl.mu_bnkrptcy;

            set @total_loops = @total_count / @chunk_size + 1;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - START',
				'Starting ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

            while @loop_count > 0
            begin

				begin transaction;


				/* *******************************
						Bankruptcy Data  
				********************************* */
				
				INSERT INTO [dbo].[bnkrptcy]
				(
					[cnsmr_id]
					,[bnkrptcy_attrny_addrss_city_txt]
					,[bnkrptcy_attrny_addrss_cnty_txt]
					,[bnkrptcy_attrny_addrss_cntry_cd]
					,[bnkrptcy_attrny_addrss_ln_1_txt]
					,[bnkrptcy_attrny_addrss_ln_2_txt]
					,[bnkrptcy_attrny_addrss_ln_3_txt]
					,[bnkrptcy_attrny_addrss_st_txt]
					,[bnkrptcy_attrny_addrss_pstl_cd_txt]
					,[bnkrptcy_attrny_nm_full_txt]
					,[bnkrptcy_attrny_phn_nmbr_txt]
					,[bnkrptcy_chptr_txt]
					,[bnkrptcy_court_nm]
					,[bnkrptcy_dschrg_dt]
					,[bnkrptcy_dsmssd_dt]
					,[bnkrptcy_dckt_nmbr_txt]
					,[bnkrptcy_flng_dt]
					,[bnkrptcy_jrsdctn_nm]
					,[bnkrptcy_prf_dt]
					,[bnkrptcy_chptr_cd]
					,[bnkrptcy_intl_clm_fld_dttm]
					,[bnkrptcy_notice_rcvd_dttm]
					,[bnkrptcy_cnvrsn_dttm]
					,[bnkrptcy_cnfrmtn_dttm]
					,[bnkrptcy_crt_addrss_ln_1_txt]
					,[bnkrptcy_crt_addrss_ln_2_txt]
					,[bnkrptcy_crt_addrss_ln_3_txt]
					,[bnkrptcy_crt_addrss_pstl_cd_txt]
					,[bnkrptcy_crt_addrss_city_txt]
					,[bnkrptcy_crt_addrss_cnty_txt]
					,[bnkrptcy_crt_addrss_st_txt]
					,[bnkrptcy_crt_cntry_cd]
					,[bnkrptcy_crt_phn_nmbr_txt]
					,[bnkrptcy_crt_bar_dttm]
					,[bnkrptcy_clsd_dttm]
					,[bnkrptcy_rpn_dttm]
					,[bnkrptcy_341_mtgn_dttm]
					,[bnkrptcy_341_mtgn_lctn_txt]
					,[bnkrptcy_clms_ddln_dttm]
					,[bnkrptcy_cmplnt_ddln_dttm]
					,[bnkrptcy_ecoa_cd_txt]
					,[bnkrptcy_jdg_intls_txt]
					,[bnkrptcy_fnds_avlbl_fr_unscr_dbts_flg]
					,[upsrt_dttm]
					,[upsrt_soft_comp_id]
					,[upsrt_trnsctn_nmbr]
					,[upsrt_usr_id]
					,[bnkrptcy_stgng_idntfr_nmbr]
				)
				SELECT c.cnsmr_id AS cnsmr_id --<cnsmr_id, bigint,>
					   ,stg.bnkrptcy_attrny_addrss_city_txt --<bnkrptcy_attrny_addrss_city_txt, varchar(64),>
					   ,stg.bnkrptcy_attrny_addrss_cnty_txt --<bnkrptcy_attrny_addrss_cnty_txt, varchar(128),>
					   ,rcc1.cntry_cd AS bnkrptcy_attrny_addrss_cntry_cd --<bnkrptcy_attrny_addrss_cntry_cd, smallint,>
					   ,stg.bnkrptcy_attrny_addrss_ln_1_txt --<bnkrptcy_attrny_addrss_ln_1_txt, varchar(128),>
					   ,stg.bnkrptcy_attrny_addrss_ln_2_txt --<bnkrptcy_attrny_addrss_ln_2_txt, varchar(128),>
					   ,stg.bnkrptcy_attrny_addrss_ln_3_txt --<bnkrptcy_attrny_addrss_ln_3_txt, varchar(128),>
					   ,stg.bnkrptcy_attrny_addrss_st_txt --<bnkrptcy_attrny_addrss_st_txt, varchar(20),>
					   ,stg.bnkrptcy_attrny_addrss_pstl_cd_txt --<bnkrptcy_attrny_addrss_pstl_cd_txt, varchar(32),>
					   ,stg.bnkrptcy_attrny_nm_full_txt --<bnkrptcy_attrny_nm_full_txt, varchar(128),>
					   ,stg.bnkrptcy_attrny_phn_nmbr_txt --<bnkrptcy_attrny_phn_nmbr_txt, varchar(20),>
					   ,stg.bnkrptcy_chptr_txt --<bnkrptcy_chptr_txt, varchar(128),>
					   ,stg.bnkrptcy_court_nm --<bnkrptcy_court_nm, varchar(128),>
					   ,stg.bnkrptcy_dschrg_dt --<bnkrptcy_dschrg_dt, datetime,>
					   ,stg.bnkrptcy_dsmssd_dt --<bnkrptcy_dsmssd_dt, datetime,>
					   ,stg.bnkrptcy_dckt_nmbr_txt --<bnkrptcy_dckt_nmbr_txt, varchar(64),>
					   ,stg.bnkrptcy_flng_dt --<bnkrptcy_flng_dt, datetime,>
					   ,stg.bnkrptcy_jrsdctn_nm --<bnkrptcy_jrsdctn_nm, varchar(256),>
					   ,stg.bnkrptcy_prf_dt --<bnkrptcy_prf_dt, datetime,>
					   ,rbcc.bnkrptcy_chptr_cd AS bnkrptcy_chptr_cd --<bnkrptcy_chptr_cd, smallint,>
					   ,stg.bnkrptcy_intl_clm_fld_dttm --<bnkrptcy_intl_clm_fld_dttm, datetime,>
					   ,stg.bnkrptcy_notice_rcvd_dttm --<bnkrptcy_notice_rcvd_dttm, datetime,>
					   ,stg.bnkrptcy_cnvrsn_dttm --<bnkrptcy_cnvrsn_dttm, datetime,>
					   ,stg.bnkrptcy_cnfrmtn_dttm --<bnkrptcy_cnfrmtn_dttm, datetime,>
					   ,stg.bnkrptcy_crt_addrss_ln_1_txt --<bnkrptcy_crt_addrss_ln_1_txt, varchar(128),>
					   ,stg.bnkrptcy_crt_addrss_ln_2_txt --<bnkrptcy_crt_addrss_ln_2_txt, varchar(128),>
					   ,stg.bnkrptcy_crt_addrss_ln_3_txt --<bnkrptcy_crt_addrss_ln_3_txt, varchar(128),>
					   ,stg.bnkrptcy_crt_addrss_pstl_cd_txt --<bnkrptcy_crt_addrss_pstl_cd_txt, varchar(32),>
					   ,stg.bnkrptcy_crt_addrss_city_txt --<bnkrptcy_crt_addrss_city_txt, varchar(64),>
					   ,stg.bnkrptcy_crt_addrss_cnty_txt --<bnkrptcy_crt_addrss_cnty_txt, varchar(128),>
					   ,stg.bnkrptcy_crt_addrss_st_txt --<bnkrptcy_crt_addrss_st_txt, varchar(20),>
					   ,rcc2.cntry_cd AS bnkrptcy_crt_cntry_cd --<bnkrptcy_crt_cntry_cd, smallint,>
					   ,stg.bnkrptcy_crt_phn_nmbr_txt --<bnkrptcy_crt_phn_nmbr_txt, varchar(20),>
					   ,stg.bnkrptcy_crt_bar_dttm --<bnkrptcy_crt_bar_dttm, datetime,>
					   ,stg.bnkrptcy_clsd_dttm --<bnkrptcy_clsd_dttm, datetime,>
					   ,stg.bnkrptcy_rpn_dttm --<bnkrptcy_rpn_dttm, datetime,>
					   ,stg.bnkrptcy_341_mtgn_dttm --<bnkrptcy_341_mtgn_dttm, datetime,>
					   ,stg.bnkrptcy_341_mtgn_lctn_txt --<bnkrptcy_341_mtgn_lctn_txt, varchar(50),>
					   ,stg.bnkrptcy_clms_ddln_dttm--<bnkrptcy_clms_ddln_dttm, datetime,>
					   ,stg.bnkrptcy_cmplnt_ddln_dttm --<bnkrptcy_cmplnt_ddln_dttm, datetime,>
					   ,stg.bnkrptcy_ecoa_cd_txt --<bnkrptcy_ecoa_cd_txt, varchar(20),>
					   ,stg.bnkrptcy_jdg_intls_txt --<bnkrptcy_jdg_intls_txt, varchar(20),>
					   ,stg.bnkrptcy_fnds_avlbl_fr_unscr_dbts_flg --<bnkrptcy_fnds_avlbl_fr_unscr_dbts_flg, char(1),>
					   ,GETDATE() AS upsrt_dttm --<upsrt_dttm, datetime,>
					   ,@Default_upsrt_soft_comp_id AS upsrt_soft_comp_id --<upsrt_soft_comp_id, int,>
					   ,1 AS upsrt_trnsctn_nmbr --<upsrt_trnsctn_nmbr, int,>
					   ,@Default_upsrt_usr_id AS upsrt_usr_id --<upsrt_usr_id, bigint,>
					   ,stg.bnkrptcy_id --<bnkrptcy_stgng_idntfr_nmbr, bigint,>
				FROM   ETL.mu_bnkrptcy stg
					   INNER JOIN [dbo].[cnsmr] c
						 ON c.agncy_entty_crrltn_id = stg.agncy_entty_crrltn_id --CRK
						    AND c.cnsmr_idntfr_drvr_lcns_txt = stg.cnsmr_idntfr_drvr_lcns_txt --Surrogate Key
					   LEFT JOIN [dbo].[Ref_cntry_cd] rcc1
						 ON rcc1.cntry_val_txt = stg.cntry_val_txt
					   LEFT JOIN [dbo].[ref_bnkrptcy_chptr_cd] rbcc
						 ON rbcc.bnkrptcy_val_txt = stg.bnkrptcy_val_txt
					   LEFT JOIN [dbo].[Ref_cntry_cd] rcc2
						 ON rcc2.cntry_val_txt = stg.bnkrptcy_crt_cntry_val_txt
				WHERE  stg.bnkrptcy_id >= @start_key
					   AND stg.bnkrptcy_id < @start_key + @chunk_size;
									
				INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
				VALUES
				(
					@msg_tbl_nm,
					'Completed insert into bnkrptcy, ' + convert(nvarchar, @@rowcount) + ' rows inserted.  Loop: ' + convert(nvarchar, @loop_count),
					3
				)
									
				/* *******************************
						Bankruptcy Additional Info Data  
				********************************* */
				
				INSERT INTO [dbo].[bnkrptcy_addtnl_info]
				(
					[bnkrptcy_id]
					,[bnkrptcy_addtnl_info_apprvl_dt_fr_motion_to_lift_stay_dt_dnd_dttm]
					,[upsrt_dttm]
					,[upsrt_soft_comp_id]
					,[upsrt_trnsctn_nmbr]
					,[upsrt_usr_id]
					,[crtd_usr_id]
					,[crtd_dttm]
				)
				SELECT b.bnkrptcy_id AS bnkrptcy_id --<bnkrptcy_id, bigint,>
					   ,stg.bnkrptcy_addtnl_info_apprvl_dt_fr_motion_to_lift_stay_dt_dnd_dttm --<bnkrptcy_addtnl_info_apprvl_dt_fr_motion_to_lift_stay_dt_dnd_dttm, datetime,>
					   ,GETDATE() AS upsrt_dttm --<upsrt_dttm, datetime,>
					   ,@Default_upsrt_soft_comp_id AS upsrt_soft_comp_id --<upsrt_soft_comp_id, int,>
					   ,1 AS upsrt_trnsctn_nmbr --<upsrt_trnsctn_nmbr, int,>
					   ,@Default_upsrt_usr_id AS upsrt_usr_id --<upsrt_usr_id, bigint,>
					   ,@Default_upsrt_usr_id AS crtd_usr_id --<crtd_usr_id, bigint,>
					   ,GETDATE() As crtd_dttm --<crtd_dttm, datetime,>                                        
				FROM   ETL.mu_bnkrptcy stg
				       INNER JOIN [dbo].[cnsmr] c
						 ON c.agncy_entty_crrltn_id = stg.agncy_entty_crrltn_id --CRK
						    AND c.cnsmr_idntfr_drvr_lcns_txt = stg.cnsmr_idntfr_drvr_lcns_txt --Surrogate Key
				       INNER JOIN [dbo].[bnkrptcy] b
					     ON b.cnsmr_id = c.cnsmr_id
						    AND stg.bnkrptcy_id = b.bnkrptcy_stgng_idntfr_nmbr --Not needed if only 1 per consumer
				       LEFT JOIN [dbo].[ref_bnkrptcy_chptr_cd] rbcc
					     ON rbcc.bnkrptcy_val_txt = stg.bnkrptcy_val_txt
				WHERE  stg.bnkrptcy_id >= @start_key
					   AND stg.bnkrptcy_id < @start_key + @chunk_size;
			   
				INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
				VALUES
				(
					@msg_tbl_nm,
					'Completed insert into bai, ' + convert(nvarchar, @@rowcount) + ' rows inserted.  Loop: ' + convert(nvarchar, @loop_count),
					3
				)
									
				/* *******************************
						Bankruptcy Trustee Data  
				********************************* */
				
				--Just insert an empty record to mimic core app functionality
				INSERT INTO [dbo].[bnkrptcy_trustee]
				(
					[bnkrptcy_id]
					,[bnkrptcy_trustee_nm]
					,[bnkrptcy_trustee_addrss_cntry_cd]
					,[bnkrptcy_trustee_phn_nmbr_txt]
					,[upsrt_dttm]
					,[upsrt_soft_comp_id]
					,[upsrt_trnsctn_nmbr]
					,[upsrt_usr_id]
					,[crtd_dttm]
					,[crtd_usr_id]
				)
				SELECT  b.bnkrptcy_id AS bnkrptcy_id --<bnkrptcy_id, bigint,>
						,'' --<bnkrptcy_trustee_nm, varchar(256),>
						,231 --<bnkrptcy_trustee_addrss_cntry_cd, smallint,>
						,'' --<bnkrptcy_trustee_phn_nmbr_txt, varchar(20),>
						,GETDATE() AS upsrt_dttm --<upsrt_dttm, datetime,>
						,@Default_upsrt_soft_comp_id AS upsrt_soft_comp_id --<upsrt_soft_comp_id, int,>
						,1 AS upsrt_trnsctn_nmbr --<upsrt_trnsctn_nmbr, int,>
						,@Default_upsrt_usr_id AS upsrt_usr_id --<upsrt_usr_id, bigint,>
						,GETDATE() As crtd_dttm --<crtd_dttm, datetime,>
						,@Default_upsrt_usr_id AS crtd_usr_id --<crtd_usr_id, bigint,>
				FROM    ETL.mu_bnkrptcy stg
						INNER JOIN [dbo].[cnsmr] c
						  ON c.agncy_entty_crrltn_id = stg.agncy_entty_crrltn_id --CRK
						     AND c.cnsmr_idntfr_drvr_lcns_txt = stg.cnsmr_idntfr_drvr_lcns_txt --Surrogate Key
						INNER JOIN [dbo].[bnkrptcy] b
						  ON b.cnsmr_id = c.cnsmr_id
						     AND stg.bnkrptcy_id = b.bnkrptcy_stgng_idntfr_nmbr --Not needed if only 1 per consumer
						LEFT JOIN [dbo].[Ref_cntry_cd] rcc
						  ON rcc.cntry_val_txt = stg.cntry_val_txt
				WHERE  stg.bnkrptcy_id >= @start_key
					   AND stg.bnkrptcy_id < @start_key + @chunk_size;
			   
				INSERT INTO etl.mu_processing_log (MessageType, LogMessage, LoggingLevel)
				VALUES
				(
					@msg_tbl_nm,
					'Completed insert into bnkrptcy_trustee, ' + convert(nvarchar, @@rowcount) + ' rows inserted.  Loop: ' + convert(nvarchar, @loop_count),
					3
				)

                -- advance the start_key for the next set of records in next loop
                set @start_key = @start_key + @chunk_size;

                -- advance the loop
                if @loop_count < @total_loops
                    set @loop_count = @loop_count + 1;
                else
                    set @loop_count = 0;

                if @@trancount > 0
                    commit transaction;
            end;

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - FINISHED',
				'Completed ' + @msg_tbl_nm + ' Loading of '
				 + convert(nvarchar, @total_count) + ' records, using '
				 + convert(nvarchar, @total_loops) + ' loops.',
				3
			);

			set @run_time = convert(nvarchar, dateadd(ms, datediff(second, @start_time, dbo.getcurrdt()) * 1000, 0), 114);

            insert into etl.mu_processing_log (messagetype, logmessage, logginglevel)
            values
			(
				@msg_tbl_nm + ' - RUN TIME',
				@msg_tbl_nm + ' Total Run Time: ' + ISNULL(@run_time, ''),
				3
			);

        end try

        begin catch
            if @@trancount > 0
                rollback;

            select @err_msg = error_message()
                 , @err_severity = error_severity()
                 , @err_num = error_number();

            insert into etl.mu_processing_log (messagetype, logmessage, severity)
            values('ERROR', cast(@err_num as varchar) + ' ' + @err_msg, @err_severity);

            insert into etl.mu_processing_log(messagetype, logmessage, logginglevel)
            values('ERROR', 'Bankruptcy Rolled Back.', 0);

            raiserror(@err_msg, @err_severity, 16);
        end catch;

        /***********************************************************
            Dump the logging to a permanent table.  
        ***********************************************************/

--        insert into etl.mu_processing_log(messagetype, logmessage, severity, logginglevel, logtime)
--        select messagetype, logmessage, severity, logginglevel, logtime
--        from @logsystem;
		
    end;
GO

