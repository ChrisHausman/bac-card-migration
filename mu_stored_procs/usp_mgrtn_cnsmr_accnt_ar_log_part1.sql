/****** Object: Procedure [etl].[usp_mgrtn_cnsmr_accnt_ar_log_part1]   Script Date: 12/15/2021 3:54:09 PM ******/
USE [crs5_oltp];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
CREATE PROCEDURE [etl].[usp_mgrtn_cnsmr_accnt_ar_log_part1]
AS
BEGIN

DECLARE @loop_count as integer
DECLARE @total_count as integer
DECLARE @total_loops as integer 
DECLARE @errNum int
DECLARE @errMsg varchar(1024)
DECLARE @StatusMsg as varchar(2500)

/* **************** */
/*  DEFAULT VALUES  */
/* **************** */

DECLARE @actn_cd AS BIGINT
DECLARE @usr_id AS BIGINT
DECLARE @Default_upsrt_soft_comp_id as bigint
DECLARE @Default_upsrt_usr_id as bigint

SELECT @actn_cd = actn_cd
FROM   actn_cd
WHERE  actn_cd_shrt_val_txt = 'CNVRSN'

SELECT @usr_id = usr_id
FROM   usr
WHERE  usr_usrnm = 'CNVRSN'

SELECT @Default_upsrt_soft_comp_id = upsrt_soft_comp_id
FROM   lkup_soft_comp
WHERE  upsrt_soft_comp_nm = 'load'

SELECT @Default_upsrt_usr_id = usr_id
FROM   usr
WHERE  usr_usrnm = 'system'

-- AR Events (Part 1 - Ones)

SET @loop_count = 1
SELECT @total_count = count(*) FROM ETL.mgrtn_cnsmr_accnt_ar_log

SELECT @total_loops = max(grp_nmbr) FROM ETL.mgrtn_cnsmr_accnt_ar_log


set @StatusMsg =  N'Number of passes (mgrtn_cnsmr_accnt_ar_log) : ' + convert(varchar,@total_loops)
PRINT @StatusMsg
EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Status', 'DL Load AR Log Part 1', @StatusMsg
 
WHILE @loop_count > 0
BEGIN
	BEGIN TRY
		BEGIN TRAN
			set @StatusMsg =  N'Pass ' + convert(varchar,@loop_count) + ' of ' + convert(varchar,@total_loops) + ' at ' + convert(varchar, getdate(), 113)
			PRINT @StatusMsg
			EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Status', 'DL Load AR Log Part 1', @StatusMsg


			INSERT INTO cnsmr_accnt_ar_log
			(
				actn_cd,
				rslt_cd,
				cnsmr_id,
				cnsmr_accnt_id,
				cnsmr_accnt_ar_mssg_txt,
				cnsmr_accnt_ar_log_crt_usr_id,
				upsrt_dttm,
				upsrt_soft_comp_id,
				upsrt_trnsctn_nmbr,
				upsrt_usr_id
			)
			SELECT @actn_cd,
			       rc.rslt_cd,
			       ca.cnsmr_id,
			       ca.cnsmr_accnt_id,
			       stg.cnsmr_accnt_ar_mssg_txt,
			       @usr_id,
			       stg.upsrt_dttm,
			       @Default_upsrt_soft_comp_id,
			       0,
			       @Default_upsrt_usr_id
			FROM   ETL.mgrtn_cnsmr_accnt_ar_log stg
			       INNER JOIN rslt_cd rc
			         ON stg.rslt_cd_val_txt = rc.rslt_cd_val_txt
			       INNER JOIN cnsmr_accnt ca
			         ON stg.cnsmr_accnt_idntfr_lgcy_txt = ca.[cnsmr_accnt_idntfr_lgcy_txt]
			WHERE  stg.grp_nmbr = @loop_count
		
			IF @loop_count < @total_loops
			BEGIN
				SET @loop_count = @loop_count + 8
			END
			ELSE
			BEGIN
				SET @loop_count = 0
			END
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH
		SELECT @errNum = error_number()
		SELECT @errMsg = convert(varchar,@errNum) + '-' + error_message()
		SELECT error_number() ErrorNBR, error_severity() Severity,
			error_line() ErrorLine, error_message() Msg
		PRINT 'Error !!! - ' + @errMsg
		ROLLBACK TRAN
		SET rowcount 0
		EXEC [DebtMgr_SuppData].[dbo].[INS_CNVRSN_LOG] 'Task Error', 'DL Load AR Log Part 1', @errMsg
	END CATCH

END

END

GO

