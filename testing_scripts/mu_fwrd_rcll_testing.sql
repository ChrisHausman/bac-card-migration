create table etl.mu_cnsmr_accnt_frwrd_rcll_dtl
( 
	cnsmr_accnt_frwrd_rcll_dtl_id     bigint identity(1, 1) not null
    , rcvr_shrt_nm                    varchar(8) not null
    , cnsmr_accnt_idntfr_lgcy_txt     varchar(20) not null
    , frwrd_rcll_mode_val_txt         varchar(32) not null
    , frwrd_rcll_stts_val_txt         varchar(32) not null
    , cnsmr_accnt_sngl_frwrd_rqst_flg char(1) not null
    , cnsmr_accnt_frwrd_rqst_dttm     datetime not null
    , cnsmr_accnt_frwrd_dttm          datetime null
    , cnsmr_accnt_frwrd_bal_amnt      decimal(16, 6) not null
    , cnsmr_accnt_frwrd_btch_id       bigint null
    , cnsmr_accnt_sngl_rcll_rqst_flg  char(1) not null
    , cnsmr_accnt_rcll_rqst_dttm      datetime null
    , cnsmr_accnt_rcll_dttm           datetime null
    , cnsmr_accnt_rcll_bal_amnt       decimal(16, 6) null
    , cnsmr_accnt_rcll_btch_id        bigint null
    , cnsmr_accnt_expctd_rcll_dttm    datetime null
    , cnsmr_accnt_frwd_hrd_rcll_flg   char(1) not null
    , upsrt_dttm                      datetime null
    , upsrt_soft_comp_id              int null
    , upsrt_trnsctn_nmbr              int null
    , upsrt_usr_id                    bigint null
    , crtd_usr_id                     bigint null
    , crtd_dttm                       datetime null
);
go

	select * from rcvr

truncate table etl.mu_cnsmr_accnt_frwrd_rcll_dtl

select * from etl.mu_cnsmr_accnt_frwrd_rcll_dtl

insert into etl.mu_cnsmr_accnt_frwrd_rcll_dtl
(
	rcvr_shrt_nm, cnsmr_accnt_idntfr_lgcy_txt, frwrd_rcll_mode_val_txt, frwrd_rcll_stts_val_txt, cnsmr_accnt_sngl_frwrd_rqst_flg,
	cnsmr_accnt_frwrd_rqst_dttm, cnsmr_accnt_frwrd_dttm, cnsmr_accnt_frwrd_bal_amnt, cnsmr_accnt_sngl_rcll_rqst_flg,
	cnsmr_accnt_frwd_hrd_rcll_flg, cnsmr_accnt_rcll_rqst_dttm, cnsmr_accnt_rcll_dttm, cnsmr_accnt_rcll_bal_amnt
)
select 'Rcvr4', '1015193', 'RECALL', 'COMPLETED', 'Y', getdate() - 82, getdate() - 81, '123.23', 'N', 'N', getdate() - 80, getdate() - 79, '42.42'

union all

select 'Rcvr4', '1015193', 'RECALL', 'COMPLETED', 'Y', getdate() - 91, getdate() - 90, '123.23', 'N', 'N', getdate() - 89, getdate() - 88, '42.42'

union all
select 'Rcvr4', '1015193', 'RECALL', 'COMPLETED', 'Y', getdate() - 44, getdate() - 43, '123.23', 'N', 'N', getdate() - 42, getdate() - 41, '42.42'

union all
select 'Rcvr4', '1015193', 'RECALL', 'COMPLETED', 'Y', getdate() - 13, getdate() - 12, '123.23', 'N', 'N', getdate() - 11, getdate() - 10, '42.42'

union all
select 'Rcvr4', '1015193', 'RECALL', 'COMPLETED', 'Y', getdate() - 99, getdate() - 98, '123.23', 'N', 'N', getdate() - 97, getdate() - 96, '42.42'

union all
select 'Rcvr4', '1015193', 'RECALL', 'COMPLETED', 'Y', getdate() - 73, getdate() - 72, '123.23', 'N', 'N', getdate() - 71, getdate() - 70, '42.42'

exec etl.sp_mu_frwrd_rcll @chunk_size = 2000

select * from etl.mu_cnsmr_accnt_frwrd_rcll_dtl

delete from cnsmr_accnt_frwrd_rcll_dtl where upsrt_dttm > '2022-01-01'

select * from cnsmr_accnt_frwrd_rcll_dtl order by upsrt_dttm desc

select cnsmr_accnt_is_frwrd_flg from cnsmr_accnt

select * from cnsmr_accnt_rcll_btch_log

update cnsmr_accnt set cnsmr_accnt_idntfr_lgcy_txt = cnsmr_accnt_idntfr_agncy_id where cnsmr_accnt_idntfr_lgcy_txt is null

insert

select * from ref_frwrd_rcll_stts_cd
select frwrd_rcll_stts_cd from ref_frwrd_rcll_stts_cd where frwrd_rcll_stts_val_txt = 'COMPLETED'

select * from cnsmr_accnt_frwrd_rcll_dtl order by upsrt_dttm desc
select * from cnsmr_accnt_frwrd_btch order by upsrt_dttm desc
select * from cnsmr_accnt_tag order by upsrt_dttm desc
select * from cnsmr_accnt_frwrd_rcll

Delete cat
from   cnsmr_accnt_tag cat
       inner join cnsmr_accnt_frwrd_rcll_dtl cafrd
	     on cat.cnsmr_accnt_id = cafrd.cnsmr_accnt_id
		    and cafrd.cnsmr_accnt_frwrd_btch_id = 197
where  cat.tag_id = (select tag_id from tag where tag_shrt_nm = 'Forward')

update cawa
set    cawa.cnsmr_accnt_lst_frwrd_rcll_dtl_id = null
--select *
from   cnsmr_accnt_wrk_actn cawa
       inner join cnsmr_accnt_frwrd_rcll_dtl cafrd
	     on cawa.cnsmr_accnt_lst_frwrd_rcll_dtl_id = cafrd.cnsmr_accnt_frwrd_rcll_dtl_id
		    and cafrd.cnsmr_accnt_frwrd_btch_id = 197

Delete caefs
from   cnsmr_accnt_effctv_fee_schdl caefs
       inner join cnsmr_accnt_frwrd_rcll_dtl cafrd
	     on caefs.cnsmr_accnt_id = cafrd.cnsmr_accnt_id
		    and cafrd.cnsmr_accnt_frwrd_btch_id = 197

delete from cnsmr_accnt_frwrd_rcll_dtl where cnsmr_accnt_frwrd_btch_id = 197
delete from cnsmr_accnt_frwrd_btch where cnsmr_accnt_frwrd_btch_id = 197
delete from cnsmr_accnt_rcll_btch_log where cnsmr_accnt_rcll_btch_id in (118)
delete from cnsmr_accnt_rcll_btch_prttn where cnsmr_accnt_rcll_btch_id in (118)
delete from cnsmr_accnt_rcll_btch where cnsmr_accnt_rcll_btch_id in (118)

select *
from   cnsmr_accnt ca
where  ca.cnsmr_accnt_idntfr_agncy_id = 1018699

select * from cnsmr_accnt_effctv_fee_schdl where cnsmr_accnt_id = 7941

INSERT INTO cnsmr_accnt_effctv_fee_schdl
(
	cnsmr_accnt_id,
	fee_schdl_id,
	crdtr_id,
	rcvr_id,
	cnsmr_accnt_fee_schdl_effctv_dttm,
	cnsmr_accnt_fee_schdl_end_dttm,
	upsrt_dttm,
	upsrt_soft_comp_id,
	upsrt_trnsctn_nmbr,
	upsrt_usr_id,
	crtd_usr_id,
	crtd_dttm
)

select rcvr_fee_schdld_id
from   rcvr r
where  r.rcvr_shrt_nm = 'Rcvr4'

select * from fee_schdl where fee_schdl_id = 53

select * from cnsmr_accnt_wrk_actn where cnsmr_accnt_id = 10424

select * from etl.mu_processing_log

            select cafrd.cnsmr_accnt_frwrd_rcll_dtl_id
                   ,cafrd.cnsmr_accnt_id
                   ,cafrd.cnsmr_accnt_frwrd_rcll_mode_cd
                   ,row_number() over (partition by cafrd.cnsmr_accnt_id order by cafrd.cnsmr_accnt_frwrd_dttm desc) rnk
            --into   #lst_id
            from   dbo.cnsmr_accnt_frwrd_rcll_dtl cafrd
            where  cafrd.cnsmr_accnt_frwrd_rcll_stts_cd = 3; -- 'COMPLETED'