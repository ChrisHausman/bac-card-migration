INSERT INTO ETL.mu_bnkrptcy
(
	agncy_entty_crrltn_id,
	[bnkrptcy_chptr_txt],
	[bnkrptcy_val_txt],
	[bnkrptcy_flng_dt],
	[bnkrptcy_court_nm],
	[bnkrptcy_jrsdctn_nm],
	[bnkrptcy_prf_dt],
	[bnkrptcy_intl_clm_fld_dttm],
	[bnkrptcy_attrny_nm_full_txt],
	[bnkrptcy_fnds_avlbl_fr_unscr_dbts_flg],
	[bnkrptcy_addtnl_info_apprvl_dt_fr_motion_to_lift_stay_dt_dnd_dttm]
)
select agncy_entty_crrltn_id,
	   '7',
	   '7',
	   getdate() - 2000,
	   'CIVIC COURT OF HONDA',
	   'WINTERFELL',
	   getdate() - 1000,
	   getdate() - 1500,
	   'BOB LOBLAW',
	   'Y',
	   getdate() - 42
from   cnsmr


select agncy_entty_crrltn_id from cnsmr
update cnsmr set agncy_entty_crrltn_id = cnsmr_idntfr_agncy_id

select 

--66
select * from bnkrptcy 

--2
select * from bnkrptcy_addtnl_info

--2
select * from bnkrptcy_trustee

--12,917
select * from etl.mu_bnkrptcy

exec etl.sp_mu_bnkrptcy @chunk_size=172

select * from etl.mu_processing_log

delete from bnkrptcy
delete from bnkrptcy_addtnl_info
delete from bnkrptcy_trustee