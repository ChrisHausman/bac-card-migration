truncate table etl.mu_UDEFCNSMR_DETAILS
truncate table etl.mu_UDEFTSYS_CC_STS_RSN_CDS
truncate table etl.mu_UDEFDECEASEDPROBATE
truncate table etl.mu_UDEFDECEASEDPROBATE_hst
truncate table etl.mu_UDEFACCT_DETAILS
truncate table etl.mu_UDEFCBR_ACCOUNT
truncate table etl.mu_UDEFLEGAL_COST_TRACKING
truncate table etl.mu_UDEFLEGAL_COST_TRACKING_hst
truncate table etl.mu_UDEFLEGAL_JUDGMENT
truncate table etl.mu_UDEFLEGAL_JUDGMENT_hst
truncate table etl.mu_UDEFFOD1099C
truncate table etl.mu_UDEFSTRTGY_FIELDS
truncate table etl.mu_UDEFDEBT_SETTLEMENT_AGENCY

truncate table etl.mu_processing_log

truncate table UDEFCNSMR_DETAILS
truncate table UDEFTSYS_CC_STS_RSN_CDS
truncate table UDEFDECEASEDPROBATE
truncate table UDEFDECEASEDPROBATE_hst
truncate table UDEFESTT_CNTCT_INF
truncate table UDEFLEGAL_JUDGMENT
truncate table UDEFSCORE_STRATEGY
truncate table UDEFACCT_DETAILS
truncate table UDEFCBR_ACCOUNT
truncate table UDEFLEGAL_COST_TRACKING
truncate table UDEFLEGAL_COST_TRACKING_hst
truncate table UDEFFOD1099C
truncate table UDEFSTRTGY_FIELDS
truncate table UDEFDEBT_SETTLEMENT_AGENCY


insert into ETL.mu_UDEFCNSMR_DETAILS
select top (10000)
       c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id
from   cnsmr c
where  c.agncy_entty_crrltn_id is not null

insert into etl.mu_UDEFTSYS_CC_STS_RSN_CDS
select top (10000)
       cnsmr_accnt_idntfr_lgcy_txt,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'ab' else 'cd' end,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'Y' else 'N' end,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'AP' else 'BK' end
from   cnsmr_accnt ca
where  ca.cnsmr_accnt_idntfr_lgcy_txt is not null

select * from etl.mu_UDEFDECEASEDPROBATE
insert into ETL.mu_UDEFDECEASEDPROBATE
select top (10000)
       c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   GETDATE() - c.cnsmr_id % 99, --AMND_APPRVD_DT
	   GETDATE() - c.cnsmr_id % 89,
	   GETDATE() - c.cnsmr_id % 79,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   'US',
	   c.agncy_entty_crrltn_id,
	   case when c.cnsmr_id % 2 = 0 then 'Y' else 'N' end, --FIS_NOTERIZED
	   c.agncy_entty_crrltn_id,
	   GETDATE() - c.cnsmr_id % 69,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   GETDATE() - c.cnsmr_id % 59, --DEATH_DATE
	   c.agncy_entty_crrltn_id,
	   GETDATE() - c.cnsmr_id % 49, --satisfaction
	   'phnex',
	   GETDATE() - c.cnsmr_id % 39,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   case when c.cnsmr_id % 2 = 0 then 'Y' else 'N' end, --SND_TO_REP
	   case when c.cnsmr_id % 2 = 0 then 'Y' else 'N' end, --IS_AMNDMNT
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   GETDATE() - c.cnsmr_id % 29,
	   case when c.cnsmr_id % 2 = 0 then 'Y' else 'N' end, --SND_TO_ATTY
	   case when c.cnsmr_id % 2 = 0 then 'Y' else 'N' end, --SND_TO_CRT
	   c.agncy_entty_crrltn_id,
	   42.42,
	   c.agncy_entty_crrltn_id,
	   '1231231234'
from   cnsmr c
where  c.agncy_entty_crrltn_id is not null

insert into ETL.mu_UDEFDECEASEDPROBATE_hst
select top (10000)
       c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   GETDATE() - c.cnsmr_id % 99, --AMND_APPRVD_DT
	   GETDATE() - c.cnsmr_id % 89,
	   GETDATE() - c.cnsmr_id % 79,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   'US',
	   c.agncy_entty_crrltn_id,
	   case when c.cnsmr_id % 2 = 0 then 'Y' else 'N' end, --FIS_NOTERIZED
	   c.agncy_entty_crrltn_id,
	   GETDATE() - c.cnsmr_id % 69,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   GETDATE() - c.cnsmr_id % 59, --DEATH_DATE
	   c.agncy_entty_crrltn_id,
	   GETDATE() - c.cnsmr_id % 49, --satisfaction
	   'phnex',
	   GETDATE() - c.cnsmr_id % 39,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   case when c.cnsmr_id % 2 = 0 then 'Y' else 'N' end, --SND_TO_REP
	   case when c.cnsmr_id % 2 = 0 then 'Y' else 'N' end, --IS_AMNDMNT
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   GETDATE() - c.cnsmr_id % 29,
	   case when c.cnsmr_id % 2 = 0 then 'Y' else 'N' end, --SND_TO_ATTY
	   case when c.cnsmr_id % 2 = 0 then 'Y' else 'N' end, --SND_TO_CRT
	   c.agncy_entty_crrltn_id,
	   42.42,
	   c.agncy_entty_crrltn_id,
	   '1231231234',
	   GETDATE() - c.cnsmr_id % 42
from   cnsmr c
where  c.agncy_entty_crrltn_id is not null


insert into ETL.mu_UDEFESTT_CNTCT_INF
select top (10000)
       c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   'US',
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   'PA',
	   case when c.cnsmr_id % 2 = 0 then 'Executor' else 'Third Party' end,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id,
	   c.agncy_entty_crrltn_id
from   cnsmr c
where  c.agncy_entty_crrltn_id is not null

insert into ETL.mu_UDEFLEGAL_JUDGMENT
select top (10000)
       cnsmr_accnt_idntfr_lgcy_txt,
       GETDATE() - ca.cnsmr_accnt_id % 1,
       cnsmr_accnt_idntfr_lgcy_txt,
	   GETDATE() - ca.cnsmr_accnt_id % 2,
	   GETDATE() - ca.cnsmr_accnt_id % 3,
	   GETDATE() - ca.cnsmr_accnt_id % 4,
	   GETDATE() - ca.cnsmr_accnt_id % 5,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   123.123,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   GETDATE() - ca.cnsmr_accnt_id % 6,
	   GETDATE() - ca.cnsmr_accnt_id % 7,
	   '12',
	   123.123,
	   cnsmr_accnt_idntfr_lgcy_txt, --JUDG_BOOK
	   GETDATE() - ca.cnsmr_accnt_id % 8,
	   GETDATE() - ca.cnsmr_accnt_id % 9,
	   123.123,
	   GETDATE() - ca.cnsmr_accnt_id % 10,
	   GETDATE() - ca.cnsmr_accnt_id % 11,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   123.123,
	   cnsmr_accnt_idntfr_lgcy_txt, --JUDG_PAGE
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'Stipulation' else 'Contested' end, --JUDG_TYP
	   GETDATE() - ca.cnsmr_accnt_id % 12,
	   123.124,
	   120.12,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'No' else 'Yes' end, --BOND_FF
	   GETDATE() - ca.cnsmr_accnt_id % 13,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   GETDATE() - ca.cnsmr_accnt_id % 14,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'No' else 'Yes' end, --BOND_RQ
	   GETDATE() - ca.cnsmr_accnt_id % 15,
	   333.33,
	   12.12,
	   GETDATE() - ca.cnsmr_accnt_id % 16,
	   GETDATE() - ca.cnsmr_accnt_id % 17,
	   1.00,
	   2.00,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   GETDATE() - ca.cnsmr_accnt_id % 17,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'Dispute' else 'New Filing' end, --JUDGE_STATUS
	   33.00,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'NY64' else 'OH23' end, --FRM_ID
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'DEFOLD' else 'DEFSUIT' end --RCVR
from   cnsmr_accnt ca
where  ca.cnsmr_accnt_idntfr_lgcy_txt is not null

set identity_insert ETL.mu_UDEFACCT_DETAILS  off

insert into ETL.mu_UDEFACCT_DETAILS
select top (10000)
       cnsmr_accnt_idntfr_lgcy_txt,
       3,
       left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'N' else 'Y' end,
	   1,
	   2,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'ABCD' else '1234' end,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'ABCDE' else '1235' end,
	   '1234567',
	   GETDATE() - ca.cnsmr_accnt_id % 3,
	   left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'ABC' else 'DEF' end,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'No' else 'Yes' end, --CR_LIFE_INS
	   case when ca.cnsmr_accnt_id % 2 = 0 then '123' else '456' end,
	   left(cnsmr_accnt_idntfr_lgcy_txt, 16),
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'N' else 'Y' end, --IND_FLG
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'Time Barred Debt' else 'Date of Obsolescence' end, --TBD_DOO
	   cnsmr_accnt_idntfr_lgcy_txt,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   case when ca.cnsmr_accnt_id % 2 = 0 then '1234' else '5678' end, --HIER
	   left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'ABC' else 'DEF' end,
	   90,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'Boatmens - 325' else 'CCA - 555' end, --APPL_CODE
	   GETDATE() - ca.cnsmr_accnt_id % 19
from   cnsmr_accnt ca
where  ca.cnsmr_accnt_idntfr_lgcy_txt is not null

insert into ETL.mu_UDEFSCORE_STRATEGY
select top (10000)
       cnsmr_accnt_idntfr_lgcy_txt,
	   'SCORE'
from   cnsmr_accnt ca
where  ca.cnsmr_accnt_idntfr_lgcy_txt is not null

insert into ETL.mu_UDEFCBR_ACCOUNT
select top (10000)
       cnsmr_accnt_idntfr_lgcy_txt,
	   GETDATE() - ca.cnsmr_accnt_id % 3,
	   case when ca.cnsmr_accnt_id % 2 = 0 then '00' else '01' end,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'C' else 'I' end,
	   '0123123123',
	   GETDATE() - ca.cnsmr_accnt_id % 2,
	   '24',
	   GETDATE() - ca.cnsmr_accnt_id % 3,
	   GETDATE() - ca.cnsmr_accnt_id % 4,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'XA' else 'XB' end,
	   case when ca.cnsmr_accnt_id % 2 = 0 then '2M' else '99' end,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'Y' else 'N' end,
	   123.45,
	   case when ca.cnsmr_accnt_id % 2 = 0 then '05' else '13' end,
	   'METRO',
	   42.42,
	   'TERM DUR',
	   87.78,
	   980.00
from   cnsmr_accnt ca
where  ca.cnsmr_accnt_idntfr_lgcy_txt is not null

insert into ETL.mu_UDEFLEGAL_COST_TRACKING
select top (10000)
       cnsmr_accnt_idntfr_lgcy_txt,
	   GETDATE() - ca.cnsmr_accnt_id % 4,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'ANDR' else 'AHLD' end,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'DEFOLD' else 'DEFSUIT' end,
	   GETDATE() - ca.cnsmr_accnt_id % 511,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'Yes' else 'No' end,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'D116' else 'D119' end,
	   GETDATE() - ca.cnsmr_accnt_id % 2,
	   123.45,
	   cnsmr_accnt_idntfr_lgcy_txt
from   cnsmr_accnt ca
where  ca.cnsmr_accnt_idntfr_lgcy_txt is not null

insert into ETL.mu_UDEFLEGAL_COST_TRACKING_hst
select top (10000)
       cnsmr_accnt_idntfr_lgcy_txt,
	   GETDATE() - ca.cnsmr_accnt_id % 4,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'ANDR' else 'AHLD' end,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'DEFOLD' else 'DEFSUIT' end,
	   GETDATE() - ca.cnsmr_accnt_id % 511,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'Yes' else 'No' end,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'D116' else 'D119' end,
	   GETDATE() - ca.cnsmr_accnt_id % 2,
	   123.45,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   getdate() - ca.cnsmr_accnt_id % 42
from   cnsmr_accnt ca
where  ca.cnsmr_accnt_idntfr_lgcy_txt is not null

insert into ETL.mu_UDEFLEGAL_JUDGMENT_hst
select top (10000)
       cnsmr_accnt_idntfr_lgcy_txt,
       GETDATE() - ca.cnsmr_accnt_id % 1,
       cnsmr_accnt_idntfr_lgcy_txt,
	   GETDATE() - ca.cnsmr_accnt_id % 2,
	   GETDATE() - ca.cnsmr_accnt_id % 3,
	   GETDATE() - ca.cnsmr_accnt_id % 4,
	   GETDATE() - ca.cnsmr_accnt_id % 5,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   123.123,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   GETDATE() - ca.cnsmr_accnt_id % 6,
	   GETDATE() - ca.cnsmr_accnt_id % 7,
	   '12',
	   123.123,
	   cnsmr_accnt_idntfr_lgcy_txt, --JUDG_BOOK
	   GETDATE() - ca.cnsmr_accnt_id % 8,
	   GETDATE() - ca.cnsmr_accnt_id % 9,
	   123.123,
	   GETDATE() - ca.cnsmr_accnt_id % 10,
	   GETDATE() - ca.cnsmr_accnt_id % 11,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   123.123,
	   cnsmr_accnt_idntfr_lgcy_txt, --JUDG_PAGE
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'Stipulation' else 'Contested' end, --JUDG_TYP
	   GETDATE() - ca.cnsmr_accnt_id % 12,
	   123.124,
	   120.12,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'No' else 'Yes' end, --BOND_FF
	   GETDATE() - ca.cnsmr_accnt_id % 13,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   GETDATE() - ca.cnsmr_accnt_id % 14,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'No' else 'Yes' end, --BOND_RQ
	   GETDATE() - ca.cnsmr_accnt_id % 15,
	   333.33,
	   12.12,
	   GETDATE() - ca.cnsmr_accnt_id % 16,
	   GETDATE() - ca.cnsmr_accnt_id % 17,
	   1.00,
	   2.00,
	   cnsmr_accnt_idntfr_lgcy_txt,
	   GETDATE() - ca.cnsmr_accnt_id % 17,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'Dispute' else 'New Filing' end, --JUDGE_STATUS
	   33.00,
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'NY64' else 'OH23' end, --FRM_ID
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'DEFOLD' else 'DEFSUIT' end, --RCVR
	   GETDATE() - ca.cnsmr_accnt_id % 27 --adt_dttm
from   cnsmr_accnt ca
where  ca.cnsmr_accnt_idntfr_lgcy_txt is not null

insert into ETL.mu_UDEFFOD1099C
select top (10000)
       cnsmr_accnt_idntfr_lgcy_txt,
       300.13,
	   GETDATE() - ca.cnsmr_accnt_id % 3,
	   case when ca.cnsmr_accnt_id % 2 = 0 then '99' else 'C' end,
	   GETDATE() - ca.cnsmr_accnt_id % 4,
       left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'Settlement' else 'Uncollectible' end
from   cnsmr_accnt ca
where  ca.cnsmr_accnt_idntfr_lgcy_txt is not null

insert into etl.mu_UDEFSTRTGY_FIELDS
select top (10000)
       cnsmr_accnt_idntfr_lgcy_txt,
       left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'Y' else 'N' end,
	   'Balance'
from   cnsmr_accnt ca
where  isnull(ca.cnsmr_accnt_idntfr_lgcy_txt, '') <> ''

insert into etl.mu_UDEFDEBT_SETTLEMENT_AGENCY
select top (10000)
       cnsmr_accnt_idntfr_lgcy_txt,
       left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   'US',
	   left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   left(cnsmr_accnt_idntfr_lgcy_txt, 10),
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'Y' else 'N' end,
	   GETDATE() - ca.cnsmr_accnt_id % 3,
	   left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   'Approved',
	   case when ca.cnsmr_accnt_id % 2 = 0 then 'C' else 'V' end,
	   left(cnsmr_accnt_idntfr_lgcy_txt, 15),
	   GETDATE() - ca.cnsmr_accnt_id % 4,
	   GETDATE() - ca.cnsmr_accnt_id % 5,
	   left(cnsmr_accnt_idntfr_lgcy_txt, 10)
from   cnsmr_accnt ca
where  isnull(ca.cnsmr_accnt_idntfr_lgcy_txt, '') not in ('', '34567890876')


exec etl.sp_mu_UDEFCNSMR_DETAILS @chunk_size = 100
exec etl.sp_mu_UDEFTSYS_CC_STS_RSN_CDS @chunk_size = 100
exec etl.sp_mu_UDEFDECEASEDPROBATE @chunk_size = 137
exec etl.sp_mu_UDEFDECEASEDPROBATE_hst @chunk_size = 138
exec etl.sp_mu_UDEFESTT_CNTCT_INF @chunk_size = 187
exec etl.sp_mu_UDEFLEGAL_JUDGMENT @chunk_size = 870
exec etl.sp_mu_UDEFSCORE_STRATEGY @chunk_size = 150
exec etl.sp_mu_UDEFACCT_DETAILS @chunk_size = 177
exec etl.sp_mu_UDEFCBR_ACCOUNT @chunk_size = 77
exec etl.sp_mu_UDEFLEGAL_COST_TRACKING @chunk_size = 66
exec etl.sp_mu_UDEFLEGAL_COST_TRACKING_hst @chunk_size = 42
exec etl.sp_mu_UDEFLEGAL_JUDGMENT_hst @chunk_size = 870
exec etl.sp_mu_UDEFFOD1099C @chunk_size = 765
exec etl.sp_mu_UDEFSTRTGY_FIELDS @chunk_size = 44
exec etl.sp_mu_UDEFDEBT_SETTLEMENT_AGENCY @chunk_size = 67

select * from ETL.mu_UDEFDECEASEDPROBATE_hst

select * from etl.mu_processing_log order by logtime asc
select * from UDEFCNSMR_DETAILS
select * from UDEFTSYS_CC_STS_RSN_CDS
select * from UDEFDECEASEDPROBATE
select * from UDEFDECEASEDPROBATE_hst
select * from udefestt_cntct_inf
select * from UDEFLEGAL_JUDGMENT
select * from UDEFSCORE_STRATEGY
select * from UDEFACCT_DETAILS order by upsrt_dttm desc
select * from UDEFCBR_ACCOUNT order by upsrt_dttm desc
select * from UDEFLEGAL_COST_TRACKING order by upsrt_dttm desc
select * from UDEFLEGAL_COST_TRACKING_hst order by upsrt_dttm desc
select * from UDEFLEGAL_JUDGMENT_hst
select * from UDEFFOD1099C
select * from etl.mu_UDEFSTRTGY_FIELDS
select cnsmr_accnt_id from UDEFSTRTGY_FIELDS group by cnsmr_accnt_id having count(1) > 1
select * from etl.mu_UDEFDEBT_SETTLEMENT_AGENCY
select * from UDEFDEBT_SETTLEMENT_AGENCY

select * from cnsmr_accnt where cnsmr_accnt_id in (11234,11235,11236)

select ca.cnsmr_accnt_idntfr_agncy_id, udef.* from UDEFLEGAL_COST_TRACKING udef
inner join cnsmr_accnt ca on udef.cnsmr_accnt_id = ca.cnsmr_accnt_id
order by udef.upsrt_dttm desc

select * from cnsmr_accnt where cnsmr_accnt_id = 5000

select * from Ref_cntry_cd
